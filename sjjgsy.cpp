#include<iostream>
using namespace std;
typedef  int  QDataType;
typedef struct QListNode
{
	struct QListNode* next;
	QDataType data;
}QNode;
 
// 队列的结构 
typedef struct Queue
{
	QNode* front;
	QNode* rear;
	int size;
}Queue;
 
// 初始化队列 
void QueueInit(Queue& q)
{
	
	q.front = NULL;
	q.rear = NULL;
	q.size = 0;
}
// 队尾入队列 
void QueuePush(Queue& q, QDataType data)
{
	QNode* cur = new QNode[sizeof(QNode)];
 
	if (cur == NULL)
	{
		perror("malloc ");
		exit(-1);
	}
	cur->data = data;
	cur->next = NULL;
 
	if (q.rear == NULL)
	{
		q.front = q.rear = cur;
	}
	else
	{
		q.rear->next = cur;
		q.rear = cur;
	}
	q.size++;
}
// 队头出队列 
void QueuePop(Queue& q)
{
	if (q.front->next == NULL)
	{
		delete q.front;
		q.front = q.rear = NULL;
	}
	else
	{
		QNode* cur = q.front->next;
		delete q.front;
		q.front = cur;
	}
	q.size--;
}
// 获取队列头部元素 
QDataType QueueFront(const Queue& q)
{
	return q.front->data;
}
// 获取队列队尾元素 
QDataType QueueBack(const Queue& q)
{
	return q.rear->data;
}
// 获取队列中有效元素个数 
int QueueSize(const Queue& q)
{
	return q.size;
}
// 检测队列是否为空，如果为空返回非零结果，如果非空返回0 
bool QueueEmpty(const Queue& q)
{
	return q.front == NULL && q.rear == NULL;
}
// 销毁队列 
void QueueDestroy(Queue& q)
{
	QNode* cur = q.front;
	while (cur)
	{
		QNode* del = cur;
		cur = cur->next;
		free(del);
	}
 
	q.front = NULL;
	q.rear = NULL;
	q.size = 0;
}
 
int main()
{
    Queue q;
    QueueInit(q);
    int i =0;
    cin >>i;
    if(i==-1)
    {
        cout<<"Head:NULL"<<endl;
        cout<<"Pop:NULL"<<endl;
        return 0;
    }
    while(i!=-1)
    {
        QueuePush(q,i);
        cin>>i;
    }
    cout<<"Head:"<<QueueFront(q)<<endl;
    cout<<"Pop:";
    while(q.size)
    {
        cout<<QueueFront(q)<<" ";
        QueuePop(q);
    }
    cout<<"NULL";
} 