#define _CRT_SECURE_NO_WARNINGS 1
//实现一个通讯录；
//通讯录可以用来存储1000个人的信息，每个人的信息包括：姓名、性别、年龄、电话、住址
//提供方法：
//添加联系人信息
//删除指定联系人信息
//查找指定联系人信息
//修改指定联系人信息
//显示所有联系人信息
//清空所有联系人
//以名字排序所有联系人
#include"txl.h"

void menu()
{
	printf("*******1.添加联系人  2.删除指定联系人***********\n");
	printf("*******3.查找联系人  4.修改指定联系人***********\n");
	printf("*******5.显示联系人  6.清空所有联系人***********\n");
	printf("*******7.以名字进行排序 0.退出***********\n");
}
int main()
{
	int input = 0;
	//创建一个通讯录结构体】
	struct TXL t;
	//初始化通讯录
	CshTxl(&t);
	do
	{
		menu();
		printf("请输入您要选择的项目\n");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			TJlxr(&t);
			break;
		case 2:
			SClxr(&t);
			break;
		case 3:
			CZlxr(&t);
			break;
		case 4:
			XZlxr(&t);
			break;
		case 5:
			XSlxr(&t);
			break;
		case 6:
			CshTxl(&t);
			break;
		case 7:
			PXlxr(&t);
			break;
		case 0:
			TCtxl(&t);
			break;
		default:
			printf("您的选择错误，请重新选择\n");
			break;
		}
	} while (input);
	return 0;
}
