class SnapshotArray {
    int m_snap_id;
    unordered_map<int, vector<int>> m_snap_vec;

public:
    SnapshotArray(int length) {
        m_snap_id = 0;
        m_snap_vec[m_snap_id].resize(length, 0);
    }
    
    void set(int index, int val) {
        m_snap_vec[m_snap_id][index] = val;
    }
    
    int snap() {
        m_snap_id++;
        m_snap_vec[m_snap_id] = m_snap_vec[m_snap_id - 1];
        return m_snap_id - 1;
    }
    
    int get(int index, int snap_id) {
        return m_snap_vec[snap_id][index];
    }
};