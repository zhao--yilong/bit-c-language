#include<stdio.h>

int fun(int n)
{
    return 3*n -1;
}

int main()
{
    int n;
    
    while(EOF != scanf("%d",&n))
    {
        int sum=0;
        for(int i=1;i<=n;i++)
        {
            sum += fun(i);
        }
        printf("%d\n",sum);
    }
    
    return 0;
}
