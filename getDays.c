#include<stdio.h>
int getDays(int year, int month)
{
	if ((year % 4 == 0 && year % 100 != 0 || year % 400 == 0) &&month ==2)
	{
			return 29;
	}
	if (month == 2)
		return 28;

	if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
	{
		return 31;
	}
	else
		return 30;
}


int main()
{
	int year, month;

	scanf("%d %d", &year, &month);

	printf("%d天\n", getDays(year, month));



	return 0;
}