#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<utility>
#include<vector>
using namespace std;

enum State
{
	EMPTY,
	EXIST,
	DELETE
};

template<class K, class V>
struct HashData
{
	pair<K, V> _kv;
	State _state;
	HashData(const pair<K, V>& kv = make_pair(0, 0))
		:_kv(kv)
		, _state(EMPTY)
	{ }
};

template<class K, class V>
class HashTable
{
public:
	bool insert(const pair<K, V>& kv)
	{
		if (_table.size() == 0 || 10 * _size / _table.size() >= 7) // 扩容
		{
			size_t newSize = _table.size() == 0 ? 10 : _table.size() * 2;
			HashTable<K, V> newHT;
			newHT._table.resize(newSize);
			// 旧表的数据映射到新表
			for (auto e : _table)
			{
				if (e._state == EXIST)
				{
					newHT.insert(e._kv);
				}
			}
			_table.swap(newHT._table);
		}
		size_t index = kv.first % _table.size();//不能模capacity,如果模出来的数大于size了还插入进去了会报错
		//线性探测
		while (_table[index]._state == EXIST)
		{
			index++;
			index %= _table.size();//过大会重新回到起点
		}
		_table[index]._kv = kv;
		_table[index]._state = EXIST;
		_size++;
		return true;
	}

	HashData<K, V>* find(const K& key)
	{
		if (_table.size() == 0)
			return nullptr;

		size_t index = key % _table.size();//负数会提升成无符号数,所以负数不影响结果,但是string类不能取模,需要加入一个仿函数
		size_t start = index;
		while (_table[index]._state != EMPTY)
		{
			if (_table[index]._kv.first == key && _table[index]._state == EXIST)
				return &_table[index];
			index++;
			index %= _table.size();
			if (index == start)//全是DELETE时,必要时会break
				break;
		}
		return nullptr;
	}

	bool erase(const K& key)
	{
		HashData<K, V>* ret = find(key);
		if (ret)
		{
			ret->_state = DELETE;
			--_size;
			return true;
		}
		return false;
	}
	size_t Size()const
	{
		return _size;
	}

	bool Empty() const
	{
		return _size == 0;
	}

private:
	size_t HashFunc(const K& key)
	{
		return key % _table.capacity();
	}

private:
	vector<HashData<K, V>> _table;//数组中存储HashData封装的数据
	size_t _size = 0; //有效数据的个数
};




