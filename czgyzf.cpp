class Solution {
public:
    vector<string> commonChars(vector<string>& words)
    {
        int size1 = words.size();
        vector<int> v(26, 0);
        vector<int> minv(26,INT_MAX);

        vector<string> s;

        for(int i =0;i<size1;i++)
        {
            //清0
            fill(v.begin(), v.end(), 0);
            
            //获取字符串各个字符出现的次数
            for(int j =0;j<words[i].size();j++)
            {
                v[words[i][j]-'a']++;
            }
            
            //进行比对，去除一方没有出现的
            for(int j=0;j<26;j++)
            {
                minv[j]=min(minv[j],v[j]);
            }
        }
        
        //开始插入
        for (int i = 0;i < 26;i++)
        {
            //有几个，插入几次
            for(int j =0;j<minv[i];j++)
            {
                s.emplace_back(1,i+'a');
            }
        }
        return s;
    }
};