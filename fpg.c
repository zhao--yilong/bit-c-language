#include <stdio.h>

int fun(int m, int n)
{
    if(m==0||n==1)
    {
        return 1;
    }
    else if(m<n)
    {
        return fun(m,m);
    }
    else
    {
        return fun(m-n,n)+fun(m,n-1);
    }
}
int main()
{
    int a,b,num;
    while(~scanf("%d%d",&a,&b))
    {
        num = fun(a,b);
        printf("%d\n",num);
    }
}
