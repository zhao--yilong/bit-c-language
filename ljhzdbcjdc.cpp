class Solution {
public:
    vector<string> uncommonFromSentences(string s1, string s2) {
        unordered_map<string, int> count;
        vector<string> res;
        int start = 0;
        for (int i = 0; i < s1.size(); i++) 
        {
            if (s1[i] == ' ') {
                count[s1.substr(start, i - start)]++;
                start = i + 1;
            }
        }
        count[s1.substr(start)]++;
        start = 0;
        for (int i = 0; i < s2.size(); i++)
         {
            if (s2[i] == ' ') {
                count[s2.substr(start, i - start)]++;
                start = i + 1;
            }
        }
        count[s2.substr(start)]++;
        for (auto& [word, cnt] : count) {
            if (cnt == 1) {
                res.push_back(word);
            }
        }
        return res;
    }
};