#include<iostream>
#include<string>
#include<cstring>
#include<unistd.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include"Comm.hpp"

using namespace std;

void Usage(const string &process)
{
    cout<<"Usage: "<< process<<"server_ip server_port"<<endl;
}

int main (int argc,char * argv[])
{
   //./xxx  ip port
   if(argc !=3)
   {
    Usage(argv[0]);
    return 1;
   }

   //保存输入的ip和port
   string serverip =argv[1];
   uint16_t serverport =stoi(argv[2]);

   //创建socket
   //固定写法 ，创建tcp套接字
   int sockfd = socket(AF_INET,SOCK_STREAM,0);
   if(sockfd<0)
   {
    cerr<<"socket 创建失败"<<endl;
    return 1;
   }

   struct sockaddr_in server;//存储套接字地址信息
   memset(&server,0,sizeof(server));//将上面的对象内容初始化为0
   //将需要保存的信息存储到sockaddr_in结构体中
   server.sin_family =AF_INET;//指定使用了ipv4 协议
   server.sin_port =htons(serverport);//将port从主机字节序变为网络字节序进行存储
   inet_pton(AF_INET,serverip.c_str(),&server.sin_addr);//将点分十进制格式的ip 地址转为网络字节序的二进制形式，并存储在server.sin_addr中
   
   //客户端也需要bind,但是不需要用户自己去bind，客户端系统会自动进行随机的端口绑定
   //再发起连接时，客户端会被os自动进行本地绑定
   int n =connect (sockfd,CONV(&server),sizeof(server));//自行进行bind
   if(n<0)
   {
       cerr<<"connect 创建失败"<<endl;
       return 2;
   }

   while(true)
   {
       string inbuffer;
       cout<<"请输入：";
       getline(cin,inbuffer);//从cin获取数据，传入到inbuffer中

       ssize_t n =write(sockfd,inbuffer.c_str(),inbuffer.size());
       if(n>0)
       {
        char buffer[1024];
        ssize_t m =read(sockfd,buffer,sizeof(buffer)-1);
        if(m>0)
        {
            buffer[m]=0;
            cout<<"get a echo messsge->" <<buffer<<endl;
        }
        else if (m==0||m<0)
        {
            break;//读取失败直接退出
        }
       }
       else 
       {
        break;//写失败也直接退出
       }
   }
    
    close(sockfd);//关闭创建的套接字文件描述符
    return 0;
}
