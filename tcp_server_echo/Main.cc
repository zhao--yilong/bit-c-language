#include"Comm.hpp"
#include"TcpServer.hpp"
#include<memory> //智能指针

void Usage(const std::string &proc)
{
    std::cout<<"Usage: \n\t"<< proc<<"local_port"<<std::endl;
}

int main (int argc ,char * argv[])
{
   if(argc != 2)
   {
      Usage(argv[0]);
      return Usage_Err;
   }
   
    uint16_t port =std::stoi(argv[1]); //stoi将字符串装换成整数
    
    //unique_ptr 智能指针             make_unique c++14引入的，用于创建一个拥有动态分配对象的智能指针
    std::unique_ptr<TcpServer> tsvr =std::make_unique<TcpServer>(port);
    tsvr->Init();
    tsvr->Statr();

    return 0;
}