#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

struct LXR
{
	char name[20];
	char sex[5];
	int nianling;	
	char dianhua[20];
	char zhuzhi[30];
};
//静态版本
//struct TXL
//{
//	struct LXR L[MAX];
//	int sz;
//};
// 
//动态内存通讯录
struct TXL
{
	struct LXR* L;
	int sz;
	int rongliang;
};

//初始化通讯录
void CshTxl(struct TXL* t);
//添加联系人
void TJlxr(struct TXL* t);
//显示联系人
void XSlxr(struct TXL*t);
//删除联系人
void SClxr(struct TXL*t);
//查找联系人
void CZlxr(struct TXL* t);
//修改联系人
void XZlxr(struct TXL *t);
//排序联系人
void PXlxr(struct TXL *t);
//退出通讯录
void TCtxl(struct TXL* t);

