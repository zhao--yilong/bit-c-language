#include <stdio.h>
#include <string.h>
// i,j 匹配 相同 ++ ,? i++,j++  * i++ j+去匹配

int main()
{
  char std[200];
  char str[200];
  int cmp_str(char *std, char *str);
  while (fgets(std, 200, stdin) != NULL)
  {
    fgets(str, 200, stdin);

    for (int k = 0; k < strlen(str); k++)
    {
      if (str[k] >= 'A' && str[k] <= 'Z')
        str[k] = str[k] - 'A' + 'a';
    }
    for (int k = 0; k < strlen(std); k++)
    {
      if (std[k] >= 'A' && std[k] <= 'Z')
        std[k] = std[k] - 'A' + 'a';
    }
    //全部替换成小写

    if (cmp_str(std, str))
    {
      printf("true\n");
    }
    else
      printf("false\n");
  }

  return 0;
}

int cmp_str(char *std, char *str)
{
  int i = 0, j = 0;

  while (i < strlen(std) && j < strlen(str))
  {
    if (std[i] == '?')
    {
      if (str[j] >= 'a' && str[j] <= 'z' || str[j] >= '0' && str[j] <= '9')
        j++, i++;
      else
        return 0;
    }
    else if (std[i] == str[j])
      i++, j++;
    else if (std[i] == '*')
    {
      while (std[i + 1] == '*')
        i++;
      //三种情况 匹配0个字符 匹配下一个字符，匹配多个（std不动，还是*在匹配，str下移）
      return cmp_str(std + i + 1, str + j) || cmp_str(std + i + 1, str + j + 1) || cmp_str(std + i, str + j + 1);
    }
    else
      return 0;
  }


  if (i == strlen(std) && j == strlen(str))
    return 1;
  else
    return 0;
}

