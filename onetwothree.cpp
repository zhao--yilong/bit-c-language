#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>

class Foo {
private:
    std::mutex mtx1;
    std::mutex mtx2;
    std::condition_variable cv;
    bool readyOne, readyTwo;

public:
    Foo() : readyOne(false), readyTwo(false) {}

    void one() {
        std::unique_lock<std::mutex> lock(mtx1);
        std::cout << "one";
        readyOne = true;
        cv.notify_all(); // 通知two可以执行
    }

    void two() {
        std::unique_lock<std::mutex> lock(mtx2);
        while (!readyOne) {
            cv.wait(lock); // 等待one执行完成
        }
        std::cout << "two";
        readyTwo = true;
        cv.notify_one(); // 通知three可以执行
    }

    void three() {
        std::unique_lock<std::mutex> lock(mtx1);
        while (!readyTwo) {
            cv.wait(lock); // 等待two执行完成
        }
        std::cout << "three";
    }
};

void thread_function(Foo& foo, int thread_id) {
    switch (thread_id) {
    case 1:
        foo.one();
        break;
    case 2:
        foo.two();
        break;
    case 3:
        foo.three();
        break;
    }
}

int main() {
    Foo foo;
    std::thread t1(&thread_function, std::ref(foo), 1);
    std::thread t2(&thread_function, std::ref(foo), 2);
    std::thread t3(&thread_function, std::ref(foo), 3);

    t1.join();
    t2.join();
    t3.join();

    return 0;
}