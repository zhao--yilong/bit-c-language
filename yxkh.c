// 支持动态增长的栈
typedef char STDataType;
typedef struct Stack
{
	STDataType* a;
	int top;		// 栈顶
	int capacity;  // 容量 
}Stack;

// 初始化栈 
void StackInit(Stack* ps)
{
	assert(ps);
	ps->capacity = 5;
	ps->a = (Stack*)malloc(ps->capacity * sizeof(STDataType));
	if (ps->a == NULL)
	{
		perror("malloc");
		exit(-1);
	}
	ps->top = 0;
}
// 入栈 
void StackPush(Stack* ps, STDataType data)
{
	assert(ps);
	//扩容
	if (ps->top == ps->capacity)
	{
		Stack * da = (Stack*)realloc(  ps->a  ,ps->capacity * 2 * sizeof(STDataType));
		if (da == NULL)
		{
			perror("realloc");
			exit(-1);
		}
		else
		{
			ps->a = da;
		}
		ps->capacity *= 2;
	}

	ps->a[ps->top] = data;
	ps->top++;
}
// 出栈 
void StackPop(Stack* ps)
{
	assert(ps);
	assert(ps->top > 0);
	ps->top--;
}
// 获取栈顶元素 
STDataType StackTop(Stack* ps)
{
	assert(ps);
	return ps->a[ps->top-1];
}
// 获取栈中有效元素个数 
int StackSize(Stack* ps)
{
	return ps->top;
}
// 检测栈是否为空，如果为空返回非零结果，如果不为空返回0 
bool StackEmpty(Stack* ps)
{
	if (ps->top == 0)
		return true;
	else
		return false;
}
// 销毁栈 
void StackDestroy(Stack* ps)
{
	assert(ps);

	ps->capacity = 0;
	ps->top = 0;
	free(ps->a);
	ps->a = NULL;	
}


bool isValid(char * s)
{
     Stack st;
     StackInit(&st);
    char  *cur =s;
    while(*cur)
    {
        if(*cur=='['||*cur=='('||*cur=='{')
        {
            StackPush(&st,*cur);
            cur++;
        }
        else
        {
             if(StackEmpty(&st))
                return false;

            char top =StackTop(&st);
            StackPop(&st);
           if(top =='('&&*cur!=')'
           ||top =='{'&&*cur!='}'
           ||top =='['&&*cur!=']'
           )
            return false;
            else
            cur++;
        }
    }

  bool  ret = StackEmpty(&st);
   
    StackDestroy(&st);
    return ret;
    

}