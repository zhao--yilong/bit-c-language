#include<iostream>
#include<string>
#include<algorithm>
using namespace std;

int Distance(const string& s1,const string& s2){
    int n=s1.size(); int m=s2.size();
    if(n*m==0) return n+m;
    int dp[n+1][m+1];
    for(int i=0;i<n+1;++i){
        dp[i][0]=i;
    }
    for(int j=0;j<m+1;++j){
        dp[0][j]=j;
    }
    for(int i=1;i<n+1;++i){
        for(int j=1;j<m+1;++j){
            if(s1[i-1]==s2[j-1])//这里遍历的是字符串，记得减一
                dp[i][j]=min(dp[i-1][j-1],min(dp[i-1][j]+1,dp[i][j-1]+1));
            else
                dp[i][j]=min(dp[i-1][j-1]+1,min(dp[i-1][j]+1,dp[i][j-1]+1));
        }
    }
    return dp[n][m];
}

int main(){
    string str1;
    string str2;
    while(cin>>str1>>str2){
        cout<<Distance(str1, str2)<<endl;
    }
    return 0;
}
