#include<iostream>
using namespace std;
#define max_len 101
int arr[max_len][max_len];

bool sym_matrix(int arr[max_len][max_len],int n)
{
    bool c = true;
    for(int i = 0;i < n;i++)
        for(int j = i;j < n;j++)
            if(arr[i][j] != arr[j][i])
            {
                c = false;
                break;
            }
    return c;
}

int main(void)
{
    int n;
    
    while(cin >> n)
    {
        
        for(int i = 0;i < n;i++)
            for(int j = 0;j < n;j++)
                cin >> arr[i][j];
        if(sym_matrix(arr, n))
            cout << "Yes!" << endl;
        else
            cout << "No!" << endl;
    }
    return 0;
}