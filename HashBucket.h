#define _CRT_SECURE_NO_WARNINGS 1
//哈希桶
#include<iostream>
#include<vector>
using namespace std;

template <class K,class V>
struct HashNode
{
	HashNode<K, V>* _next;
	pair<K, V>  _kv;

	HashNode(const pair<K,V> &kv)
		:_next(nullptr)
		,_kv(kv)
	{}
};

template <class K,class V>
class HashBucket
{
public:
	typedef HashNode<K, V> Node;

	bool insert(const pair <K, V>& kv)
	{
		//去重+扩容
		if (find(kv.first))
			return false;
		//负载因子到1就扩容
		if (_size == _table.size())
		{
			vector<Node*> newT;
			size_t newSize = _table.size() == 0 ? 10 : _table.size() * 2;
			newT.resize(newSize, nullptr);
			//将旧表中的节点移动到新表
			for (int i = 0; i < _table.size(); i++)
			{
				Node* cur = _table[i];
				while (cur)
				{
					Node* next = cur->_next;
					size_t hashi = cur->_kv.first % newT.size();
					cur->_next = newT[hashi];
					newT[i] = cur;
					cur = next;
				}
				_table[i] == nullptr;
			}
			_table.swap(newT);
		}
		size_t hashi = kv.first % _table.size();
		//头插
		Node* newnode = new Node(kv);
		newnode->_next = _table[hashi];
		_table[hashi] = newnode;
		++_size;
		return true;
	}

	Node* find(const K& key)
	{
		if (_table.size() == 0)
			return nullptr;
		size_t hashi = key % _table.size();
		Node* cur = _table[hashi];
		while (cur)//走到空还没有就是没用此数据
		{
			if (cur->_kv.first == key)
				return cur;
			cur = cur->_next;
		}
		return nullptr;
	}

	bool erase(const K& key)
	{
		Node* ret = find(key);
		if (ret == nullptr)
			return false;
		size_t hashi = key % _table.size();
		Node* cur = _table[hashi];
		Node* prev = nullptr;
		while (cur && cur->_kv.first != key)//找到要删除的节点
		{
			prev = cur;
			cur = cur->_next;
		}
		Node* next = cur->_next;
		if (cur == _table[hashi])//注意头删的情况
			_table[hashi] = next;
		else
			prev->_next = next;
		delete cur;
		cur = nullptr;
		_size--;
		return true;
	}
private:
	vector<Node*> _table;
	size_t _size = 0;
};