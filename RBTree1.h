#define _CRT_SECURE_NO_WARNINGS 1
#include<time.h>
#include<utility>
#include<iostream>
using namespace std;
enum COLOR
{
	BLACK,
	RED
};
template <class T>
struct RBNode
{
	RBNode<T>* _left;
	RBNode<T>* _right;
	RBNode<T>* _parent;
	T _value;
	COLOR _color;//颜色

	RBNode(const T & value=T())
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _value(value)
		, _color(RED)
	{}
};

template <class T>
class RBTree
{
public:
	typedef RBNode<T> Node;
	typedef Node* pNode;

	RBTree()
	{
		//构建空的红黑树  空树--》带头的红黑树，头不是根
		_header = new Node;
		_header->_left = _header;
		_header->_right = _header;
	}


	/*
	红黑树插入：
	1.相对于AVL树，插入比较简单，且效率高，红黑树比AVL树调整次数要少
	2.二叉树进行插入
	3.判断有没有连续的红色结点
	如果有：
	a:只需要修改颜色： uncle为红色
	b:修改颜色，旋转：u不存在、存在且为黑
	单旋：cur,parent在gfather的同一边
	双旋：cur,parent不在gfather的同一边，首先经过一次单璇，交换指针，转化为上面单璇场景，
	没有：
	不需要做任何操作，插入结束。
	*/

	bool insert(const T& value)
	{
		//搜索树的插入
		if (_header->_parent == nullptr)
		{
			//空树，创建根节点
			pNode root = new Node(value);
			root->_color = BLACK;
			root->_parent = _header;
			_header->_parent = root;

			_header->_left = root;
			_header->_right = root;
			return true;
		}

		//从根开始搜索
		pNode cur = _header->_parent;
		pNode parent = nullptr;
		//查找插入的位置
		while (cur)
		{
			parent = cur;
			//按照key值确定位置, key不能重复
			if (cur->_value == value)
				return false;
			else if (cur->_value > value)
				cur = cur->_left;
			else
				cur = cur->_right;
		}
		//节点创建
		cur = new Node(value);
		//节点插入
		if (parent->_value > cur->_value)
			parent->_left = cur;
		else
			parent->_right = cur;
		//节点连接
		cur->_parent = parent;

		//调整和更新（颜色）：连续红色需要调整
		while (cur != _header->_parent && cur->_parent->_color == RED)//当前不是根，并且你的父亲是红色
		{
			//cur:当前节点，parent:父亲节点， gfather：祖父节点，uncle:叔叔节点
			parent = cur->_parent;
			pNode gfather = parent->_parent;
			if (gfather->_left == parent)
			{
				pNode uncle = gfather->_right;
				//uncle 存在且为红
				if (uncle && uncle->_color == RED)
				{
					//修改颜色
					parent->_color = uncle->_color = BLACK;
					gfather->_color = RED;
					//继续向上更新
					cur = gfather;
				}
				else
				{
					//如果存在双旋的场景，可以先进行一次单旋，使它变成单旋的场景
					if (cur == parent->_right)
					{
						RotateL(parent);
						swap(cur, parent);
					}
					//右旋
					RotateR(gfather);
					//修改颜色
					parent->_color = BLACK;
					gfather->_color = RED;
					//停止调整
					break;
				}

			}
			//gfather->_right == parent
			else
			{
				pNode uncle = gfather->_left;
				if (uncle && uncle->_color == RED)
				{
					//修改颜色
					uncle->_color = parent->_color = BLACK;
					gfather->_color = RED;
					cur = gfather;
				}
				else
				{
					//判断是否有双旋的场景
					if (cur == parent->_left)
					{
						//以parent右旋
						RotateR(parent);
						//交换指针
						swap(cur, parent);
					}
					//以gfather 左旋
					RotateL(gfather);
					//修改颜色
					parent->_color = BLACK;
					gfather->_color = RED;
					//停止调整
					break;
				}
			}
		}

		//根的颜色始终是黑的 根：_header->_parent
		_header->_parent->_color = BLACK;

		//更新 _header->_left, _header->_right
		_header->_left = leftMost();
		_header->_right = rightMost();
		return true;
	}
	pNode leftMost()
	{
		pNode cur = _header->_parent;
		while (cur && cur->_left != nullptr)
		{
			cur = cur->_left;
		}
		return cur;
	}

	pNode rightMost()
	{
		pNode cur = _header->_parent;
		while (cur && cur->_right != nullptr)
		{
			cur = cur->_right;
		}
		return cur;
	}

	void RotateR(pNode parent)
	{
		pNode subL = parent->_left;
		pNode subLR = subL->_right;

		// 1
		subL->_right = parent;
		// 2
		parent->_left = subLR;
		// 3
		if (subLR)
			subLR->_parent = parent;
		// 4,  5
		if (parent != _header->_parent)
		{
			// subL <---> parent->parent
			pNode gParent = parent->_parent;
			if (gParent->_left == parent)
				gParent->_left = subL;
			else
				gParent->_right = subL;
			subL->_parent = gParent;
		}
		else
		{
			//更新根节点
			_header->_parent = subL;
			//subL->_parent = nullptr;
			subL->_parent = _header;
		}
		// 6
		parent->_parent = subL;

	}

	void RotateL(pNode parent)
	{
		pNode subR = parent->_right;
		pNode subRL = subR->_left;

		subR->_left = parent;
		parent->_right = subRL;
		if (subRL)
			subRL->_parent = parent;
		if (parent != _header->_parent) {
			pNode gParent = parent->_parent;
			if (gParent->_left == parent)
				gParent->_left = subR;
			else
				gParent->_right = subR;
			subR->_parent = gParent;
		}
		else
		{
			_header->_parent = subR;
			//根的父节点不是nullptr
			//subR->_parent = nullptr;
			subR->_parent = _header;
		}
		parent->_parent = subR;
	}

	void inOrder()
	{
		_inOrder(_header->_parent);
	}

	void _inOrder(pNode root)
	{
		if (root) {
			_inOrder(root->_left);
			cout << root->_value<<endl;
			_inOrder(root->_right);
		}
	}

	bool isRBTree()
	{
		pNode root = _header->_parent;
		if (root == nullptr)
			return true;
		if (root->_color == RED)
		{
			cout << "根节点必须是黑色的!!!" << endl;
			return false;
		}
		//根节点是黑色
		//需要判断每条路径上黑色个数相同
		//可以先任意遍历一条路径 比如走最右路径。查找black数量
		pNode cur = root;
		int blackCount = 0;
		while (cur)
		{
			if (cur->_color == BLACK)
				++blackCount;
			cur = cur->_right;
		}
		int k = 0;
		return _isRBTree(root, k, blackCount);
	}

	bool  _isRBTree(pNode root, int curBlackCount, int totalBlackCout)//curBlackCount:走到当前节点黑色个数
	{
		//每条路径上黑色个数相同//没有连续红色结点
		//一条路径走完
		if (root == nullptr)
		{
			if (curBlackCount != totalBlackCout)
			{
				cout << "每条路径-黑色结点个数不同" << endl;
				return false;
			}
			return true;

		}

		if (root->_color == BLACK)
			++curBlackCount;

		//没有红色连续
		pNode parent = root->_parent;
		if (parent->_color == RED && root->_color == RED)
		{
			cout << "有连续的红色结点" << endl;
			return false;
		}
		return  _isRBTree(root->_left, curBlackCount, totalBlackCout) && _isRBTree(root->_right, curBlackCount, totalBlackCout);
	}

private:
	pNode _header;
};





