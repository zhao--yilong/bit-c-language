//计算二维数组周边元素之和
#include<stdio.h>
#define N 5
#define M 4

int fun(int a[M][N])
{
	int i,j;
	int sum = 0;
	for (i = 0;i < M;i++)
	{
		for (j = 0;j < N;j++)
		{
			if (i == 0 || i == M - 1 || j == 0 || j == N - 1)
			{
				sum += a[i][j];
			}
		}
	}
	return sum;
}

int main()
{
	int a[M][N] = { {1,3,5,7,9},{2,9,9,9,4},{6,9,9,9,8},{1,3,5,7,0} };

	int y = fun(a);
	printf("\n周边元素之和为：%d\n", y);

	return 0;
}