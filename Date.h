#pragma once

#include<assert.h>
#include<iostream>
using namespace std;


class Date
{

public:

	// 获取某年某月的天数
	int GetMonthDay(int year, int month);

	//构造函数
	Date(int year = 2000, int month = 1, int day = 26);

	//拷贝构造
	Date(const Date& d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
    }

	//打印日期类
	void Print() const;

	// 赋值运算符重载

// d2 = d3 -> d2.operator=(&d2, d3)

	Date& operator=(const Date& d)
	{
		if (this != &d)
		{
			_year = d._year;
			_month = d._year;
			_day = d._day;
		}
		return *this;
	}

	// >运算符重载
	bool operator>(const Date& d);

	// <运算符重载

	bool operator < (const Date& d);

	// <=运算符重载

	bool operator <= (const Date& d);

	// >=运算符重载

	bool operator >= (const Date& d);

	// ==运算符重载

	bool operator==(const Date& d);

	// !=运算符重载

	bool operator != (const Date& d);

	// 日期+=天数

	Date& operator+=(int day);

	//日期-=天数
	Date& operator-=(int day);
    
	// 日期+天数

	Date operator+(int day);

	// 日期-天数

	Date operator-(int day);

	// 前置++

	Date& operator++();



	// 后置++

	Date operator++(int);



	// 后置--

	Date operator--(int);



	// 前置--

	Date& operator--();

	// 日期-日期 返回天数

	int operator-(const Date& d);

private:

	int _year;
	int _month;
	int _day;
};