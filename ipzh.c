#include <stdio.h>
#include <string.h>

int main(void)
{
    char str[128];
    while(gets(str))
    {
        int is_ip = 0;
        int len = strlen(str);
        for(int i = 0; i < len; i++)
        {
            if(str[i] == '.')
                is_ip = 1;
        }
        if(is_ip)
        {
            unsigned int number = 0;
            unsigned char ip[4];
            sscanf(str, "%d.%d.%d.%d", ip, ip+1, ip+2, ip+3);
            number |= ip[0]<<24;
            number |= ip[1]<<16;
            number |= ip[2]<<8;
            number |= ip[3];
            printf("%u\n", number);
        }
        else
        {
            unsigned int number = 0;
            unsigned char ip[4];
            sscanf(str, "%u", &number);
            ip[0] = number>>24 & 0xFF;
            ip[1] = number>>16 & 0xFF;
            ip[2] = number>>8 & 0xFF;
            ip[3] = number & 0xFF;
            printf("%d.%d.%d.%d\n", ip[0], ip[1], ip[2], ip[3]);
        }
    }
    return 0;
}
