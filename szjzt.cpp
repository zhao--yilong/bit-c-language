#include<bits/stdc++.h>
using namespace std;
int a[1005][1005],n,ans;
void dfs(int x,int y,int cur){
	if(x==n){
		ans=max(ans,cur);
		return;
	}
	dfs(x+1,y,cur+a[x+1][y]);
	dfs(x+1,y+1,cur+a[x+1][y+1]);
}
int main(){
	cin>>n;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=i;j++)
			cin>>a[i][j];
	dfs(1,1,a[1][1]);
	cout<<ans;
    return 0;
}