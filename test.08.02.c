#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//int main()
//{
//	int a = 0x11223344;
//	char* pc = (char*)&a;
//	*pc = 0;
//	printf("%x\n", a);
//	return 0;
//
//
//	return 0;
//}
//写一个函数打印arr数组的内容，不使用数组下标，使用指针。

//arr是一个整形一维数组。

//void print(int* arr,int sz)
//{
//	int i = 0;
//	for (i = 0;i <sz ;i++)
//	{
//		printf("%d ", *(arr + i));
//	}
//
//}
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	print(arr,sz);
//
//	return 0;
//}

//#include<string.h>
//void nx(char* str,int sz)
//{
//    char * left = str;
//    char * right = (str + sz)-1;
//
//    while (*left < *right)
//    {
//        char* tmp = *left;
//        *left = *right;
//        *right = *tmp;
//        left++;
//        right--;
//    }
//    printf("%s", str);
//    
//}
//int main()
//{
//    char c[50];
//    scanf("%s", &c);
//    int sz = strlen(c);
//    nx(c,sz);
//    return 0;
//}
//
//int main()
//{
//	int i = 0;
//	int j = 0;
//	for (i = 0;i < 7;i++)//上半部分
//	{
//		for (j = 6;j >i ;j--)//打印空格
//		{
//			printf("  ");
//		}
//		for (j = 0;j <(2 * i + 1);j++)//打印*
//		{
//			printf("* ");
//		}
//		printf("\n");
//	}
//
//	for (i = 5;i >=0;i--)//下半部分
//	{
//		for (j=0 ;j<6-i;j++)//打印空格
//		{
//			printf("  ");
//		}
//		for (j = 0;j < (2 * i + 1);j++)//打印*
//		{
//			printf("* ");
//		}
//		printf("\n");
//
//	}
//	return 0;
//}



//求Sn=a+aa+aaa+aaaa+aaaaa的前5项之和，其中a是一个数字，
//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	int i = 0;
//	int sum = 0;
//	int d = a;
//	for (i = 0;i < 5;i++)
//	{
//		    sum = sum + a;
//			a = a * 10 + d;
//	}
//	printf("%d", sum);
//	return 0;
//}

//输出逆序的字符串
//#include<string.h>
//
//int main()
//{
//	char ch[100];
//	scanf("%s", ch);//输入一个字符串
//	
//	int sz = strlen(ch);
//	
//	printf("%s\n", ch);
//
//	nx(ch, sz);
//
//	printf("%s\n", ch);
//
//	return 0;
//}
//求出0～100000之间的所有“水仙花数”并输出。

//“水仙花数”是指一个n位数，其各位数字的n次方之和确好等于该数本身，如:153＝1 ^ 3＋5 ^ 3＋3 ^ 3，则153是一个“水仙花数”。

#include<math.h>
int main()
{
	int i = 0;
	for (i = 1;i <= 100000;i++)
	{
		int count = 1;//最少是一位数
		int  sum = 0;
		int n  = i;
		while (n / 10)
		{
			count++;//计算位数
			n /= 10;
		}
		n = i;
		while (n)
		{
			sum += pow(n % 10, count);
			n /= 10;
    	 }
		if (i == sum)
		{
			printf("%d\n", sum);
		}
	}

	return 0;
}

