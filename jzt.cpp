#include<bits/stdc++.h>
using namespace std;
int a[1005][1005],f[1005][1005],n;
int dfs(int x,int y){
	if(f[x][y]==-1)
		f[x][y]=(x==n?a[x][y]:a[x][y]+max(dfs(x+1,y),dfs(x+1,y+1)));
	return f[x][y];
} 
int main(){
	memset(f,-1,sizeof(f));
	cin>>n;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=i;j++)
			cin>>a[i][j];
	dfs(1,1);
	cout<<f[1][1];
    return 0;
}