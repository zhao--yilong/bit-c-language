#define _CRT_SECURE_NO_WARNINGS 1
#include"txl.h"

//初始化通讯录
void CshTxl(struct TXL* t)
{
	t->sz = 0;
	memset(t->L, 0, sizeof(t->L));
}

//添加联系人
void TJlxr(struct TXL* t)
{

	if (t->sz == MAX)
	{
		printf("通讯录人员信息已满 无法增加\n");
		return;//因为返回值是void 所以return 不需要返回东西  直接结束函数运行
	}

	printf("请输入联系人姓名 性别 年龄 电话号 住址 用空格隔开\n");

	scanf("%s %s %d %s %s", t->L[t->sz].name,
		t->L[t->sz].sex,
		&t->L[t->sz].nianling,
		t->L[t->sz].dianhua,
		t->L[t->sz].zhuzhi);

		t->sz++;
}

void XSlxr(struct TXL* t)
{
	int i = 0;
	if (t->sz == 0)
	{
		printf("抱歉 您的通讯录里面还没有人\n");
		return;
	}
	for (i = 0;i < t->sz;i++)
	{
		printf("%-9s %-9s %-9s %-9s %-9s \n", "姓名", "性别", "年龄", "电话", "住址");
		printf("%-9s %-9s %-9d %-9s %-9s \n",t->L[i].name,
			t->L[i].sex,
			t->L[i].nianling,
			t->L[i].dianhua,
			t->L[i].zhuzhi
		);
	}
}

void SClxr(struct TXL* t)
{
	printf("请输入您要删除人的姓名\n");
	char name2[30];
	scanf("%s", &name2);
	int i = 0;
	for (i = 0;i < t->sz;i++)
	{
		int j = i;
		if (strcmp(name2, t->L[i].name) == 0)
		{
			for (j = i;j < t->sz;j++)
			{
				t->L[j] = t->L[j + 1];
			}
		}
	}
	t->sz -= 1;
	printf("删除成功\n");


}

void  CZlxr(struct TXL* t)
{
	printf("请输入您要查找的姓名\n");
	char name1[30];
	scanf("%s", &name1);
	int i = 0;
	for (i = 0;i < t->sz;i++)
	{
		if (strcmp(t->L[i].name, name1) == 0)
		{
			printf("%-9s %-9s %-9s %-9s %-9s \n", "姓名", "性别", "年龄", "电话", "住址");
			printf("%-9s %-9s %-9d %-9s %-9s \n", t->L[i].name,
				t->L[i].sex,
				t->L[i].nianling,
				t->L[i].dianhua,
				t->L[i].zhuzhi
			);
			break;
		}
	}
}

void XZlxr(struct TXL* t)
{
	printf("请输入您要修改的联系人姓名\n");
	char name[20];
	scanf("%s", &name);
	int i = 0;
	for (i = 0;i < t->sz;i++)
	{
		if (strcmp(t->L[i].name, name) == 0)
		{
			printf("请进行 您的修改\n");
			printf("请按  姓名 性别 年龄 电话 住址 修改 中间用空格隔开\n");
			scanf("%s %s %d %s %s", t->L[i].name,
				t->L[i].sex,
				&t->L[i].nianling,
				t->L[i].dianhua,
				t->L[i].zhuzhi);
			printf("修改成功\n");
			break;
		}
	}
}


void PXlxr(struct TXL* t)
{
	int i = 0;
	for (i = 0;i < t->sz-1;i++)
	{
		int j = 0;
		for (j = 0;j < t->sz - i - 1;i++)
		{
			if (strcmp(t->L[j].name, t->L[j + 1].name) < 0)
			{
				struct LXR tmp = t->L[j];
				t->L[j] = t->L[j + 1];
				t->L[j + 1] = tmp;
			}
		}
	}
	printf("叮咚，排序完成\n");



}
