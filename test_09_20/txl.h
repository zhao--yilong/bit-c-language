#define _CRT_SECURE_NO_WARNINGS 1
//实现一个通讯录；
//通讯录可以用来存储1000个人的信息，每个人的信息包括：姓名、性别、年龄、电话、住址
//提供方法：
//添加联系人信息
//删除指定联系人信息
//查找指定联系人信息
//修改指定联系人信息
//显示所有联系人信息
//清空所有联系人
//以名字排序所有联系人
#include<stdio.h>
#include<string.h>
#define MAX 1000


struct LXR
{
	char name[20];
	char sex[5];
	int nianling;	
	char dianhua[20];
	char zhuzhi[30];
};

struct TXL
{
	struct LXR L[MAX];
	int sz;
};
//初始化通讯录
void CshTxl(struct TXL* t);
//添加联系人
void TJlxr(struct TXL* t);
//显示联系人
void XSlxr(struct TXL*t);
//删除联系人
void SClxr(struct TXL*t);
//查找联系人
void CZlxr(struct TXL* t);
//修改联系人
void XZlxr(struct TXL *t);
//排序联系人
void PXlxr(struct TXL *t);

