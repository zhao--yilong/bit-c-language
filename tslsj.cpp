#include<iostream>
#include<vector>
#include<string>
using namespace std;

//创建一个类，只能在堆上创建对象

//1. 将类的构造函数私有，拷贝构造声明成私有。防止别人调用拷贝在栈上生成对象。
//2. 提供一个静态的成员函数，在该静态成员函数中完成堆对象的创建

class D
{
public:
	static D* CreateObject()
	{
		return new D;
	}

private:
	//构造函数私有
	D()
	{

	}
	//拷贝构造私有
	D(const D&)
	{

	}

	D(const D&) = delete;

};

//创建一个类，只能在栈上创建对象
//构造函数私有化，然后设计静态方法创建对象返回即可

class Z
{
public:
	static Z CreateObj()
	{
		return Z();
	}
	
	void* operator new(size_t size) = delete;
	void operator delete(void* p) = delete;


private:
	Z()
	{}
};

//创建一个类,该类不能被拷贝
//拷贝只会放生在两个场景中：拷贝构造函数以及赋值运算符重载，因此想要让一个类禁止拷贝，
//只需让该类不能调用拷贝构造函数以及赋值运算符重载即可

//c++98 将拷贝构造函数与赋值运算符重载只声明不定义，并且将其访问权限设置为私有即可。
class Copy
{
	// ...
private:
	Copy(const Copy&);
	Copy& operator=(const Copy&);
	//...
};

//c++11C++11扩展delete的用法，delete除了释放new申请的资源外，如果在默认成员函数后跟上 = delete，表示让编译器删除掉该默认成员函数
class Copy
{
	Copy(const Copy&)=delete;
	Copy& operator=(const Copy&)=delete;
};


//创建一个类,该类不能被继承
//final关键字，final修饰类，表示该类不能被继承。

class J   final
{

};



int main()
{


	return 0;
}