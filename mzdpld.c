#include<stdio.h>
#include<string.h>
int main(){
    char str[1000][10010]={'\0'};
    int n,count[1000][26]={0};
    while(scanf("%d",&n)==1){
        for(int i=0;i<n;i++)
            scanf("%s",&str[i][0]);
        for(int i=0;i<n;i++){
            for(int j=0;j<strlen(str[i]);j++)
                count[i][str[i][j]-'a']++;
            //排序
            int max;
            for(int k=0;k<26-1;k++){
                for(int m=k+1;m<26;m++){
                    if(count[i][k]<count[i][m]){
                        max=count[i][m];
                        count[i][m]=count[i][k];
                        count[i][k]=max;
                    }
                }
            }
            //最大漂亮程度
            int nice=26,out=0;
            for(int pos=0;pos<26;pos++){
                if(count[i][pos]!=0)
                    out+=count[i][pos]*(nice--);
            }
            printf("%d\n",out);
        }
    }
}
