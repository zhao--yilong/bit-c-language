//模拟实现strlen   计算字符串长度
int my_strlen( char* s)  
{
 int i = 0;
 while (*s++ != '\0')
 {
  i++;
 }
 return i;
}


//模拟实现strcpy   字符串拷贝  src拷贝到dest中
char* my_strcpy(char* dest, char* src)//char* 返回的是目标空间首地址
{
 char* ret = dest;
 while (*dest++ = *src++);
 return ret;
}


//模拟实现 strcat  字符串追加 在dest\0位置追加src
char* my_strcat(char* dest, const char* src)
{
 //1.找目标空间中的\0
 char* cur = dest;
 while (*cur)
 {
  cur++;
 }
 //2.拷贝源头数据到\0之后的空间
 while (*cur++ = *src++);
 return dest;
}

//模拟实现 strcmp  字符串比较 
int my_strcmp(char* s1,char* s2)
{
 while (*s1 == *s2)
 {
  if (*s1 == '\0')
  {
   return 0;
  }
  s1++;
  s2++;
 }
 return *s1 - *s2;
}