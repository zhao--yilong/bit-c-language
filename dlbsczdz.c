// 沒看到有c的，我来发一个吧
// 由于是刷题，malloc、scanf等没判断返回值，见谅

#include <stdio.h>
#include <stdlib.h>

typedef struct tagNode {
    int val;
    struct tagNode *next;
} Node;

static void insertNode(Node *head, int aim, int val) {
    while(head != NULL) {
        if(head->val == aim) {
            Node *temp = (Node*)malloc(sizeof(Node));
            temp->val = val;
            temp->next = head->next;
            head->next = temp;
            return;
        }
        head = head->next;
    }
}

static void delNode(Node **head, int aim)
{
    if ((*head)->val == aim) {
        Node *temp = (*head)->next;
        free(*head);
        *head = temp;
        return;
    }
    Node *iter = *head;
    while (iter->next != NULL) {
        if (iter->next->val == aim) {
            Node *temp = iter->next;
            iter->next = temp->next;
            free(temp);
            return;
        }
        iter = iter->next;
    }
}

static void printList(Node *head) {
    while(head != NULL) {
        printf("%d ", head->val);
        head = head->next;
    }
    printf("\n");
}

static void freeList(Node *head) {
    while(head != NULL){
        Node *next = head->next;
        free(head);
        head = next;
    }
}

int main() {
    int num = 0;
    int headVal = 0;
    int delVal = 0;
    scanf("%d %d", &num, &headVal);
    Node *head = (Node*)malloc(sizeof(Node));
    head->next = NULL;
    head->val = headVal;
    int i;
    for (i = 1; i < num; i++) {
        int aim = 0;
        int val = 0;
        scanf("%d %d", &val, &aim);
        insertNode(head, aim, val);
    }
    scanf("%d", &delVal);
    delNode(&head, delVal);
    printList(head);
    freeList(head);
    return 0;
}
