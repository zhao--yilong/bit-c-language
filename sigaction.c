#include <stdio.h>
#include <signal.h>
#include <unistd.h>


void sigcb(int signum) {

    printf("Caught signal: %d\n", signum);
}

int main() {

    struct sigaction sa;

    sa.sa_handler = sigcb; 
    sigemptyset(&sa.sa_mask); 
    sa.sa_flags = 0;

    if (sigaction(SIGINT, &sa, NULL) == -1) {
        perror("sigaction");
        return 1;
    }

    while (1) {
        sleep(1); 
        printf("Program is running...\n");
    }

    return 0;
}