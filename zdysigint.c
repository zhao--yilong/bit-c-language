#include<stdio.h>
#include<unistd.h>
#include<signal.h>

//自定义信号处理函数
void sigcb(int signum)
{
    //signum为信号值
    printf("\n");
    printf("%d\n",signum);
}

int main ()
{
  //使用signal 自定义sigint信号
  signal(SIGINT,sigcb);
  //主程序 进行循环等待
   while(1)
   {
   sleep(1);
   }
return 0;
}
