 /**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 * };
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
 /**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 *
 * 
 * @param head ListNode类 
 * @param n int整型 
 * @return ListNode类
 */
 
//遍历整个链表，看总共有多少结点
int bianli(struct ListNode* head){
    if(head==NULL){
        return 0;
    }
    int a=0;
    struct ListNode* node=head;
    while(node){
        node=node->next;
        a++;
    }
    return a;
}

struct ListNode* removeNthFromEnd(struct ListNode* head, int n ) {
    //空链表
    if(head==NULL){
        return NULL;
    }
    int num_node=bianli(head);
    //删除倒第数结点个数个，即删除第一个
    if(n==num_node){
        return head->next;
    }
    //删除倒数第一个
    if(n==1){
        struct ListNode* node=head;
        struct ListNode* node_pre=NULL;
        struct ListNode* node_next=node->next;
        while(node_next){
            node_pre=node;
            node=node_next;
            node_next=node->next;
        }
        node_pre->next=NULL;
        return head;
    }
    //排除各种特殊情况后，按个数到需要删除的，链接前后结点即可
    struct ListNode* node=head;
    struct ListNode* node_pre=NULL;
    struct ListNode* node_next=node->next;
    int m=1;
    while(m!=num_node-n+1){
        node_pre=node;
        node=node_next;
        node_next=node->next;
        m++;
    }
    node_pre->next=node_next;
    return head;
}
