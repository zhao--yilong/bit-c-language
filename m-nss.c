#include<stdio.h>

int ispr(int n)
{
	for (int i = 2;i < n;i++)
	{
		if (n % i == 0)
			return 0;
	}
	return 1;

}

int main()
{
	int m, n;
	scanf("%d %d", &m, &n);

	int num = 0;

	for (int i = m;i <= n;i++)
	{
		if (ispr(i))
		{
			printf("%2d ", i);
			num++;
			if (num%10==0)
				printf("\n");
		}
	}

	printf("\n%d\n", num);

	return 0;
}