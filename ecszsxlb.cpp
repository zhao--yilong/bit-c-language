/*
struct TreeNode {
	int val;
	struct TreeNode *left;
	struct TreeNode *right;
	TreeNode(int x) :
			val(x), left(NULL), right(NULL) {
	}
};*/
class Solution {
public:
	//phead->pre = 
	TreeNode* phead = nullptr;
	TreeNode* pre = nullptr;
    TreeNode* Convert(TreeNode* pRootOfTree) {
		//使用的是二叉树的中序遍历
		if(pRootOfTree)
		{
			Convert(pRootOfTree->left);
			//do somehting
			if(phead == nullptr)
			{
				phead = pRootOfTree;
				pre = pRootOfTree;
			}
			else {
				pre->right = pRootOfTree;
				pRootOfTree->left = pre;
				pre = pRootOfTree;
			}
			Convert(pRootOfTree->right);
		}
		return phead;
    }

	
};
