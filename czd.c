int* decrypt(int* code, int codeSize, int k, int* returnSize) {
    int *newCode = (int *)malloc(sizeof(int) * codeSize * 2);
    memcpy(newCode, code, sizeof(int) * codeSize);
    memcpy(newCode + codeSize, code, sizeof(int) * codeSize);
    int *res = (int *)malloc(sizeof(int) * codeSize);
    memset(res, 0, sizeof(int) * codeSize);
    *returnSize = codeSize;
    code = newCode;
    if (k == 0) {
        return res;
    }
    int l = k > 0 ? 1 : codeSize + k;
    int r = k > 0 ? k : codeSize - 1;
    int w = 0;
    for (int i = l; i <= r; i++) {
        w += code[i];
    }
    for (int i = 0; i < codeSize; i++) {
        res[i] = w;
        w -= code[l];
        w += code[r + 1];
        l++;
        r++;
    }
    free(code);
    return res;
}


