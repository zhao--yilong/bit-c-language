#include <iostream>
#include <cmath>
#include <cstdlib>
using namespace std;

static const int month[2][13] =
    {
        {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
        {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}};
static const int year[2] = {365, 366};

int main()
{
    int isRun = 0;
    int Y1, M1, D1, Y2, M2, D2;
    while (scanf("%4d%2d%2d", &Y1, &M1, &D1) != EOF)
    {
        scanf("%4d%2d%2d", &Y2, &M2, &D2);
        int PastDays1 = 0, PastDays2 = 0, YearGapDays = 0;
        //此处需要求出各日期,在本年内过了多久
        //以及相差的年份中过了多久

        if ((!Y1 % 4 && Y1 % 100) || !Y1 % 400)
            isRun = 1;
        else
            isRun = 0;
        for (int i = 0; i < M1; i++)
            PastDays1 += month[isRun][i];
        PastDays1 += D1;
        //以上求出了第一个日期在本年内过了多久

        if ((!Y2 % 4 && Y2 % 100) || !Y2 % 400)
            isRun = 1;
        else
            isRun = 0;
        for (int i = 0; i < M2; i++)
            PastDays2 += month[isRun][i];
        PastDays2 += D2;
        //此处求出了第二个日期在本年内过了多久

        for (int i = min(Y1, Y2); i < max(Y1, Y2); i++)
        {
            if ((!i % 4 && i % 100) || !i % 400)
                isRun = 1;
            else
                isRun = 0;
            YearGapDays += year[isRun];
        } //此处求出了算上最小年在内,年之间差了多久

        if (Y1 > Y2)
            cout << YearGapDays + PastDays1 - PastDays2 + 1;
        else
            cout << YearGapDays + PastDays2 - PastDays1 + 1;
        //此处给出结果,需要区分哪个是大年
        //需要减去小年度过的天数
        //并加上大年度过的天数
    }
    return 0;
}
