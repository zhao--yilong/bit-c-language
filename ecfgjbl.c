#include <stdio.h>
#include<stdlib.h>

struct TreeNode
{
   char val;
   struct TreeNode *left;
   struct TreeNode *right;
};

// 构建数
struct TreeNode* BinaryTreeCreate(char*a,int *n)
{
     if(a[(*n)]=='#')
     {
         (*n)++;
       return NULL;
     }
     struct TreeNode* root =(struct TreeNode*)malloc(sizeof(struct TreeNode));
     root->val=a[((*n)++)]; 
     root->left=BinaryTreeCreate(a,n);
     root->right=BinaryTreeCreate(a,n);    

     return root;
}

void BinaryTreeInOrder(struct TreeNode*root)
{
     if(root==NULL)
        return ;

     BinaryTreeInOrder(root->left);
     printf("%c ",root->val);
     BinaryTreeInOrder(root->right);

}

int main() {
    
    char a[100];
    scanf("%s",a);
    int n =0;
    struct TreeNode*root=BinaryTreeCreate(a,&n);
     //中序遍历
    BinaryTreeInOrder(root);
    return 0;
}