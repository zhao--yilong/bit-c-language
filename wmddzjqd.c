typedef struct TeamInfo {
    int scores;
    int ages;
}TEAM_INFO;

int cmp(const void *a, const void *b)
{
    TEAM_INFO *aInfo = (TEAM_INFO*)a;
    TEAM_INFO *bInfo = (TEAM_INFO*)b;
    if (aInfo->ages - bInfo->ages == 0) { //年龄相同时, 按照分数从小到大选择,这样可以把相同年龄的全部选中,贪心算法
        return aInfo->scores - bInfo->scores;
    } else {
        return aInfo->ages - bInfo->ages; //按照年龄从小到大排序
    }
}

//一名年龄较小球员的分数 严格大于 一名年龄较大的球员，则存在矛盾。同龄球员之间不会发生矛盾  -> 按照年龄从小到大排序, 如果年龄相同,则按照分数从小到大排序,这样做的目的是，只需考虑score就行，分数大直接加就行
int bestTeamScore(int* scores, int scoresSize, int* ages, int agesSize){
    if (scores == NULL||ages == NULL||scoresSize == 0) return 0; 
    if (scoresSize == 1) return scores[0];
    int sum = INT_MIN;
    int lastScore = 0;
    int *dp = malloc(sizeof(int)*scoresSize);
    memset(dp, 0, sizeof(int)*scoresSize);

    TEAM_INFO *Info = malloc(sizeof(TEAM_INFO)* scoresSize);
    memset(Info, 0, sizeof(TEAM_INFO)* scoresSize);
    for (int i = 0; i < scoresSize; i++) {
        Info[i].scores = scores[i];
        Info[i].ages = ages[i];
    }

    //通过排序将两个影响因子降为一个,只需要考虑scores
    qsort(Info, scoresSize, sizeof(Info[0]), cmp);
    int tmpSum = 0;
    dp[0] = Info[0].scores;
    for (int i = 1; i < agesSize; i++) {
        //因为是按照年龄排序的,所以直接计算满足要求的sum
        dp[i] = Info[i].scores; //最小值为这个值，就是只有自己为一对球队
        //问题转化为：求最长上升序列和最大
        for (int j = 0; j < i; j++) { //以i位置的scores值为基准进行计算序列和
            //求所有满足要求的上升序列的最大和
            if (Info[j].scores <= Info[i].scores) { //满足要求
                dp[i] = fmax(dp[i], dp[j] + Info[i].scores); //需要注意这里+的是Info[i].scores,不是Info[j].scores
            }
        }
        sum = fmax(dp[i], sum);
    }    
    return sum;
}

