#include <iostream>
#include <string>

using namespace std;

int main()
{
    string s;
    getline(cin, s);
    // rfind 成功查找返回索引（从 0 开始），否则返回 npos (size_t)(-1)
    cout << s.length() - 1 - s.rfind(' ') << endl;
}
