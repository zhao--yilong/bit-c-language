#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int main() {
	ll a,b,p,ans=1;
	cin>>a>>b>>p;
	ll base=a,bb=b;
	while(b>0) {
	//b转换为二进制与1进行位运算，例如2^11=2^8*2^2*2^1,base从1变为2，再变为4，可惜4的时候if为0，不存在，最后成8 
		if(b&1) ans=(ans*base)%p;
		base=(base*base)%p;
		b>>=1;
	}
	printf("%lld^%lld mod %lld=%lld",a,bb,p,ans); 
	return 0;
}