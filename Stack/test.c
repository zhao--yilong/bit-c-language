#define _CRT_SECURE_NO_WARNINGS 1

#include"Stack.h"

int main()
{
	Stack s;
	StackInit(&s);

	StackPush(&s, 1);
	StackPush(&s, 2);
	StackPush(&s, 3);
	StackPush(&s, 4);

	StackPop(&s);

	printf("栈顶元素为%d \n", StackTop(&s));
	printf("栈中有效元素个数为 %d \n", StackSize(&s));

	if (StackEmpty(&s))
	{
		printf("为空\n");
	}
	else
	{
		printf("不为空\n");
	}

	StackDestroy(&s);


}