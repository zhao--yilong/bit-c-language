#include<iostream>
#include<vector>
#include<algorithm>
#include<cstring>
using namespace std;
bool cmp(const string & s1,const string & s2)//自定义排序，字符串长度短的排前面
{
    return s1.size() < s2.size();
}

int main(void)
{
    vector<string> arr;
    string s;
    
    while(getline(cin,s))
        arr.push_back(s);

    sort(arr.begin(), arr.end(),cmp);
    
    int index_min,index_max;
    index_min = index_max = 0;
    
    for(int i = 0;i < arr.size();i++)//定位原文本中最短和最长字符串的位置
    {
        if(arr[index_max].size() < arr[i].size())
            index_max = i;
        if(arr[index_min].size() == arr[i].size())
            index_min = i;
    }
    
    for(int i = 0;i <= index_min;i++)
        cout << arr[i] << endl;
    for(int i = index_max;i < arr.size();i++)
        cout << arr[i] << endl;
    
    return 0;
}