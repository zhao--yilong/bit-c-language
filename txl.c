#define _CRT_SECURE_NO_WARNINGS 1
#include"txl.h"

//初始化通讯录
void CshTxl(struct TXL* t)
{
	    t->L = (struct LXR*) malloc(3 * sizeof(struct LXR));
		t->sz = 0;
		t->rongliang = 3;
}

//添加联系人
void TJlxr(struct TXL* t)
{

	if (t->sz == t->rongliang)
	{
		struct LXR *ptr =(struct LXR*) realloc(t->L, (t->rongliang + 2) * sizeof(struct LXR));
		if (ptr != NULL)
		{
			t->L = ptr;
			t->rongliang += 2;
		}
		printf("增容成功\n");
	}
	printf("请输入联系人姓名:");
	scanf("%s", t->L[t->sz].name);

	printf("请输入联系人性别:");
	scanf("%s", t->L[t->sz].sex);

	printf("请输入联系人电话:");
	scanf("%s", t->L[t->sz].dianhua);

	printf("请输入联系人住址:");
	scanf("%s", t->L[t->sz].zhuzhi);

	printf("请输入联系人年龄:");
	scanf("%d", &t->L[t->sz].nianling);

	t->sz++;
}

void XSlxr(struct TXL* t)
{
	int i = 0;
	if (t->sz == 0)
	{
		printf("抱歉 您的通讯录里面还没有人\n");
		return;
	}
	for (i = 0;i < t->sz;i++)
	{
		printf("%-9s %-9s %-9s %-9s %-9s \n", "姓名", "性别", "年龄", "电话", "住址");
		printf("%-9s %-9s %-9d %-9s %-9s \n",t->L[i].name,
			t->L[i].sex,
			t->L[i].nianling,
			t->L[i].dianhua,
			t->L[i].zhuzhi
		);
	}
}

void SClxr(struct TXL* t)
{
	printf("请输入您要删除人的姓名\n");
	char name2[30];
	scanf("%s", &name2);
	int i = 0;
	for (i = 0;i < t->sz;i++)
	{
		int j = i;
		if (strcmp(name2, t->L[i].name) == 0)
		{
			for (j = i;j < t->sz;j++)
			{
				t->L[j] = t->L[j + 1];
			}
		}
	}
	t->sz -= 1;
	printf("删除成功\n");
}

void  CZlxr(struct TXL* t)
{
	printf("请输入您要查找的姓名\n");
	char name1[30];
	scanf("%s", &name1);
	int i = 0;
	for (i = 0;i < t->sz;i++)
	{
		if (strcmp(t->L[i].name, name1) == 0)
		{
			printf("%-9s %-9s %-9s %-9s %-9s \n", "姓名", "性别", "年龄", "电话", "住址");
			printf("%-9s %-9s %-9d %-9s %-9s \n", t->L[i].name,
				t->L[i].sex,
				t->L[i].nianling,
				t->L[i].dianhua,
				t->L[i].zhuzhi
			);
			break;
		}
		if (i == t->sz)
			printf("没有叫做 %s 的联系人\n", name1);
	}
}

void XZlxr(struct TXL* t)
{
	printf("请输入您要修改的联系人姓名\n");
	char name[20];
	scanf("%s", &name);
	int i = 0;
	for (i = 0;i < t->sz;i++)
	{
		if (strcmp(t->L[i].name, name) == 0)
		{
			printf("请进行 您的修改\n");
			printf("请输入联系人姓名:");
			scanf("%s", t->L[i].name);

			printf("请输入联系人性别:");
			scanf("%s", t->L[i].sex);

			printf("请输入联系人年龄:");
			scanf("%d", &t->L[i].nianling);

			printf("请输入联系人电话:");
			scanf("%s", t->L[i].dianhua);

			printf("请输入联系人住址:");
			scanf("%s", t->L[i].zhuzhi);

			printf("修改成功\n");
			break;
		}
	}
}


void PXlxr(struct TXL* t)
{
	

		if (t->sz <= 0) {
			printf("通讯录中没有联系人，请添加!\n");
		}
		int i = 0;
		int j = 0;
		for (i = 0; i < t->sz - 1; i++)
		{
			for (j = 0; j < t->sz - i - 1; j++)
			{
				if (strcmp(t->L[j].name, (t->L[j + 1]).name) > 0)
				{
					struct LXR tmp;
					tmp = t->L[j];
					t->L[j] = t->L[j + 1];
					t->L[j + 1] = tmp;
				}
			}
			printf("排序成功！\n");
		}

}

void TCtxl(struct TXL* t)
{
	free(t->L);
	t->L = NULL;
	t->rongliang = 0;
	t->sz = 0;
}

