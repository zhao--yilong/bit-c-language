#include<stdio.h>
#include<stdlib.h>
typedef struct TreeNode
{
     char _val;
    struct TreeNode* pleft;
    struct TreeNode* pright;
}TreeNode;

TreeNode* CreateTree(char* arr,int *pi)
{
     if (arr[*pi] == '#')
     {
         return NULL;
     }
     else
     {
     TreeNode * root = (TreeNode *)malloc (sizeof(TreeNode));
         root->_val = arr[*pi] ;
         ++(*pi);
         root->pleft = CreateTree(arr,pi);
           ++(*pi);
         root->pright = CreateTree(arr,pi);
         return root;
     } 
}
void InorderTraversal(TreeNode * root)
{
    if (root == NULL)
    {
        return ;
    }
    InorderTraversal(root->pleft);
    printf("%c",root->_val);
    InorderTraversal(root->pright);
}

int main()
{
     char arr[100];
     scanf("%s",arr);
     int i = 0;
     TreeNode * root = CreateTree(arr,&i);
     InorderTraversal(root);
     return 0;
}
