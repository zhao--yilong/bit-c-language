#include<stdio.h>
#include<math.h>

int main()
{
	float a, b, c, disc, x1, x2, realpart, imapart;

	scanf("%f,%f,%f", &a, &b, &c); //获取a b c值

	printf("这个方程\n");

	//判断 a  是否合法
	if (a == 0)
		printf("不是二元一次方程\n");
	else
	{
		disc = b * b - 4 * a * c;
	}

	//有俩个相等实数根的情况
	if (disc == 0)
	{
		printf("有俩个相等的实数根%.2f", -b / (2 * a));
	}
	else if (disc > 0)//有俩个不相等的实根
	{
		x1 = (-b + sqrt(disc)) / (2 * a);
		x2 = (-b - sqrt(disc)) / (2 * a);
		printf("俩个实数根分别是%.2f  %.2f", x1, x2);
	}
	else//disc 不大于等于 0   小于0  的情况
	{
		realpart = -b / (2 * a);
		imapart = sqrt(-disc) / (2 * a);
		printf("俩个虚根为%.2f   %.2f", realpart, imapart);
	}



	return 0;
}


