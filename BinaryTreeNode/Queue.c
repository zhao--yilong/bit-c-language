#define _CRT_SECURE_NO_WARNINGS 1

#include"Queue.h"

// 初始化队列 
void QueueInit(Queue* q)
{
	assert(q);
	q->front = NULL;
	q->rear = NULL;
	q->size = 0;
}
// 队尾入队列 
void QueuePush(Queue* q, QDataType data)
{
	assert(q);
	QNode* cur = (QNode*)malloc(sizeof(QNode));
     
	if (cur == NULL)
	{
		perror("malloc ");
		exit(-1);
	}
	cur->data = data;
	cur->next = NULL;

	if (q->rear == NULL)
	{
		q->front = q->rear = cur;
	}
	else
	{
		q->rear->next = cur;
		q->rear = cur;
	}
	q->size++;
}
// 队头出队列 
void QueuePop(Queue* q)
{
	assert(q);
	assert(!QueueEmpty(q));
	if (q->front->next == NULL)
	{
		free(q->front);
		q->front = q->rear = NULL;
	}
	else
	{
		QNode* cur = q->front->next;
		free(q->front);
		q->front = cur;
	}
	q->size--;
}
// 获取队列头部元素 
QDataType QueueFront(Queue* q)
{
	assert(q);

	return q->front->data;


}
// 获取队列队尾元素 
QDataType QueueBack(Queue* q)
{
	assert(q);

	return q->rear->data;
}
// 获取队列中有效元素个数 
int QueueSize(Queue* q)
{
	assert(q);
	return q->size;
}
// 检测队列是否为空，如果为空返回非零结果，如果非空返回0 
bool QueueEmpty(Queue* q)
{
	assert(q);
	return q->front == NULL && q->rear == NULL;
}
// 销毁队列 
void QueueDestroy(Queue* q)
{
	assert(q);

	QNode* cur = q->front;
	while (cur)
	{
		QNode* del = cur;
		cur = cur->next;
		free(del);
	}

	q->front = NULL;
	q->rear = NULL;
	q->size = 0;
}
