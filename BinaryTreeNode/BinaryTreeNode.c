#define _CRT_SECURE_NO_WARNINGS 1

#include"BinaryTreeNode.h"

// 通过前序遍历的数组"ABD##E#H##CF##G##"构建二叉树
BTNode* BinaryTreeCreate(BTDataType* a, int n, int * pi)
{
	if ('#' == a[*pi])
	{
		++(*pi);
		return NULL;
	}

	BTNode * root = (BTNode *)malloc (sizeof(BTNode));
	if (root == NULL)
	{
		perror("malloc");
		return;
	}

	root->data = a[(*pi)++];

	root->left = BinaryTreeCreate(a, n, pi);
	root->right = BinaryTreeCreate(a, n, pi);

	return root;
}
// 二叉树销毁
void BinaryTreeDestory(BTNode** root)
{
	if (*root == NULL)
		return;
	
	BinaryTreeDestory((*root)->left);
	BinaryTreeDestory((*root)->right);
	free(*root);
	*root = NULL;
}
// 二叉树节点个数
int BinaryTreeSize(BTNode* root)
{
	return root == NULL ? 0 : BinaryTreeLeafSize(root->left) + BinaryTreeLeafSize(root->right) + 1; 
}
// 二叉树叶子节点个数
int BinaryTreeLeafSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}
	if (root->left == NULL && root->right == NULL)
	{
		return 1;
	}
	return BinaryTreeLeafSize(root->left) + BinaryTreeLeafSize(root->right);
}
// 二叉树第k层节点个数
int BinaryTreeLevelKSize(BTNode* root, int k)
{
	if (root == NULL)
	{
		return 0;
	}

	if (k == 1)
	{
		return 1;
	}
	return BinaryTreeLevelKSize(root->left, k - 1) +
		   BinaryTreeLevelKSize(root->right, k - 1);

}
// 二叉树查找值为x的节点
BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
{
	BTNode* node;
	if (root == NULL)
		return NULL ;
	//一开始就是 x
	if (root->data == x)
	{
		return root;
	}
	//前序遍历寻找x
   	node = BinaryTreeFind(root->left, x);
	if (node)
		return node;

    node =BinaryTreeFind(root->right, x);
	if (node)
		return node;

	//遍历完找不到返回空
	return NULL;
}
// 二叉树前序遍历 
void BinaryTreePrevOrder(BTNode * root)
{
	if (root == NULL);
	{
		return  ;
	}

	printf("%c ", root->data);

	BinaryTreePrevOrder(root->left);
	BinaryTreePrevOrder(root->right);
}
// 二叉树中序遍历
void BinaryTreeInOrder(BTNode* root)
{
	if (root == NULL)
	{
		return;
	}
	BinaryTreeInOrder(root->left);
	printf("%c ", root->data);
	BinaryTreeInOrder(root->right);
}
// 二叉树后序遍历
void BinaryTreePostOrder(BTNode* root)
{
	if (root == NULL)
	{
		return;
	}
	BinaryTreePostOrder(root->left);
	BinaryTreePostOrder(root->right);
	printf("%c ", root->data);
}
// 层序遍历
void BinaryTreeLevelOrder(BTNode* root)
{
	Queue q;
	//初始化队列
	QueueInit(&q);
	if (root)
		QueuePush(&q, root);

	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		printf("%c ", front->data);
		QueuePop(&q);

		if (front->left)
		{
			QueuePush(&q, front->left);
		}
		if (front->right)
		{
			QueuePush(&q, front->right);
		}
	}
	printf("\n");
	//销毁队列
	QueueDestroy(&q);
}
// 判断二叉树是否是完全二叉树
int BinaryTreeComplete(BTNode* root)
{
	Queue q;
	//初始化队列
	QueueInit(&q);
	if (root)
		QueuePush(&q, root);



}
