#define _CRT_SECURE_NO_WARNINGS 1

//#include<stdio.h>
////最小公倍数
//int main()
//{
//
//	int x, y;
//	scanf("%d %d", &x, &y);
//
//	int max = x > y ? x : y;
//
//	while (max%x!=0|| max%y!=0)
//	{
//		max++;
//	}
//	printf("%d", max);
//	return 0;
//}

//将一句话的单词进行倒置，标点不倒置。比如 I like beijing. 经过函数后变为：beijing. like I

#include <stdio.h>
#include <string.h>
void Reverse(char* left, char* right)
{
    while (left < right)
    {
        char temp = *left;
        *left = *right;
        *right = temp;
        ++left;
        --right;
    }
}
int main()
{
    char str[100];
    gets(str);
    //首先进行整体逆置
    Reverse(str, str + strlen(str) - 1);

    char* left = str;
    char* right = str;
    while (*left != '\0')
    {
        while (*right != ' ' && *right != '\0')
            right++;
        if (*right == ' ')
        {
            Reverse(left, right - 1);
            right++;
            left = right;
        }
        else
        {
            Reverse(left, right - 1);
            left = right;
        }
    }
    printf("%s\n", str);
    return 0;
}