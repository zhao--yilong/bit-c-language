#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int pdjs(int x)
{
	if (x % 2 == 0)
	{
		return 0;
	}
	else
	{
		return 1;
	}

}

int main()
{
	int i = 0;
	
	for (i = 0;i < 100;i++)
	{
		if (pdjs(i))
		{
			printf("%d ", i);
		}
	}

	return 0;
}