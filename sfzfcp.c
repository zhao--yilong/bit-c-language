int cmp(const void* _a, const void* _b) {
    char a = *(char*)_a, b = *(char*)_b;
    return a - b;
}

bool CheckPermutation(char* s1, char* s2) {
    int len_s1 = strlen(s1), len_s2 = strlen(s2);
    if (len_s1 != len_s2) {
        return false;
    }
    qsort(s1, len_s1, sizeof(char), cmp);
    qsort(s2, len_s2, sizeof(char), cmp);
    return strcmp(s1, s2) == 0;
}