class ObjectCounter {
private:
    static int count;
    static ObjectCounter instance;

public:
    ObjectCounter() {
        ++count;
    }
    ~ObjectCounter() {
        --count;
    }

    static int getCount() {
        return count;
    }

    // 禁止拷贝构造和赋值操作
    ObjectCounter(const ObjectCounter&) = delete;
    void operator=(const ObjectCounter&) = delete;
};

int ObjectCounter::count = 0;
ObjectCounter ObjectCounter::instance;

class MyClass {
public:
    MyClass() {
        ObjectCounter::getCount(); // 触发计数增加
    }
    ~MyClass() {
        ObjectCounter::getCount(); // 触发计数减少
    }
};