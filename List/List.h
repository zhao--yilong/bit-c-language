#include<iostream>
#include<assert.h>

using std::cin;
using std::cout;
using std::endl;

namespace lz
{
	//封装结点
	template<class _Tp>
	struct _list_node
	{
	public:
		_Tp _data;                           //数据域
		_list_node<_Tp>* _next;              //下一个数据
		_list_node<_Tp>* _prev;              //上一个数据

		_list_node(const _Tp& val = _Tp())  //初始化节点
			:_data(val)
			, _next(nullptr)
			, _prev(nullptr)
		{}

	};

	//封装迭代器
	template<class _Tp, class Ref, class Ptr>
	struct _list_iterator
	{
		typedef _list_node<_Tp>  Link_type;
		typedef _list_iterator<_Tp, Ref, Ptr>  self;
		//引用       
		typedef Ref reference;
		typedef const Ref const_reference;

		//指针
		typedef Ptr pointer;
		typedef const Ptr const_pointer;


		Link_type* _pnode;

		_list_iterator(Link_type* pnode = nullptr) //传节点构造
			:_pnode(pnode)
		{}

		_list_iterator(const self& lt)             //传对象构造
			:_pnode(lt._pnode)
		{}

	    //引用的 * -> const * ->
		reference operator*()
		{ 
			return _pnode->_data; 
		}

		pointer operator->() 
		{ 
			return &(operator*()); 
		}

		const_reference operator*()const { 
			return _pnode->_data;
		}

		const_pointer operator->()const
		{
			return &(operator*());
		}

		bool operator!=(const self& x)const 
		{ 
			return _pnode != x._pnode;
		}

		bool operator==(const self& x)const 
		{ 
			return _pnode == x._pnode;
		}

		//前置自增
		self& operator++()
		{
			_pnode = _pnode->_next;
			return *this;
		}

		//后置自增
		self operator++(_Tp)
		{
			Link_type* tmp = _pnode;
			_pnode = _pnode->_next;
			return tmp;
		}

		//前置自减
		self& operator--()
		{
			_pnode = _pnode->_prev;
			return *this;
		}

		//后置自减
		self operator--(_Tp)
		{
			Link_type* tmp = _pnode;
			_pnode = _pnode->_prev;
			return tmp;
		}

	};

	//封装结点和迭代器
	template<class _Tp>
	struct _list
	{
		typedef _list_node<_Tp>  Link_type;

		typedef _list_iterator<_Tp, _Tp&, _Tp*> iterator;

		typedef _list_iterator<_Tp, const _Tp&, const _Tp*> const_iterator;

	private:

		Link_type* _head;

		void Creatphead()//创建头指针
		{
			_head = new Link_type;  //定义一个节点 ，他的前后位置都指向自己； 
			_head->_next = _head;
			_head->_prev = _head;
		}
	public:

		//迭代器
		iterator begin()
		{
			return _head->_next;
		}

		iterator end() 
		{ 
			return _head;
		}

		const_iterator cbegin()const
		{ 
			return _head->_next;
		}

		const_iterator cend()const
		{ 
			return _head;
		}

		//无参构造
		_list()                 
		{
			Creatphead();     // 只需创建一个头节点就好了
		}

		//迭代器区间构造
		template<class InputIterator>
		_list(InputIterator first, InputIterator last)
		{
			Creatphead();

			while (first != last)
			{
				push_back(*first);
				first++;
			}
		}

		//int 不能用size_t,否则会撞！！！？？？
		//类型不一致，会调用上面的模板迭代器区间  n 个val进行构造

		_list(int n, const _Tp& val = _Tp())
		{
			Creatphead();
			while (n)
			{
				push_back(val);
				n--;
			}
		}

		//拷贝构造函数
		_list(const _list<_Tp>& lt)
		{
			Creatphead();
			/*const_iterator it = lt.cbegin();
			while (it != lt.cend())
			{
				push_back(*it);
				it++;
			}*/

			//现代写法
			_list tmp(lt.cbegin(), lt.cend()); //迭代器区间构造
			swap(tmp);
		}

		//析构函数
		~_list()
		{
			clear();                //清空节点
			delete _head;           //删除头节点
			_head = nullptr;        //头节点置空
		}

		//赋值运算符重载
		_list<_Tp>& operator=(const _list<_Tp>& lt)
		{
			_list<_Tp> tmp(lt);//拷贝构造
			      
			swap(tmp);         //对this和lt进行交换
		}

		//尾插
		void push_back(const _Tp& val)
		{
			//Link_type* new_node = new Link_type;
			//new_node->_data = val;

			
				//Link_type* tail = _head->_prev;

				//tail->_next = new_node;
				//new_node->_prev = tail;
				//new_node->_next = _head;
				//_head->_prev = new_node;


				//代码复用
				insert(end(), val);
		}

		//头插
		void push_front(const _Tp& val)
		{
			//Link_type* new_node = new Link_type;
			//new_node->_data = val;
			//Link_type* pnext = _head->_next;

		
				//_head->_next = new_node;
				//new_node->_prev = _head;
				//new_node->_next = pnext;
				//pnext->_prev = new_node;

				//代码复用
				insert(begin(), val);
		}

		//尾删
		void pop_back()
		{
			/*
				if (!empty())
				{
					Link_type* last = _head->_prev;
					Link_type* prev = last->_prev;

					prev->_next = _head;
					_head->_prev = prev;

					delete last;
				}*/

				//代码复用
			erase(--end());
		}

		//头删
		void pop_front()
		{
			/*	if (!empty())
				{
					Link_type* first = _head->_next;
					Link_type* second = first->_next;

					_head->_next = second;
					second->_prev = _head;

					delete first;
				}*/

				//代码复用
			erase(begin());
		}

		//任意位置插入一个数据
		iterator insert(iterator pos, const _Tp& val = _Tp())
		{
			Link_type* new_node = new Link_type;                  //新建一个节点

			new_node->_data = val;                                //节点赋值
			Link_type* pprev = pos._pnode->_prev;                 // 先把当前位置的前一个节点 赋值给pprev

			//插入新节点
			pprev->_next = new_node;                             
			new_node->_prev = pprev;
			new_node->_next = pos._pnode;
			pos._pnode->_prev = new_node;

			return pos++;
		}

		//int 不能用size_t,否则会撞！！！？？？
		//类型不一致，会调用上面的模板迭代器区间
		
		//任意位置插入多个数据
		void insert(iterator pos, int n, const _Tp& val)
		{
			iterator it = pos;
			while (n)
			{
				it = insert(it, val);
				n--;
			}
		}

		//任意位置插入一个迭代器区间的数据
		template<class InputIterator>
		void insert(iterator pos, InputIterator first, InputIterator last)
		{
			iterator it = pos;
			while (first != last)
			{
				it = insert(it, *first);
				first++;
			}
		}
		//任意位置删除一个数据***
		iterator erase(iterator pos)
		{
			Link_type* pdel = pos._pnode;
			Link_type* pnext = pdel->_next;
			Link_type* pprev = pdel->_prev;

			//如果为空就不用进行删除操作，同时不能删除头结点
			if (!empty() && pdel != _head)
			{
				pprev->_next = pnext;
				pnext->_prev = pprev;
				delete pdel;
			}

			return pnext;
		}

		//删除一个迭代器区间的数据[first, last)
		iterator erase(iterator first, iterator last)
		{
			iterator it = first;
			while (it != last)
			{
				it = erase(it);
			}
			return last++;
		}

		//判空
		bool empty()
		{
			return _head->_next == _head;
		}

		//计算个数
		size_t size()const
		{
			size_t count = 0;
			iterator it = begin();
			while (it != end())
			{
				count++;
				it++;
			}
			return count;
		}

		//清除  把所有内容进行删除
		void clear()
		{
			erase(begin(), end());
		}

		//交换  this和lt进行交换
		void swap(_list<_Tp>& lt)
		{
			std::swap(_head, lt._head);
		}
	};
}

//打印函数
template<class T>
void PrintCon(const lz::_list<T>& con)
{
	auto it = con.cbegin();
	while (it != con.cend())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;
}

void Test1()//构造函数
{
	//无参构造
	lz::_list<int> lt1;
	PrintCon(lt1);

	//存入多个数据
	lz::_list<int> lt2(4, 5);
	PrintCon(lt2);

	//迭代器区间
	lz::_list<int> lt3(lt2.begin(), lt2.end());
	PrintCon(lt3);

	//拷贝构造
	lz::_list<int> lt4(lt3);
	PrintCon(lt4);

	//=重载
	lz::_list<int> lt5 = lt4;
	PrintCon(lt5);

}

void Test2()//insert+erase(避免迭代器失效)
{
	lz::_list<int> lt1(4, 5);
	lz::_list<int> lt2(4, 1);

	//找到尾，插入数据
	lz::_list_iterator<int, int&, int*> it1 = lt1.end();

	//插入单个数据
	it1 = lt1.insert(it1, '!');
	it1 = lt1.insert(it1, 'a');
	PrintCon(lt1);

	//插入多个数据
	lt1.insert(lt1.begin(), 4, 6);
	PrintCon(lt1);

	//将lt2中的数据插入lt1中
	lt1.insert(it1, lt2.begin(), lt2.end());
	PrintCon(lt1);
	;
	//删除任意位置数据
	it1 = lt1.erase(it1);//此时的位置是头结点，不能删除
	it1 = lt1.erase(it1);
	PrintCon(lt1);

	//删除迭代器区间的数据[first, last)
	lt1.erase(lt1.begin(), lt1.end());
	PrintCon(lt1);

}

void Test3()//push_back+push_front+pop_back+pop_front
{
	lz::_list<int> lt1;
	PrintCon(lt1);//无数据打印

	lt1.push_back(2);
	lt1.push_back(1);
	lt1.push_front(3);
	lt1.push_front(4);
	PrintCon(lt1);

	//测试删完后的处理
	lt1.pop_back();
	//lt1.pop_back();
	//lt1.pop_back();
	lt1.pop_front();
	//lt1.pop_front();
	//lt1.pop_front();
	PrintCon(lt1);

}

int main()
{
	Test3();

	return 0;
}
