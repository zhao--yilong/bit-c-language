#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"

void menu()
{
	printf("************************\n");
	printf("******   1.play   ******\n");
	printf("******   0.eixt   ******\n");
	printf("************************\n");
}

void game()
{
	//创建棋盘
	char board[ROW][COL] = { 0 };
	//初始化棋盘
	init_board(board, ROW, COL);
	//打印棋盘
	print_board(board, ROW, COL);
	char ret = '0';//接受游戏状态 
	while (1)
	{
		//人开始下棋
		people_play(board, ROW, COL);
		print_board(board, ROW, COL);
		//判断输赢
		ret = judge_lw(board, ROW, COL);
		if (ret != 'C')
			break;
		//电脑下棋
		com_play(board, ROW, COL);
		print_board(board, ROW, COL);
		ret = judge_lw(board, ROW, COL);
		if (ret != 'C')
			break;
	}
	if (ret == '*')
	{
		printf("玩家赢了\n");

	}
	else if (ret == '#')
	{
		printf("电脑赢了\n");

	}
	else
	{
		printf("平局\n");
	}
}

int main()
{
	int input = 0;
	srand((unsigned int)time(NULL));

	do
	{
		menu();
		printf("请选择您要选择的项目\n");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("您输入的选择有误，请重新选择\n");
			break;
		}

	} while (input);

}