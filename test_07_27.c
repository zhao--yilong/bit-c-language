#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
//
// 
//递归和非递归分别实现strlen
//int  my_strlen(char* str)
//{
//	int count = 0;
//	while (*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//
//int my_dg_strlen(char* str) //递归
//{
//	if (*str == '\0')
//		return 0;
//	else
//		return 1 + my_dg_strlen(str+1);
//
//}
//
//int main()
//{
//	char ch[] = "asdfgh";
// 
//	printf("递归实现strlen得出ch的长度为 %d \n", my_dg_strlen(ch));
//
//	printf("普通实现strlen得出ch的长度为%d ", my_strlen(ch));
//	return 0;
//}

//递归和非递归分别实现求第n个斐波那契数
//
//int dgfbnq(int n)
//{
//	if (n == 1 || n == 2)
//	{
//		return 1;
//	}
//	else
//	{
//		return dgfbnq(n - 1) + dgfbnq(n - 2);
//	}
//}
//
//int fbnq(int n)
//{
//	int f1 = 1;//1//2
//	int f2 = 1;//2//3
//	int f3 = 1;//2//3
//	int i = 0;
//	for (i = 2; i < n; i++)
//	{
//		f3 = f1 + f2;
//		f1 = f2;
//		f2 = f3;
//	}
//	return f3;
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	
//	int c= dgfbnq(n);
//	int d = fbnq(n);
//	printf("递归%d\n", c);
//	printf("普通%d\n", d);
//	return 0;
//}

//编写一个函数 reverse_string(char* string)（递归实现）
//实现：将参数字符串中的字符反向排列，不是逆序打印。
//要求：不能使用C函数库中的字符串操作函数。

//int  my_strlen(char* str)
//{
//	int count = 0;
//	while (*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//
////void reverse_string(char* string)
////{
////	int left = 0;
////	int right = my_strlen(string) - 1;
////	while (left < right)
////	{
////		char tmp = *(string+left);
////		*(string+left) = *(string+right);
////		*(string+right) = tmp;
////		left++;
////		right--;
////	}
////}
//
//void  reverse_string(char* string)
//{
//	int len = my_strlen(string);
//	char tmp = *string;
//	
//	*string = *(string + len - 1);
//
//	*(string + len - 1) = '\0';
//
//	if (my_strlen(string + 1) >= 2)
//	{
//		reverse_string(string + 1);
//	}
//	*(string + len - 1) = tmp;
//}
//
//int main()
//{
//	char ch[] = "abcdef";
//
//	reverse_string(ch);
//	
//	printf("%s ", ch);
//	return 0;
//}

//将数组A中的内容和数组B中的内容进行交换。（数组一样大）

//void szjh(int arr[], int arr1[], int n)
//{
//	int i = 0;
//	for (i = 0;i < n;i++)
//	{
//		int tmp = arr[i];
//		arr[i] = arr1[i];
//		arr1[i] = tmp;
//	}
//}
//
//int main()
//{
//	int arr[] = { 1,2,3 };
//	int arr1[] = { 2,3,4 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	szjh(arr, arr1, sz);
//	int i = 0;
//	for (i = 0;i < sz;i++)
//	{
//		printf("%d ", arr[i]);
//	}
//
//	return 0;
//}

//创建一个整形数组，完成对数组的操作
//实现函数init() 初始化数组为全0
//实现print()  打印数组的每个元素
//实现reverse()  函数完成数组元素的逆置。
//要求：自己设计以上函数的参数，返回值。

//void init(int arr[],int sz)
//{
//	int i = 0;
//	for (i = 0;i < sz;i++)
//	{
//		arr[i] = 0;
//	}
//}
//
//void print(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0;i < sz;i++)
//	{
//		printf("%d ", arr[i]);
//	}
//}
//
//void reverse(int arr[], int sz)
//{
//	int left = 0;
//	int right = sz - 1;
//	int i = 0;
//	while (left<right)
//	{
//		int tmp = arr[left];
//		arr[left] = arr[right];
//		arr[right] =tmp;
//		left++;
//		right--;
//	}
//}
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	print(arr, sz);
//	reverse(arr, sz);
//	printf("\n");
//	print(arr, sz);
//	printf("\n");
//	init(arr,sz);
//	print(arr, sz);
//	return 0;
//}

//实现一个对整形数组的冒泡排序

void mppx(int arr[], int sz)
{
	int i = 0;
	for (i = 0;i < sz - 1;i++)
	{
		int j = 0;
		for (j = 0;j < sz - 1 - i;j++)
		{
			if (arr[j] > arr[j + 1])
			{
				int tmp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = tmp;
			}
		}
	}
}

void print(int arr[], int sz)
{
	int i = 0;
	for (i = 0;i < sz;i++)
	{
		printf("%d ", arr[i]);
	}
}

int main()
{

	int arr[] = { 3,7,9,1,2,6,4,5,12,16,33 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	print(arr, sz);
	mppx(arr, sz);
	printf("\n");
	print(arr, sz);
	return 0;
}