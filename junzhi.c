static inline int cmp(const void *pa, const void *pb) {//排序
    return *(int *)pa - *(int *)pb;
}

double trimMean(int* arr, int arrSize){
    qsort(arr, arrSize, sizeof(int), cmp);//排序
    double partialSum = 0;//统计累加和
    for (int i = arrSize / 20; i < (19 * arrSize / 20); i++) {
        partialSum += arr[i];
    }
    return partialSum / (arrSize * 0.9);//取平均值
}
