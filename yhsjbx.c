//题解，根据规律可发现，偶数出现的位置分别是：-1 -1 2 3 2 4 2 3 2 4 循环往复
#include<stdio.h>
int main(){
    int n,m,a[4] = {2,3,2,4};
    while(scanf("%d",&n)!=EOF){
        if(n <= 2){
            m = -1;
        }else{
            n -= 3;
            n %= 4; 
            m = a[n];
        }
        printf("%d\n",m);
    }
    return 0;
}
