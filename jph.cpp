class Solution {
public:
    vector<string> findWords(vector<string>& words) {
        string str1 = "qwertyuiop";
        string str2 = "asdfghjkl";
        string str3 = "zxcvbnm";
        vector<string> res;
        for (string word : words) {
            if (str1.find(tolower(word[0])) !=-1) {
                bool flag = true;
                for (int i = 1; i < word.length(); i++) {
                    if (str1.find(tolower(word[i]))==-1) {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                    res.push_back(word);
            } else if (str2.find(tolower(word[0]))!=-1) {
                bool flag = true;
                for (int i = 1; i < word.length(); i++) {
                    if (str2.find(tolower(word[i]))==-1) {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                    res.push_back(word);
            } else {
                bool flag = true;
                for (int i = 1; i < word.length(); i++) {
                    if (str3.find(tolower(word[i]))==-1) {
                        flag = false;
                        break;
                    }
                }
                if (flag)
                    res.push_back(word);
            }
        }
        return res;
    }
};