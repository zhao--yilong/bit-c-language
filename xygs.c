/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* findClosedNumbers(int num, int* returnSize){
    int tmpNum = num;
    int numCnt = 0;
    while (tmpNum) {
        numCnt++;
        tmpNum = tmpNum & (tmpNum - 1);
    };
    *returnSize = 2;
    int* result = (int*)malloc(sizeof(int) * (*returnSize));
    if (num == 1) {
        result[0] = 2;
        result[1] = -1;
    } else if (num == INT_MAX) {
        result[0] = -1;
        result[1] = -1;
    } else {
        int biggerNum = num + 1;
        int lessNum = num - 1;
        while (biggerNum != INT_MAX) {
            int tmpBigger = biggerNum;
            int tmpBiggerCnt = 0;
            while (tmpBigger) {
                tmpBigger = tmpBigger & (tmpBigger - 1);
                tmpBiggerCnt++;
            };
            if (tmpBiggerCnt == numCnt) {
                result[0] = biggerNum;
                break;
            } else {
                biggerNum++;
            }
        }
        while (lessNum != 0) {
            int tmpLess = lessNum;
            int tmpLessCnt = 0;
            while (tmpLess) {
                tmpLess = tmpLess & (tmpLess - 1);
                tmpLessCnt++;
            };
            if (tmpLessCnt == numCnt) {
                result[1] = lessNum;
                break;
            } else {
                lessNum--;
            }
        }

    }
    return result;
}