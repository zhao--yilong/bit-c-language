#include <stdio.h>
#include <string.h>

#define MAX_STR_LEN 1000

int main(void)
{
    char str[MAX_STR_LEN + 1] = {0};
    int strLen;
    int winLen;
    int cntGC;
    int winLeft;
    int winRight;
    int i;
    int max = 0;
    int index = 0;
    
    scanf("%s%d", str,&winLen);
    strLen = strlen(str);
    cntGC = 0;
    winLeft = 0;
    winRight = winLen - 1;
    /* 先计算出初始窗口的GC个数 */
    for (i = 0; i < winLen; i++) {
        if (str[i] == 'G' || str[i] == 'C') {
            cntGC++;
        }    
    }
    /* 保存初始的GC max数 */
    max = cntGC;
    /* 滑动窗口 */
    for (i = 0; i < strLen - winLen; i++) {
        winRight++;
        /* 划入右边窗口判断 */
        if (str[winRight] == 'G' || str[winRight] == 'C') {
            cntGC++;
        }
        /* 弹出左边窗口判断 */
        if (str[winLeft] == 'G' || str[winLeft] == 'C') {
            cntGC--; 
        }
        winLeft++;
        /* 判断最大GC，并记录index */
        if (max < cntGC) {
            max = cntGC;
            index = winLeft;
        }
    }
    printf("%.*s\n", winLen, &str[index]);
    return 0;
}