#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char *cmd1[6] = {"reset", "reset", "board", "board", "reboot", "backplane"};
char *cmd2[6]= {"no use", "board", "add", "delete", "backplane", "abort"};

void cmd_out(int index)
{
    switch(index)
    {
        case 0:
            printf("reset what\n");
            break;
        case 1:
            printf("board fault\n");
            break;
        case 2:
            printf("where to add\n");
            break;
        case 3:
            printf("no board at all\n");
            break;
        case 4:
            printf("impossible\n");
            break;
        case 5:
            printf("install first\n");
            break;
        default:
            printf("unknown command\n");
            break;
    }
}

int main()
{
    char in[28] = {'\0'};
    while(gets(in))
    {
        int len = strlen(in);
        if(len > 16)
        {
            printf("unknown command\n");
            continue;
        }
        int blank_flag = 1, pos;        //blank_flag判断是否有空格，pos空格的位置
        for(int i=0; i<len; i++)
        {
            if(in[i] == ' ')
            {
                blank_flag = 0;
                pos = i;
                break;
            }
        }
        //输入一个字串的情况
        if(blank_flag)
        {

            if(strncmp(in, cmd1[0], len) == 0)
                printf("reset what\n");
            else
                printf("unknown command\n");
        }
        //输入两个字串的情况
        else
        {
            int cnt=0, index;
            for(int i=1; i<6; i++)
            {
                if(strncmp(in, cmd1[i], pos) == 0 &&
                   strncmp(in+pos+1, cmd2[i], len-pos-1) == 0)
                {
                    cnt++;
                    index=i;
                }
            }
            if(cnt==1)
                cmd_out(index);
            else
                printf("unknown command\n");
        }
    }
    return 0;
}