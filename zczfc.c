#define MAX(a, b) ((a) > (b) ? (a) : (b))

int maxLengthBetweenEqualCharacters(char * s){
    int maxLength = -1;
    int firstIndex[26];
    int len = strlen(s);
    memset(firstIndex, -1, sizeof(firstIndex));
    for (int i = 0; i < len; i++) {
        if (firstIndex[s[i] - 'a'] < 0) {
            firstIndex[s[i] - 'a'] = i;
        } else {
            maxLength = MAX(maxLength, i - firstIndex[s[i] - 'a'] - 1);
        }
    }
    return maxLength;
}
