#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>

std::mutex mtx; // 互斥锁
std::condition_variable cond; // 条件变量
bool turn = false; // 用于指示哪个线程先执行的标志

// 线程A的函数
void threadA() {
    while (true) {
        std::unique_lock<std::mutex> lock(mtx); // 锁定互斥锁
        if (!turn) { // 如果当前不是线程A的轮次
            cond.wait(lock); // 等待条件变量的通知
            continue;
        }
        // 打印线程A的信息
        std::cout << "我是线程A" << std::endl;
        turn = false; // 切换到线程B
        cond.notify_one(); // 通知所有等待的线程
        lock.unlock(); // 解锁互斥锁
    }
}

// 线程B的函数
void threadB() {
    while (true) {
        std::unique_lock<std::mutex> lock(mtx); // 锁定互斥锁
        if (turn) { // 如果当前不是线程B的轮次
            cond.wait(lock); // 等待条件变量的通知
            continue;
        }
        // 打印线程B的信息
        std::cout << "我是线程B" << std::endl;
        turn = true; // 切换到线程A
        cond.notify_one(); // 通知所有等待的线程
        lock.unlock(); // 解锁互斥锁
    }
}

int main() {
    std::thread tA(threadA); // 创建线程A
    std::thread tB(threadB); // 创建线程B
    turn = true; // 开始时让线程A先执行
    tA.join(); // 等待线程A结束
    tB.join(); // 等待线程B结束
    return 0;
}