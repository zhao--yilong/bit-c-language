#define _CRT_SECURE_NO_WARNINGS 1

#include"Heap.h"

void TestHeap1()
{
	int array[] = { 27, 15, 19, 18, 28, 34, 65, 49, 1, 37 };
	Heap hp;
	Heapinit(&hp);
	for (int i = 0; i < sizeof(array) / sizeof(int); ++i)
	{
		HeapPush(&hp, array[i]);
	}
	HeapPrint(&hp);

	// topK 
	int k = 5;
	while (k--)
	{
		printf("%d ", HeapTop(&hp));
		HeapPop(&hp);
	}

	HeapDestory(&hp);
}
void TestHeap3()
{
	int array[] = { 27, 15, 19, 18, 28, 34, 65, 49, 25, 37 };
	Heap hp;
	HeapCreate(&hp, array, sizeof(array) / sizeof(int));
	HeapPrint(&hp);

	HeapDestory(&hp);
}
// 对数组进行堆排序
void TestHeap4()
{
	int array[] = { 27, 15, 19, 18, 28, 34, 65, 49, 25, 37 };

	HeapSort(array, sizeof(array) / sizeof(int));
	
	for (int i = 0;i < sizeof(array) / sizeof(int);i++)
	{
		printf("%d ", array[i]);
	}

}
int main()
{
	//TestHeap1();
	// TestHeap3();
	TestHeap4();
	/* TestTopk();
	 int a[] = { 0 };
	 int k = 10;
	 int n = 0;

	 PrintTopK(a,n,k);*/
}