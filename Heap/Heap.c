#define _CRT_SECURE_NO_WARNINGS 1

#include"Heap.h"
void Swap(int* a, int* b);

//堆的调整
void AdjustUp(HPDataType* a, int child)
{
	//父亲 = 孩子-1除2
	int parent = (child - 1) / 2;
	//while (parent >= 0)  不好
	while (child > 0)
	{
		//如果孩子小于父亲
		if (a[child] dx a[parent])
		{
			//交换父亲和孩子
			Swap(&a[child], &a[parent]);
			//孩子=父亲
			child = parent;
			parent = (child - 1) / 2;//父亲= 父亲的父亲
		}
		else
		{
			break;
		}
	}
}
//向下调整法
void AdjustDown(HPDataType* a, int size, int pareat)
{
	//孩子  = 父亲*2+1
	int child = pareat * 2 + 1;

	while (child < size)
	{
		if (child+1 < size && a[child + 1] dx a[child])
		{
			++child;
		}
		// 1、孩子大于父亲，交换，继续向下调整
		// 2、孩子小于父亲，则调整结束
		if (a[child] dx a[pareat])
		{
			Swap(&a[child], &a[pareat]);
			pareat = child;
			child = pareat * 2 + 1;
		}
		else
		{
			break;
		}
	}
}
// 堆的插入


// 堆的构建
void HeapCreate(Heap* hp, HPDataType* a, int n)
{
	assert(hp);
	hp->a = (HPDataType*)malloc(sizeof(HPDataType) * n);
	if (hp->a == NULL)
	{
		perror("realloc fail");
		exit(-1);
	}
	memcpy(hp->a, a, sizeof(HPDataType) * n);
	hp->size = hp->capacity = n;

	// 建堆算法
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(hp->a, n, i);
	}
}
//堆的初始化
void Heapinit( Heap*hp)
{
	hp->a = NULL;
	hp->capacity = hp->size = 0;
}
//堆的打印
void HeapPrint(Heap* php)
{
	assert(php);
	for (int i = 0; i < php->size; ++i)
	{
		printf("%d ", php->a[i]);
	}
	printf("\n");
}

// 堆的销毁
void HeapDestory(Heap* hp)
{
	assert(hp);
	free(hp->a);
	hp->a = NULL;
	hp->capacity = hp->size = 0;
}

void Swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}


void HeapPush(Heap* hp, HPDataType x)
{
	assert(hp);
	//扩容
	if (hp->capacity == hp->size)
	{
		int newsize = hp->capacity == 0 ? 4 : (hp->capacity * 2);
		HPDataType* tmp = (HPDataType*)realloc(hp->a, newsize * sizeof(HPDataType));
		if (tmp == NULL)
		{
			perror("realloc");
			exit(-1);
		}
		hp->a = tmp;
		hp->capacity = newsize;
	}
	    //插入数据
		hp->a[hp->size] = x;
		hp->size++;
		//调整数据
		AdjustUp(hp->a, hp->size - 1);
}

// 堆的删除
void HeapPop(Heap* hp)
{
	assert(hp);
	//1.先交换第一个和最后一个数据
	Swap(&hp->a[0], &hp->a[hp->size - 1]);

	//2.删除
	hp->size--;
	//3.向下调整法调整堆
	AdjustDown(hp->a, hp->size, 0);
}
// 取堆顶的数据
HPDataType HeapTop(Heap* hp)
{
	assert(hp);
	assert(hp->size > 0);

	return hp->a[0];
}
// 堆的数据个数
int HeapSize(Heap* hp)
{
	assert(hp);
	return hp->size;
}
// 堆的判空
int HeapEmpty(Heap* hp)
{
	return hp->size == 0;
}

// TopK问题：找出N个数里面最大/最小的前K个问题。
// 比如：未央区排名前10的泡馍，西安交通大学王者荣耀排名前10的韩信，全国排名前10的李白。等等问题都是Topk问题，
// 需要注意：
// 找最大的前K个，建立K个数的小堆
// 找最小的前K个，建立K个数的大堆

// 寻找并打印topk
void PrintTopK(int* a, int n, int k)
{

	FILE* pf = fopen("data.txt", "r");
	if (pf == NULL)
	{
		perror("fopen");
		return;
	}
	int min[10] = {0};
	for (int i = 0;i < k;i++)
	{
		fscanf(pf, "%d\n", &min[i]);
	}

	//1.创建一个小堆
	for (int i = (k - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(min, k, i);
	}

	int val = 0;
	while(fscanf(pf,"%d",&val) != EOF)
	{
		if (min[0] < val)
		{
			min[0] = val;
			AdjustDown(min, k, 0);
		}
	}

	for (int i = 0;i < k;i++)
	{
		printf("%d\n", min[i]);
	}

	fclose(pf);

}
//创建文件的数据
void TestTopk()
{
	//生成随机数种子
	int a = rand((unsigned int)time(NULL));

	FILE* pf = fopen("data.txt", "w");
	if (pf == NULL)
	{
		perror("fopen");
		return;
	}
	int k = 1000;
	for (int i = 0;i < k;i++)
	{
		int sum = rand() % 10000;//生成随机数
		fprintf(pf, "%d\n", sum);
	}
}

// 对数组进行堆排序
void HeapSort(int* a, int n)
{
	//1.创建一个大堆
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(a, n, i);
	}

   //最大的跟最后的数交换
	n--;
	while (n > 0)
	{
		Swap(&a[n], &a[0]);
		n--;
		AdjustDown(a, n, 0);
	}

}
