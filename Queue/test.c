#define _CRT_SECURE_NO_WARNINGS 1

#include"Queue.h"  

int main()
{
	Queue q;
	QueueInit(&q);

	printf("%d", !QueueEmpty(&q));

	QueueDestroy(&q);
	return 0;
}