#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

//写一个宏，计算结构体中某变量相对于首地址的偏移，并给出说明
//考察：offsetof宏的实现
//#define OFFSETOF(type,member) (size_t)(&((type*)0)->member) 
//                               
//struct Stu
//{
//	char name[20];
//	int age;
//	char dianhua[30];
//};
//
//int main()
//{
//	printf("%u\n",OFFSETOF(struct Stu , name));
//	printf("%u\n",OFFSETOF(struct Stu, age));
//	return 0;
//}

#define CHANGE(num) ((((num) & 0xaaaaaaaa) >> 1) | (((num)& 0x55555555) << 1))  

int main()
{
	int num = 0;
	scanf("%d", &num);
	printf("%d\n", CHANGE(num));
	return 0;
}