class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     * 
     * @param array int整型vector 
     * @return int整型
     */

    int FindGreatestSumOfSubArray(vector<int> array) 
    {
        int ret = 0;
        int tmp = 0;
        for (const int k : array) {
            if (tmp + k < 0) {
                tmp = 0;
            }
            else {
                tmp += k;
            }
            ret = max(ret, tmp);
        }
        //ret等于0，证明数组都是负数
        if (ret != 0)
            return ret;
        return *max_element(array.begin(), array.end());//返回范围内最大的元素
    }
};



