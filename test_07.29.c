#define _CRT_SECURE_NO_WARNINGS 1
//
//#include<stdio.h>
//
//int main()
//{
//	int a = 2;
//	int b = 5;
//	a = a ^ b;
//	b = a ^ b;
//	a = a ^ b;
//	printf("a=%d,b=%d", a, b);
//	return 0;
//}

////方法1
//#include <stdio.h>
//int main()
//{
//	int num = 10;
//	int count = 0;//计数
//	while (num)
//	{
//		if (num % 2 == 1)
//			count++;
//		num = num / 2;
//	}
//	printf("二进制中1的个数 = %d\n", count);
//	return 0;
//}
//思考这样的实现方式有没有问题？
//方法2：
//#include <stdio.h>
//int main()
//{
//	int num =  10;
//	int i = 0;
//	int count = 0;//计数
//	for (i = 0; i < 32; i++)
//	{
//		if (num & (1 << i))
//			count++;
//	}
//	printf("二进制中1的个数 = %d\n", count);
//	return 0;
//}
////思考还能不能更加优化，这里必须循环32次的。
////方法3：
#include <stdio.h>

int NumberOf1(int n) 
{
	int count = 0;
	while (n)
	{
		count++;
		n = n & (n - 1);
	}
	printf("二进制中1的个数 = %d\n", count);
}

int main()
{
	int num = -1;
	NumberOf1(num);
	return 0;
}
//这种方式是不是很好？达到了优化的效果，但是难以想到。