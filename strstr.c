//模拟实现strstr
#include<stdio.h>
#include<string.h>

int isbh(char* s, char* t)
{
	char* p = s;
	char* q = t;

	while (*s != '\0')
	{
		while (*q != '\0')
		{
			if (*q == *p)
			{
				q++;
				p++;
			}
			else
				break;
		}
		if (*q == '\0')
			return 1;
		s++;
		p = s;	
		q = t;
	}
	return 0;
}

int main()
{
	char s[1000];
	char t[100];
	scanf("%s", s);
	scanf("%s", t);

	if (isbh(s, t))
		printf("shi ");
	else
		printf("bushi ");

	return 0;
}

