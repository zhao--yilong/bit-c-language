#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

//建立表头结点表 
vector<int> adjacency[20000];
int book[20000];

void dfs(int cur){
    
    cout<<cur<<" ";
    book[cur]=1;

    //遍历链表中的结点 
    int length=adjacency[cur].size();
    for(int i=0;i<length;i++){
        if(book[adjacency[cur][i]]==0){
            dfs(adjacency[cur][i]);
        }
    }
}

int main(){
    int vertex,edge,vertex1,vertex2;
    cin>>vertex>>edge;
    
    //输入边 
    for(int i=0;i<edge;i++){
        cin>>vertex1>>vertex2;
        adjacency[vertex1].push_back(vertex2);
    }
    
    //给邻接表的每个链表的结点排序 
    for(int i=0;i<vertex;i++){
        sort(adjacency[i].begin(),adjacency[i].end());
    }
    
    //遍历每个结点 
    for(int i=0;i<vertex;i++){
        if(book[i]==0){
        dfs(i); 
        }
    }

    return 0;
}