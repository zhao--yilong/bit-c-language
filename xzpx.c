//选择排序
#include<stdio.h>

int main()
{
	int i, j ,k;
	int arr[10];

	//输入数组元素
	for (i = 0;i < 10;i++)
	{
		scanf("%d", &arr[i]);
	}
	
	//利用选择排序法排序
	for (i = 0;i < 9;i++)
	{
		k = i;
		for (j = i + 1;j < 10;j++)
		{
			if (arr[k] > arr[j])
				k = j;
		}

		int tmp = arr[i];
		arr[i] = arr[k];
		arr[k] = tmp;
	}

	//打印数组
	for (i = 0;i < 10;i++)
	{
		printf(" %d ", arr[i]);
	}
	return 0;
}