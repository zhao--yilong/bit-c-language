int maximumDifference(int* nums, int numsSize)
{
    int max=0;
    int i =0;
    int j =0;
    int z= 0;
    
    for(i=0;i<numsSize;i++)
    {
        for(j=1;j<numsSize;j++)
        {
            if(j>i)
            {
                if(nums[j]>nums[i]) 
                {
                     z =nums[j]-nums[i];
                        if(z>max)
                        max=z;
                }
            }
        }
    }
    
    if(max ==0)
    return -1;
    
    return max;
}