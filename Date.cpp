﻿#define _CRT_SECURE_NO_WARNINGS 1
#include"Date.h"

int Date:: GetMonthDay(int year, int month)
{
    int arr[12] = { 30,28,31,30,31,30,31,31,30,31,30,31 };

    if (month == 2 && year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
    {
        return 29;
    }
    else
    {
        return arr[month - 1];
    }
}

void Date::Print() const
{
    cout << _year << "/" << _month << "/" << _day << endl;
}

Date:: Date(int year, int month , int day )
{
    if (month > 0 && month < 13
        && (day > 0 && day <= GetMonthDay(year, month)))
    {
        _year = year;
        _month = month;
        _day = day;
    }
    else
    {
        cout << "ڷǷ" << endl;
    }
}

bool Date:: operator>(const Date& d)
{
    if (_year > d._year)
    {
        return true;
    }
    else if (_year = d._year&& _month > d._month )
    {
        return true;
    }
    else if (_month = d._month && _day > d._day)
    {
        return true;
    }
    else
        return false;
}

bool Date::operator < (const Date& d)
{
    if (_year < d._year)
    {
        return true;
    }
    else if (_year = d._year && _month < d._month)
    {
        return true;
    }
    else if (_month = d._month && _day < d._day)
    {
        return true;
    }
    else
        return false;
}

bool Date:: operator <= (const Date& d)
{
    if (*this > d)
    {
        return false;
    }
    else
    {
        return true;
    }
}

bool Date:: operator >= (const Date& d)
{
    if (*this < d)
    {
        return false;
    }
    else
    {
        return true;
    }


}

bool Date:: operator==(const Date& d)
{
    if (_year == d._year && d._month == _month && d._day == _day)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Date:: operator != (const Date& d)
{
    if (*this == d)
    {
        return false;
    }
    else
    {
        return true;
    }
}

Date& Date::operator+=(int day)
{
    if (day < 0)
    {
        *this -= -day;
        return *this;
    }
    _day += day;
    while (_day > GetMonthDay(_year, _month))
    {
        _day -= GetMonthDay(_year, _month);
        _month++;
        if (_month == 13)
        {
            ++_year;
            _month = 1;
        }
    }
    return *this;
}

Date& Date::operator-=(int day)
{
    if (day < 0)
    {
        *this += -day;
        return *this;
    }

    _day -= day;
    while (_day <= 0)
    {
        --_month;
        if (_month == 0)
        {
            --_year;
            _month = 12;
        }

        _day += GetMonthDay(_year, _month);
    }

    return *this;
}

Date Date::operator+(int day) 
{
    Date tmp(*this);

    tmp += day;

    return tmp;
}

Date Date:: operator-(int day)
{
    Date tmp(*this);

    tmp -= day;

    return tmp;
}

// 前置++

Date& Date:: operator++()
{
    *this += 1;
    return *this;
}



// 后置++

Date Date:: operator++(int)
{
    Date tmp(*this);
    *this += 1;
    return tmp;
}



// 后置--

Date Date:: operator--(int)
{
    Date tmp(*this);
    *this -= 1;
    return tmp;
}



// 前置--

Date& Date:: operator--()
{
    *this -= 1;
    return *this;
}

int Date:: operator-(const Date& d)
{
    Date max = *this;
    Date min = d;
    int flag = 1;

    if (*this < d)
    {
        max = d;
        min = *this;
        flag = -1;
    }

    int n = 0;
    while (min != max)
    {
        ++min;
        ++n;
    }

    return n * flag;



}



