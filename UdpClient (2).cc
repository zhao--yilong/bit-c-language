#include <iostream>
#include<cstdio>
#include<string>
#include<WinSock2.h>
#include<thread>
#include<Windows.h>

#pragma warning(disable:4996)
#pragma comment(lib, "Ws2_32.lib")

uint16_t serverport = 8888;
std::string serverip = "39.104.209.46";


int main() {
    
    WSADATA wsd;
    WSAStartup(MAKEWORD(2, 2), &wsd);

    struct sockaddr_in server;
    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_port = htons(serverport);
    server.sin_addr.s_addr = inet_addr(serverip.c_str());
    
    SOCKET sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd == SOCKET_ERROR)
    {
        std::cout << "套接字错误" << std::endl;
        return 1;
    }
    std::string message;
    char buffer[1024];
    while (true)
    {
        std::cout << "Please Enter# ";
        std::getline(std::cin, message);
        // 我们要发给谁呀？server
        sendto(sockfd, message.c_str(), (int)message.size(), 0, (struct sockaddr*)&server, sizeof(server));
            //收消息
            struct sockaddr_in temp;
            int len = sizeof(temp);
            int m = recvfrom(sockfd, buffer, sizeof(buffer) - 1, 0, (struct sockaddr*)&temp, &len); // 一般建议都是要填的.
            if (m > 0)
            {
                buffer[m] = 0;
                std::cout <<"server echo#" << buffer << std::endl;
            }
    }


    closesocket(sockfd);
    WSACleanup();
    return 0;
}