#include<string.h>
#include<ctype.h>
int pos=0;
int result(char str[]){
    int len=strlen(str);
    int stack[100]={0};
    int num=0;
    int top=-1;
    char flag='+';
    while(pos<len){
        if(str[pos]=='{'||str[pos]=='['||str[pos]=='('){
            pos++;
            num=result(str);
        }
        while((pos<len)&&isdigit(str[pos])){
            num=num*10+(str[pos]-'0');
            pos++;
        }
        switch(flag){
            case '+':stack[++top]=num;break;
            case '-':stack[++top]=-num;break;
            case '*':stack[top]*=num;break;
            case '/':stack[top]/=num;break;   
        }
        if(str[pos]=='}'||str[pos]==']'||str[pos]==')'){
            pos++;
            break;
        }
        num=0;
        flag=str[pos];
        pos++;
    }
    int plus=0;
    for(int i=0;i<=top;i++)
        plus+=stack[i];
    return plus;
}
int main(){
    char strr[100]={'\0'};
    scanf("%[^\n]",strr);
    int computer=result(strr);
    printf("%d",computer); 
}
