#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

int main()
{
	int n = 0;
	int interval = 0;
	scanf("%d %d", &n, &interval);//从键盘输入n和inter的值

	int arr[1000] = { 0 };//容器存放1-n 人数位置

	int count = 0;//计数
	int i = 0;
	int j = n;//剩下的人数
	int z = 0;

	for (i = 0;i <= n;i++)//确定每人位置  1—n
	{
		arr[i] = i+1;
	}

	//只要不只剩下一个人,那么就重复报数
	while (j > 1)
	{
		 z = 0;
		//遍历整个数组,重复报数,直到一轮结束
		while (z < n)
		{
			if (arr[z] != 0)
			{
				count++;
				//报到interval重头报数
				if (count == interval)
				{
					arr[z] = 0;//退出的人以0为标识
					j--;
					count = 0;
				}
			}
			z++;
		}
	}
	//寻找最后剩下的人
	for (i = 0;i < n;i++)
	{
		if (arr[i] != 0)
		{
			printf("最后剩下的是原来的%d号\n", arr[i]);
		}
	}

	return 0;
}