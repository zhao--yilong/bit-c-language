#include<stdio.h>

int main(int argc, char const *argv[])
{
    int in_num = 0;

    while (scanf("%d", &in_num) != EOF)
    {
        int snum[in_num];
        snum[0] = 1;
        // 1. 第一步给首行赋值， 规则是an = a(n-1) + n
        for (int i = 1; i < in_num; i++)
        {
            snum[i] = i + 1 + snum[i - 1];
        }
        // 2. 打印首行
        for (int i = 0; i < in_num ; i++)
        {
            printf("%d ", snum[i]);
        }
        printf("\n");
        //3. 循环打印剩余行 规则：第二行的元素为上一行元素去掉第一个，再减去1即可
        for (int i = in_num-1; i > 0; i--)
        {
            for (int j = 0; j < i; j++)
            {
                printf("%d ", snum[j+1] - 1 );
                snum[j] = snum[j+1] - 1; //将下一列数据写入数组中
            }
            printf("\n");
        }
    }
    
    return 0;
}

