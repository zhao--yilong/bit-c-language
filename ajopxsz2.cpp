class Solution {
public:
    vector<int> sortArrayByParityII(vector<int>& nums)
{
    vector<int> v;
    v.resize(nums.size());
    int j = 0;
    int n = 1;
    for (int i = 0;i < nums.size();i++)
    {
        //偶
        if (nums[i] % 2 == 0)
        {
            v[j] = nums[i];
            j += 2;
        }
        else
        {
            v[n] = nums[i];
            n += 2;
        }
    }
    return v;
}
};