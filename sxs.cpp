#include <iostream>
#include <cstdio>
using namespace std;
bool check(int i){
    if(i == 5 || i == 6){
        return true;
    }
    if(i >= 10){
        if((i * i) % 100 == i){
            return true;
        }
    }
    return false;
}
int main(){
    int n;
    while(~scanf("%d", &n)){
        if(check(n)){
            printf("Yes!\n");
        }else{
            printf("No!\n");
        }
    }
    return 0;
}