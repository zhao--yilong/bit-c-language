#define _CRT_SECURE_NO_WARNINGS 1
#include"LBC_link.h"

ListNode* BuyListNode(LTDataType x)
{
	ListNode* node = (ListNode*)malloc(sizeof(ListNode));
	
	if (node == NULL)
	{
		perror("maollc");
		exit(-1);
	}

	node->data = x;
	node->next = NULL;
	node->prev = NULL;
	return node;
}

ListNode* ListCreate()
{
	//创建哨兵位
	ListNode* phead = BuyListNode(-1);

	phead->next = phead;
	phead->prev = phead;

	return phead;

}
//尾插
void ListPushBack(ListNode* pHead, LTDataType x)
{
	assert(pHead);
	ListNode* cur = BuyListNode(x);

	ListNode* tail = pHead->prev;
	tail->next = cur;
	cur->prev = tail; 

	cur->next = pHead;
	pHead->prev = cur;

}
//打印
void ListPrint(ListNode* pHead)
{
	ListNode* cur = pHead->next;

	while (cur!= pHead)
	{
		printf("%d ", cur->data);
		cur = cur->next;
	}
}

//查找
ListNode* ListFind(ListNode* pHead, LTDataType x)
{
	ListNode* newhead = pHead->next;

	while (newhead->next!=pHead)
	{
		if (newhead->data == x)
		{
			return newhead;
		}
		newhead = newhead->next;
	}
	return NULL;
}


// 双向链表在pos的前面进行插入
void ListInsert(ListNode* pos, LTDataType x)
{
	assert(pos);
	ListNode* newhead = BuyListNode(x);
	//保存pos之后的位置
	ListNode* cur = pos->next;
	pos->next = newhead; 
	newhead->next = cur;
	newhead->prev = pos;
}

// 双向链表删除pos位置的节点
void ListErase(ListNode* pos)
{
	ListNode* cur = pos->next;
	ListNode* tail = pos->prev;

	tail->next = cur;
	cur->prev = tail;

	free(pos);

}

// 双向链表尾删
void ListPopBack(ListNode* pHead)
{
	ListNode* newhead = pHead;
	 ListNode*cur= newhead->prev->prev;

	 free(pHead->prev);

	 newhead->prev = cur;
	 cur->next = newhead;

}

// 双向链表头插
void ListPushFront(ListNode* pHead, LTDataType x)
{
	//创建新的
	ListNode* newhead = BuyListNode(x);

	ListNode*cur = pHead->next;

	newhead->next = cur;
	newhead->prev = pHead;
	cur->prev = newhead;

	pHead->next = newhead;

}
// 双向链表头删
void ListPopFront(ListNode* pHead)
{
	ListNode* cur = pHead->next->next;

	free(pHead->next);
	cur->prev = pHead;
	pHead->next = cur;
}