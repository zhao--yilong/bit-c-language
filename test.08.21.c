#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

//int majorityElement(int* nums, int numsSize)
//{
//    int sz = numsSize / 2;
//    int i = 0;
//    int j = 0;
//    int a = 0;
//    for (i = 0;j < numsSize;i++)
//    {
//        int sum = 0;
//        a = nums[i];
//        for (j = 0;j < numsSize;j++)
//        {
//            if (nums[j] == a)
//            {
//                sum++;
//            }
//        }
//        if (sum > sz + 2)
//        {
//            return a;
//        }
//    }
//    return a;
//}
int majorityElement(int* nums, int numsSize)
{
    int count = 0;
    int sum = nums[0];
    int i = 0;
    for (i = 0;i < numsSize;i++)
    {
        if (sum == nums[i])
        {
            count++;
        }
        else
        {
            count--;
            if (count <= 0)
            {
                sum = nums[i + 1];
            }
        }
    }
    return sum;
}
int main()
{
    int arr[3] = { 6,5,5 };
    int c = majorityElement(arr, 3);
    printf("%d",c);
	return 0;
}