#include <stdio.h>
int main(void)
{
    int n;
    while(scanf("%d",&n)!=EOF)
    {
        int i,m,b;
        int cnt=0;
        for(i=1;i<=n;i++)
        {
            m=i;
            while(m)
            {
                b=m%10;
                m=m/10;
                if(b==7||m==7||i%7==0){
                    cnt++;
                    break;
                }
            }
        }
        printf("%d\n",cnt);
    }
}