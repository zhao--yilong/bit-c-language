#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

#include<stdlib.h>

struct node {
	char c;
	struct node* next;
};

main()
{
	struct node* head = NULL, * p = NULL, * q, * t;

	char* s = "The shining C";
	while (*s)
	{
		q = (struct node*)malloc(sizeof(struct node));

		q->c = *s;
		q->next = NULL;

		if (head == NULL)
			head = q;
		else
			p->next = q;
		p = q;
		s++;
	}

	p = q = head;

	while (q != NULL)
	{
		if (!(q->c >= 'A' && q->c <= 'Z'))
		{
			if (q == head)
				head = q->next;
			else
				p->next = q->next;
			t = q;
			q = q->next;
			free(t);
		}
		else
		{
			p = q;
			q = q->next;
		}
	}

	q = head;
	while (q != NULL)
	{
		putchar((*q).c);
		q = q->next;
	}


}