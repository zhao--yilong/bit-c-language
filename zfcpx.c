#include <stdio.h>
#include <string.h>
//字典顺序排列(AaBb - Zz，0-9顺序)
//冒泡排序 str flag  strcmp
//strcmp  ASCII顺序比较
//strcmp()首先将s1 第一个字符值减去s2 第一个字符值，若差值为0 则再继续比较下个字符
//若差值不为0 则将差值返回a
int main()
{
	int n;
	scanf("%d", &n);
	char str[n][101];
	for (int i = 0; i < n; i++)
		scanf("%s", str[i]);
	char tmp[101];
	int flag;
	for (int j = n - 1; j >= 1; j--)
	{
		flag = 0;
		for (int i = 1; i <=j; i++)
		{
			if (strcmp(str[i - 1], str[i]) > 0)
			{
				strcpy(tmp, str[i - 1]);
				strcpy(str[i - 1], str[i]);
				strcpy(str[i], tmp);
				flag = 1;
			}
		}
		if (flag == 0)
			break;
	}

	for (int i = 0; i < n; i++)
		printf("%s\n", str[i]);

	return 0;
}

