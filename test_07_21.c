#define _CRT_SECURE_NO_WARNINGS 1

//#include<stdio.h>
//
//int main()
//{
//	int i, j, ret, sum=0;
//	for (i = 1;i <= 10;i++)
//	{
//		ret = 1;
//		for (j = 1;j <=i;j++)
//		{
//			ret *= j;
//		}
//		sum += ret;
//	}
//	printf("%d ", sum);
//
//	return 0;
//}
//
//#include<stdio.h>
//
//int main()
//{
//	int i, j, ret, sum = 0;
//	for (i = 1;i <= 3;i++)
//	{
//    	ret *= i;
//		sum += ret;
//	}
//	printf("%d ", sum);
//
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	printf("%d", 1 % 3);
//	return 0;
//}

//1/1-1/2+1/3-1/4+1/5 …… + 1/99 - 1/100 的值，打印出结果

//#include<stdio.h>
//int main()
//{
//	int  i = 1;
//	float sum = 0;
//	for (i = 1;i < 101;i++)
//	{
//		if (i % 2 == 0)
//		{
//			sum -= (1.0 / i);
//		}
//		else
//		{
//			sum += (1.0 / i);
//		}
//	}
//	printf("%f", sum);
//	return 0;
//}
//编写程序数一下 1到 100 的所有整数中出现多少个数字9

//#include<stdio.h>
//int main()
//{
//
//	int i = 0;
//	int js = 0;
//	for (i = 0;i < 100;i++)
//	{
//		if (i % 9 == 9 || i % 10 == 9||i/10==9)
//		{
//			printf("%d ", i);
//			js++;
//		}
//	}
//	printf("\n%d", js);
//	return 0;
//}

//#include<stdio.h>
//#include<time.h>
//#include <stdlib.h>
//void menu()
//{
//	printf("******************\n");
//	printf("*****1.play  *****\n");
//	printf("*****0.exit  *****\n");
//	printf("******************\n");
//}
//
//void game()
//{
//	int ret = rand() % 100 + 1;//生成1-100之内的数字
//	int a = 0;
//	while (1)
//	{
//		printf("请输入一个0-100数字来进行猜数\n");
//		scanf("%d", &a);
//
//		if (a > ret)
//		{
//			printf("猜大了\n");
//		}
//		else if (a < ret)
//		{
//			printf("猜小了\n");
//		}
//		else
//		{
//			printf("恭喜您，猜对了\n");
//			break;
//		}
//	}
//}
//
//int main()
//{
//	int input = 0;
//	//生成随机数种子
//	srand((unsigned int) time(NULL));
//	do
//	{
//		menu();
//		printf("请输入您要选择的选项\n");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			printf("开始游戏\n");
//			game();
//			break;
//		case 0:
//			printf("退出游戏\n");
//			break;
//		default:
//			printf("输入错误，请重新输入\n");
//		}
//
//	} while (input);
//
//	return 0;
//}

//编写代码在一个整形有序数组中查找具体的某个数

//要求：找到了就打印数字所在的下标，找不到则输出：找不到。
//#include<stdio.h>
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int a = 0;
//	printf("请输入您要查找的数字\n");
//	scanf("%d", &a);
//	int sz = sizeof(arr) / sizeof(arr[0]);//数组的元素个数
//	//左右下标
//	int left = 0;
//	int right = sz-1;
//	
//	while (left <= right)
//	{
//		//int min = left + (right - left) / 2;
//		int min = (left + right) / 2;
//		if (arr[min] > a)
//		{
//			right = min - 1;
//		}
//		else if (arr[min] < a)
//		{
//			left = min + 1;
//		}
//		else
//		{
//			printf("找到了，数字下标为%d\n", min);
//			break;
//		}
//	}
//	if (left > right)
//	{
//		printf("很抱歉，没找到\n");
//	}
//	return 0;
//}

//计算 n的阶乘。
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	int n = 0;
//	int ret = 1;
//	scanf("%d", &n);
//	for (i = 1;i <= n;i++)
//	{
//		ret *= i;
//	}
//	printf("%d的阶乘为%d ", n, ret);
//	return 0;
//}
//2. 计算 1!+2!+3!+……+10!
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	int ret = 1;
//	int sum = 0;
//	for (i = 1;i <= 10;i++)
//	{
//		ret *= i;
//		sum += ret;
//	}
//	printf("%d", sum);
//	return 0;
//}

//编写代码实现，模拟用户登录情景，并且只能登录三次。（只允许输入三次密码，如果密码正确则
//提示登录成，如果三次均输入错误，则退出程序。
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1[20] = {0};
//	int i = 0;
//	for ( i = 0;i < 3;i++)
//	{
//		printf("请输入密码\n");
//		scanf("%s", arr1);
//		if (strcmp(arr1, "zylzyl") == 0)
//		{
//			printf("输入正确\n");
//			break;
//		}
//		else
//		{
//			printf("输入错误\n");
//		}
//	}
//	if (i == 3)
//	{
//		printf("三次程序错误，退出程序\n");
//	}
//
//	return 0;
//}

// 编写代码，演示多个字符从两端移动，向中间汇聚。
#include<stdio.h>
#include<Windows.h>
int main()
{
	char arr[] = { "************" };
	char arr1[] = { "xixixixixixi" };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int left = 0;
	int right = sz - 2;
     
	while (left<=right)
	{
		arr[left] = arr1[left];
		arr[right] = arr1[right];
		printf("%s\n",arr);
		Sleep(1000);
		system("cls");
		left++;
		right--;
	}
	printf("%s\n", arr);
	return 0;
}






