#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

//struct S
//{
//	char name[20];
//	int age;
//}s;
//
//int main()
//{
//
//	printf("%d", sizeof(s.name));
//	return 0;
//}

//struct S
//{
//	int a;
//	int b;
//};
//int main()
//{
//	struct S a, * p = &a;
//	a.a = 99;
//	printf("%d\n", (*p).a);
//}

//喝汽水，1瓶汽水1元，2个空瓶可以换一瓶汽水，给20元，可以多少汽水（编程实现）。

int main()
{
	    int money = 0;
		scanf("%d", &money);
		int total = money;//被喝掉的水
		int empty = money;//空瓶
		while (empty >= 2)
		{
			total += empty / 2;
			empty = empty / 2 + empty % 2;
		}
		printf("%d\n", total);
	return 0;
}