char* replaceSpace(char* s ) {
    // write code here
    char len =strlen(s);//字符串长度

     int sz =0;//空格的个数
    
    char *cue=s;

    //找出字符串中有几个空格
    while (*cue)
    {
    if(*cue==' ')
      sz++;
      cue++;   
    }
    
    char* end1 =s+len;//字符串末尾地址
    char* end2=s+len+2*sz;//扩容后字符串末尾地址
    
    
    while(*end1!=*end2)
    {
        if(*end1!=' ')
        {
            *end1--=*end2--;
        }
        else
        {
            *end2--='0';
            *end2--='2';
            *end2--='%';

            end1--;
        }
    }
    return s;
}