#define _CRT_SECURE_NO_WARNINGS 1

//递归和非递归分别实现求n的阶乘（不考虑溢出的问题）

#include<stdio.h>

//void Jcheng(int n)
//{
//	int i = 0;
//	int ret = 1;
//	for (i = 144i <= n;i++)
//	{
//		ret *= i;
//	}
//	printf("n的阶乘为%d", ret);
//}

//int Jcheng(int n)
//{
//	if (n == 1)					
//		return 1;
//	else
//	{
//		return Jcheng(n - 1) * n;
//	}
//}
// 
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);	
//	printf("n的阶乘为%d", Jcheng(n));
//	return 0;
//}
//递归方式实现打印一个整数的每一位
#include<stdio.h>

void print(int n)
{
	if (n> 9)
	{
		print(n / 10);
	}
	printf("%d ", n % 10);
}

int main()
{
	int n = 0;
	scanf("%d", &n);
	print(n);
	return 0;
}