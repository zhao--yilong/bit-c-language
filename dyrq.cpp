#include <iostream>
using namespace std;

int arr[12] = { 31,28,31,30,31,30,31,31,30,31,30,31 };
int getmon(int year, int i)
{
    if (i == 1 &&((year % 4 == 0 && year % 100 != 0 )|| year % 400 == 0))
    {
        return arr[i]+1;
    }
    return arr[i];

}

int main()
{
    int year, month = 1, day, dday, i = 0;
    while (scanf("%d %d", &year, &dday) != EOF)
    {
      while (dday - arr[i] > 0)
        {
            dday -= (getmon(year, i));
            month++;
            i++;
        }
        day = dday;
        printf("%04d-%02d-%02d", year, month, day);
    }
}