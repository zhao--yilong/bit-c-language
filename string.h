
#define _CRT_SECURE_NO_WARNINGS 1
//模拟实现string 大部分功能
#include<iostream>
#include<list>
#include<vector>
#include<assert.h>

using namespace std;

//class string
//{
//public:
//
//
//
//
//private:
//     
//    char* _str;
//    int   _size;
//    int   _capacity;
//
//};

namespace zyl      //使用自己创建的命名空间 避免和库里的冲突

{

    class string    //封装string

    {

        //friend ostream& operator<<(ostream& _cout, const bit::string& s);

        //friend istream& operator>>(istream& _cin, bit::string& s);

    public:

        typedef char* iterator;
        typedef const char* const_iterator;
        static const size_t npos = -1; //全局比较变量 -1，size_t无符号整型 -1 约等42亿
    public:
        //构造函数 初始化
      
        string(const char* str = "") //默认缺省参数为空串
            :_size(strlen(str))      //size为传入字符串大小 
        {
            _capacity = _size;       
            _capacity = _capacity == 0 ? 4 : _capacity * 2;
            _str = new char[_capacity + 1];

            strcpy(_str, str);
        }

        //拷贝构造
        string(const string& s)
            :_str(nullptr)
            ,_size(s._size)
            ,_capacity(s._capacity*2)
        {
            _str = new char[_capacity + 1];//多开一位放\0
            strcpy(_str, s._str);
        }

        //重载=
        string& operator=(const string& s)
        {
            if (this != &s)//判断是否同一个对象
            {
                _size = s._size;
                _capacity =(_size * 2);
                char * tmp = new char[_capacity + 1];
                delete[] _str;

                strcpy(tmp, s._str);
                _str = tmp;
            }

            return *this;
        }
        //析构函数
        ~string()
        {
            _size = 0;
            _capacity = 0;
            delete[]_str;
            _str = nullptr;
        }

            //////////////////////////////////////////////////////////////
            // iterator

        iterator begin()
        {
            return _str;
        }

        iterator end()
        {
            return _str + _size;
        }
            /////////////////////////////////////////////////////////////

            // modify

        void push_back(char c)
        {
            insert(_size, c);
        }

        string& operator+=(char c)
        {
            _str[_size + 1] = c;
            _str[_size + 2] = '\0';
        }

        string& append(const string& str)
        {
            if (str._size >= _capacity - _size) //判断是否需要扩容
            {
                reserve(_capacity + str._size + 1); //复用reserve
            }
            strcpy(_str + _size, str._str); //_str+size是当前字符串尾部
            _size += str._size; //更新_size
            _str[_size] = '\0'; //手动设置字符串尾部的\0
            return *this; //返回string对象
        }
        string& operator+=(const char* str)
        {
            append(str);
            return  *this;
        }
        void clear()
        {
            _size = 0;
            _str[0] = '\0';
        }
        void swap(string& s)
        {
            std::swap(_str, s._str);
            std::swap(_capacity, s._capacity);
            std::swap(_size, s._size);
        }
        const char* c_str()const
        {
            assert(_str);
            return _str;
        }

        /////////////////////////////////////////////////////////////

        // capacity

        size_t size()const
        {
            return _size;
        }

        size_t capacity()const
        {
            return _capacity;
        }
        bool empty()const
        {
            return !size;
        }
        void resize(size_t n, char c = '\0')//调整字符串大小resize
        {
            if (n > _capacity)//查看是否需要扩容   变大
            {
                reserve(n);
            }

            if (n >_size)//扩容完 赋值
            {
                for (size_t  i = _size; i < n; i++)
                {
                    _str[i]= c;
                }
            }
            _size = n;
            _str[n + 1] = '\0';
        }

        void reserve(size_t n)//扩容
        {
            if (n > _capacity)
            {
                _capacity = n;
                char* tmp = new char[_capacity + 1];
                strcpy(tmp, _str);
                delete[]_str;
                _str = tmp;
            }
        }

        // access

        char& operator[](size_t index)
        {
            return _str[index];
        }
        const char& operator[](size_t index)const
        {
            return _str[index];
        }


        /////////////////////////////////////////////////////////////

        //relational operators

        bool operator<(const string& s)
        {
            int i = 0;
            while (s._str[i] == _str[i])
            {
                i++;
            }

            if (s._str[i] < _str[i])
            {
                return true;
            }
            else
            {
                return false;
            }


        }

        bool operator<=(const string& s)
        {
            int i = 0;

            while (s._str[i] == _str[i])
            {
                i++;
            }

            if (s._str[i] <= _str[i])
            {
                return true;
            }
            else
            {
                return false;
            }


        }

        bool operator>(const string& s)
        {
            return !(*this <= s);
        }

        bool operator>=(const string& s)
        {
            return !(*this < s);
        }
        bool operator==(const string& s)
        {
            return !((*this > s) && (*this < s));
        }

        bool operator!=(const string& s)
        {
            return !(*this == s);
        }



        // 返回c在string中第一次出现的位置

        size_t find(char c, size_t pos = 0) const
        {
            for (size_t sub = pos; sub < _size; ++sub) //迭代遍历查找
            {
                if (_str[sub] == c)
                {
                    return sub; //找到就返回下标
                }
            }
            return npos; //没找到返回npos
        }

        // 返回子串s在string中第一次出现的位置

        size_t find(const char* s, size_t pos = 0) const
        {
            assert(pos <= _size);

            auto sub = strstr(_str + pos, s);
            if (sub)
            {
                return (sub - _str); //返回的地址减去首地址得到对应下标
            }
            return npos; //返回空则该函数返回npos
        }

        // 在pos位置上插入字符c/字符串str，并返回该字符的位置

        string& insert(size_t pos, char c)
        {
           assert(pos <= _size); //判断下标是否合法(允许在\0尾端处进行插入)

                if (_size + 1 >_capacity) //判断是否需要扩容
                {
                    reserve(_capacity * (1.5));
                }
                memmove(begin() + pos + 1, begin() + pos, (end() - (begin() + pos))); //移动字符串
                _str[pos] = c; //将字符串c插入pos位置
                _str[++_size] = '\0'; //为了保证安全性，最后我们还是手动将字符串尾置为\0

                return *this; //返回string对象
        }


        string& insert(size_t pos, const char* str)
        {
                assert(str); //判断字符串是否为空指针
                assert(pos <= _size); //判断下标是否合法(允许在\0尾端处进行插入)

                size_t slen = strlen(str); //计算插入字符串长度
                if ((_size + slen + 1) > _capacity) //判断是否需要扩容
                {
                    reserve(_size + slen + 1);
                }

                memmove(_str + pos + slen, _str + pos, _size - pos + 1); //挪动原字符串
                strncpy(_str + pos, str, slen); //采用strncpy将字符串s拷贝到pos位置
                _size += slen - pos; //更新size
                return *this; //返回string对象
        }



        iterator serase(iterator first, iterator last)
        {
            if (first < begin() || first>end() || last <begin() || last > end()) //区间不合法则退出
            {
                cout << "iterator区间存在越界！" << endl;;
                assert(0);
                exit(-1);
            }
            //计算区间然后删除，尾指针减当前区间的尾区间可以得到减去的字符数
            memmove(first, last, end() - last);
            _size -= (last - first);
            _str[_size] = '\0';
            return first;
        }


    private:
        char* _str;//字符串存储空间首地址指针
        size_t _capacity;//当前可用容量
        size_t _size;  //当前字符数量

    };
}