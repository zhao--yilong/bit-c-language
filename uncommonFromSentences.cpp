#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
using namespace std;

class Solution {
public:
    vector<string> uncommonFromSentences(string s1, string s2) {
        unordered_map<string, int> count;
        vector<string> res;
        int start = 0;
        for (int i = 0; i < s1.size(); i++) {
            if (s1[i] == ' ') {
                count[s1.substr(start, i - start)]++;
                start = i + 1;
            }
        }
        count[s1.substr(start)]++;
        start = 0;
        for (int i = 0; i < s2.size(); i++) {
            if (s2[i] == ' ') {
                count[s2.substr(start, i - start)]++;
                start = i + 1;
            }
        }
        count[s2.substr(start)]++;
        for (auto& [word, cnt] : count) {
            if (cnt == 1) {
                res.push_back(word);
            }
        }
        return res;
    }
};

int main() {
    Solution s;
    string s1 = "this apple is sweet";
    string s2 = "this apple is sour";
    vector<string> res = s.uncommonFromSentences(s1, s2);
    for (auto& word : res) {
        cout << word << " ";
    }
    return 0;
}