class Solution {
public:
    bool lemonadeChange(vector<int>& bills) 
    {
        int five=0, ten=0;
         //遍历数组      
        for(auto e: bills)
        {
          if(e==5)
          five++;
          else if(e==10)
          {
              if(five==0)
              return false;
              five--;
              ten++;
          }
          else
          {
            if(ten&&five)
            {
                ten--;
                five--;
            }
            else if(five>=3)
            {
                five-=3;
            }
            else
            return false;
          }
        }
        return true;
    }
};