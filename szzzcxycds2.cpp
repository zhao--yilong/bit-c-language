class Solution {
public:
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     * 
     * @param arr int整型vector 
     * @param k int整型 
     * @return int整型
     */
    int foundOnceNumber(vector<int>& arr, int k) 
    {
        // write code here
        int n=0;
        int i=0,j=0;
        for(i=0;i<arr.size();i++)
        {
            for(j=0;j<arr.size();j++)
            {
                if(arr[i]==arr[j]&&i!=j)
                {
                    break;
                }
            }
            if(arr[i]!=arr[j])
            {
               n=arr[i];
               break;    
            }

        }
        return n;
    }
};