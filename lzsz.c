//字符串旋转函数
void xzzfc (int *nums,int left,int right)
{
     while (left<right)
     {
         int tmp =nums[left];
         nums[left]=nums[right];
         nums[right]=tmp;
         left++;
         right--;
     }
}

void rotate(int* nums, int numsSize, int k){

     if(k>=numsSize)
     k%=numsSize;
     
          //前n-k个数逆置
      xzzfc(nums,0,numsSize-k-1);

      //后k个数逆置
      xzzfc(nums,numsSize-k,numsSize-1);

      //整体旋转
      xzzfc(nums,0,numsSize-1);
    


}