/*
直接穷举计算，注意，1不算完全数，一直遍历到当前数的一半即可。
*/
#include<stdio.h>
int main(){
    int n = 0;
    int count = 0;
    scanf("%d",&n);
    for(int j = 2; j <=n; j++){
        int sum = 1;
        for(int i = 2; i <= j/2; i++){
            if(j%i==0){
                sum = sum + i;
            }
        }
        if(sum==j) count++;
    }
    printf("%d",count);
    return 0;
}