#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

int main()
{
    int n = 0;
    scanf("%d", &n);
    int arr[30][30];
    int i, j;
    for (i = 1;i <= n;i++)
    {
        for (j = 1;j <= i;j++)
        {
            if (j == 1 || j == i)
            {
                arr[i][j] = 1;
                printf("%5d", arr[i][j]);
            }
            else
            {
                arr[i][j] = arr[i - 1][j] + arr[i - 1][j - 1];
                printf("%5d", arr[i][j]);
            }
        }
        printf("\n");
    }

    return 0;
}