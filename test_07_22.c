#define _CRT_SECURE_NO_WARNINGS 1
//#include<stdio.h>
////实现一个函数，打印乘法口诀表，口诀表的行数和列数自己指定
//
////如：输入9，输出9 * 9口诀表，输出12，输出12 * 12的乘法口诀表。
//
//void cfb(int x, int y)
//{
//	int i, j;
//	//行数
//	for (i = 1;i <= x;i++)
//	{
//		for (j = 1;j <= y - x + i;j++)
//		{
//			printf("%d*%d=%2d ", i, j, i * j);
//		}
//		printf("\n");
//	}
//}
//int main()
//{
//	int x = 0;
//	int y = 0;
//	scanf("%d %d", &x, &y);
//
//	cfb(x, y);
//
//	return 0;
//}
//
//#include<stdio.h>
//
//void jh(int* px, int* py)
//{
//	int t = *px;
//	*px = *py;
//	*py = t;
//}
//
//int main()
//{
//	int x = 0;
//	int y = 0;
//	scanf("%d %d", &x, &y);
//	printf("交换前%d %d\n", x, y);
//	jh(&x,&y);
//	printf("交换后%d %d\n", x, y);
//	return 0;
//}

//#include<stdio.h>
//
//void pdrn(int x)
//{
//	if (x % 4 == 0 && x % 100 != 0 || x % 400 == 0)
//	{
//		printf("%d年是闰年\n", x);
//	}
//	else
//	{
//		printf("%d年不是闰年\n", x);
//	}
//}
//
//int main()
//{
//	int year = 0;
//	scanf("%d", &year);
//
//	pdrn(year);
//
//	return 0;
//}

#include<stdio.h>
//实现一个函数，判断一个数是不是素数。
//利用上面实现的函数打印100到200之间的素数。
int pdss(int x)
{
	    int i=0;
		for (i = 2;i < x;i++)
		{
			if (x % i == 0)
			{
				break;
			}
		}
		if (i == x)
			return 1;
		else
			return 0;
}

int main()
{
	int j = 0;
	for (j = 100;j < 200;j++)
	{
		if (pdss(j)==1)
			printf("%d ", j);
	}
	return 0;
}