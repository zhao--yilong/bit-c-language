#include <stdio.h>

static char base[][10] = { "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
static char baseTen[][8] = { "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };
static char unit[][10] = { "", " thousand", " million" };
int main(int argc, char** argv)
{
    int input, data[3];
    while (scanf("%d", &input) != EOF) {
        unsigned char flag = 0;
        for (int i = 0; i < 3; i++) {
            data[i] = input % 1000;
            if (data[i]) flag |= 0x1 << i;
            input /= 1000;
        }

        for (int i = 2; i >= 0; i--) {
            if (data[i] / 100) { printf("%s hundred", base[data[i] / 100 - 1]); data[i] %= 100; if (data[i] % 100) printf(" and "); }
            if (data[i] >= 20) { printf("%s", baseTen[data[i] / 10 - 2]); data[i] %= 10; if(data[i]) printf(" "); }
            if (data[i]) printf("%s", base[data[i] - 1]);
            if (flag & (0x1 << i)) { printf("%s", unit[i]); flag &= ~(0x1 << i); if (flag) printf(" ");}
        }
    }
    return 0;
}
