//  使用递归法求解阶乘
//
//  Created by 有理想 on 2021/4/2.
//
#include<stdio.h>
 
int main()
{
    int f(int n) ;
    int  m;
    printf("请输入一个整数：");
    scanf("%d",&m);
    printf("%d\n", f(m));
 
    return 0;
}
 
int f(int n)
{
    int x;
 
    if(n==1||n==0)
        x=1;
 
    else
        x=n*f(n-1);    // 反复调用 int f( int n )函数然后返回出去。
 
    return x;
}
