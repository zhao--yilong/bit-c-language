#include<stdio.h>
int main()
{
    int num,count;
    num=0;
    while(scanf("%d",&num)!=EOF)
    {
        count=0;
        while(num)
        {
            num=num&(num-1);//数字在内存中，被转化为二进制。
            //若n=7,则n&(n-1)即（0111）&（0110）=0110.有几个1，去除几个1；
            count++;
        }
        printf("%d\n",count);
    }
    return 0;
}