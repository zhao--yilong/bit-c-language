#include<iostream>
#include<string>
#include<vector>
using namespace std;
/*令tag[i][j]表示以A第i个字符.B第j个字符结尾的最长公共子串的长度

	若A[i]==B[j],tag[i][j] = tag[i-1][j-1] + 1;
否则A[i][j] = 0;
这样时间和空间复杂度为O(mn)。

	改进1：计算tag下一行仅仅需要上一行的结果即可，故而空间复杂度可以降低为O(min(m,n));

	*/

class Solution {
public:
	string longestCommonSubstring(string &A, string &B) {
		if (A.empty() || B.empty())	return 0;
		unsigned int m = A.length();
		unsigned int n = B.length();
		string res;
		vector<vector<int>> tag(m + 1, vector<int>(n + 1, 0));
		for (unsigned int i = 1; i < m + 1; ++i){
			for (unsigned int j = 1; j < n + 1; ++j){
				if (A[i - 1] == B[j - 1]){
					tag[i][j] = tag[i - 1][j - 1] + 1;
					res = tag[i][j] > res.length() ? A.substr(i-tag[i][j],tag[i][j]) : res;
				}
			}
		}
		return res;
	}
};
int main() {
	string a, b;
	Solution s;
	while (cin >> a >> b) {
		if (a.length() > b.length()) swap(a, b);//题目要求，短串优先
		cout << s.longestCommonSubstring(a,b) << endl;
	}
}