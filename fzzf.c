#include<stdio.h>
#include<string.h>

int main()
{
 int n = 0;
 char s[80];
 gets(s);
 scanf("%d", &n);

 int left = 0;
 int right = strlen(s) - 1;

 int r = right;
 //先整体翻转
 while (left < r)
 {
  char tmp = s[left];
  s[left] = s[r];
  s[r] = tmp;
  left++;
  r--;
 }
 //循环停止后  r在中间  然后r左边翻转   右边翻转
 int i = r;//保存中间值
 //左边翻转
 left = 0;
 while (left < i)
 {
  char tmp = s[left];
  s[left] = s[i];
  s[i] = tmp;
  left++;
  i--;
 }
 //右边翻转
 r = r + 1;
 while (r < right)
 {
  char tmp = s[r];
  s[r] = s[right];
  s[right] = tmp;
  r++;
  right--;
 }
 puts(s);
 return 0;
}