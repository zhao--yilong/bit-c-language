#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

struct STu
{
	char name[20];
	char xh[20];
	int sxcj;
	int c;
	double pjcj;
};

int main()
{
	struct STu s[10];
	int i = 0;
	for (i = 0;i < 10;i++)
	{
		scanf("%s %s %d %d", &s[i].name, &s[i].xh, &s[i].sxcj, &s[i].c);
		s[i].pjcj = (s[i].sxcj + s[i].c)/2.0;
	}
	for (i = 0;i <10-1;i++)
	{
		int j = 0;
		for (j = 0;j < 10 - i - 1;j++)
		{
			if(s[j].pjcj < s[j+1].pjcj)
			{
				struct STu tmp = s[j];
				s[j] = s[j + 1];
				s[j + 1] = tmp;
			}
		}
	}
	for (i = 0;i < 10;i++)
	{
		printf("姓名%s 学号%s 数学成绩%d c语言成绩%d 平均成绩%lf", s[i].name, s[i].xh, s[i].sxcj, s[i].c, s[i].pjcj);
		printf("\n");
	}

	//2打印出平均分 90 分以上 和不及格的信息
	for (i = 0;i < 10;i++)
	{
		if(s[i].pjcj>90||s[i].pjcj<60)
			printf("姓名%s 学号%s 数学成绩%d c语言成绩%d 平均成绩%lf\n ", s[i].name, s[i].xh, s[i].sxcj, s[i].c, s[i].pjcj);

	}

	return 0;
}
