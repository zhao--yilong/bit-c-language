#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;

namespace zyl
{ 
// 非类型模板参数
template <size_t N>  //容量N个字节
class bitset
{
public:
	//构造函数
	bitset()
	{
		_bits.resize(N / 8 + 1, 0);//开辟最少一个字节的空间
	}
	//置位函数，将指定的位设置为1
	//因为我们用的char存储的数据，一个char为一个字节,
	void set(size_t x)
	{
		size_t i = x / 8;        //x/8 表明i是在vector中的哪一个字节当中
	                            
		size_t j = x % 8;        //j表明 在 vector 第i个字节中的第几位

		_bits[i] |= 1 << j;      //把vector 第i个字节中的第j位置1；
	}

    //复位函数，将指定位设置为0
	void reset(size_t x)
	{
		size_t i = x / 8;
		size_t j = x % 8;
		_bits[i] &= ~(1 << j);   //把vector 第i个字节中的第j位置0；
	}
	//访问函数，获取指定的位的值
	bool test(size_t x)
	{
		size_t i = x / 8;
		size_t j = x % 8;
		return _bits[i] & (1 << j);  //用&只查看指定位置为0或1 不修改
	}
private:
	vector<char> _bits;
};
}