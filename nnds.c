#include <stdio.h>
//#include <stdlib.h>应该是要加上的，没加也过了
typedef struct Book{
    char name[100];
    int price;
}BOOK;
int cmp_by_price(const void*e1,const void*e2)
{
    return ((BOOK *)e1)->price-((BOOK *)e2)->price;
}
int main()
{
    int n = 0;
    scanf("%d",&n);
    BOOK arr[n];
    for (int i = 0;i < n;i++)
    {
        scanf("%s %d",&arr[i].name,&arr[i].price);
    }
    int sz = sizeof(arr)/sizeof(arr[0]);
    qsort(arr,sz,sizeof(arr[0]),cmp_by_price);//快排
    for(int i = 0;i < n;i++)
    {
        printf("%s\n",arr[i].name);
    }
    return 0;
}