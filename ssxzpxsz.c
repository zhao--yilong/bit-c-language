int search(int* nums, int numsSize, int target)
{
    int left =0;
    int right =numsSize-1;
    int mid =0;
    
    //首先确定翻转顺序  
    while (left<=right)
    {
        mid = left+((right -left)>>1);
        
        if(nums[0]<=nums[mid])
        {
            if(nums[0]<=target&&nums[mid]>target)//确定tar是否在上半区
            {
                right=mid-1;
            }
            else
            {
                left=mid+1;
            }
            
        }
        else
        {
            if(nums[0]>target&&nums[mid]<=target)//tar在下半区
            {
                left=mid+1;
            }
            else
            {
                right =mid -1;
            }
            
        }
    }
     return target ==nums[left-1]?left-1:-1;
}