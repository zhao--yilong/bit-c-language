/**
 * struct ListNode {
 *	int val;
 *	struct ListNode *next;
 * };
 */

/**
 * 
 * @param pHead ListNode类 
 * @return ListNode类
 */
struct ListNode* ReverseList(struct ListNode* pHead ) 
{
    struct ListNode *Newhead = NULL;
    struct ListNode *node;
    while(pHead != NULL)
    {
        node = pHead;
        pHead = pHead->next;
        
        node->next = Newhead;
        Newhead = node;
    }
    return Newhead;
}
