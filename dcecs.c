/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     struct TreeNode *left;
 *     struct TreeNode *right;
 * };
*/
/*
*dfs：判断两个节点是否相等
struct TreeNode* left:树节点 
struct TreeNode* right：树节点
返回值：相等返回true；不相等返回false
*/
bool dfs(struct TreeNode* left , struct TreeNode* right)
{
    if(left == NULL && right == NULL)
    {
        return true;
    }else if(left == NULL || right == NULL)
    {
        return false;
    }else if(left->val != right->val)
    {
        return false;
    }
    return dfs(left->left , right->right) && dfs(left->right , right->left);
}
/*
*isSymmetric:判断一个树是不是对称
struct TreeNode* root：根节点
返回值：对称返回true；不对称返回false
*/
bool isSymmetric(struct TreeNode* root)
{
    if(root == NULL)
    {
        return true;
    }
    return dfs(root->left,root->right);
}   