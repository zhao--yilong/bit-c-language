#include<stdio.h>
#include<stdlib.h>
//子函数用两个for循环遍历数组
//一旦出现不相等则返回0，否则继续对比直至最后一项 
int judge(int n,int a[n][n]){
	int i=0,j=0,k;
	for(i=0;i<n;i++){
		for(j=0;j<n;j++){
			if(a[i][j]!=a[j][i]){
				k=0;
				break;//只要出现不相等就跳出内层循环 
			}
			else {
				k=1;
			}
		}if(k==0){break;//跳出外层循环 
		}
 
	}
	return k;
}
//主函数 
int main(){
	int s[4][4]={//非对称矩阵 
	  {5,6,9,4},
	  {4,8,7,9},
	  {4,5,8,9},
	  {5,1,2,4},
	  };int t[4][4]={//对称矩阵 
	  {5,6,9,4},
	  {6,8,7,9},
	  {9,7,8,9},
	  {4,9,9,4},
	  };
	  int n=4;
	  printf("1矩阵为:\n5 6 9 4\n4 8 7 9\n4 5 8 9\n5 1 2 4\n");
		if(judge(n,s)){
			printf("该矩阵是对称矩阵\n"); 
		}
		else if(judge(n,s)==0){
			printf("该矩阵不是对称矩阵\n");
		}
		printf("2矩阵为:\n5 6 9 4\n6 8 7 9\n9 7 8 9\n4 9 9 4\n");
		if(judge(n,t)){
			printf("该矩阵是对称矩阵\n"); 
		}
		else if(judge(n,t)==0){
			printf("该矩阵不是对称矩阵\n");
		}
		system("pause");
		return 0;
	  
}