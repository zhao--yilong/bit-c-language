#include<stdio.h>
#include<stdlib.h>
#include<string.h>

/*  
IsBrother()
是兄弟字串，则返回1；否则返回0
*/
int IsBrother(char* aim,char* src)
{

    int map[26] = {0};
    int i = 0;
    if(strcmp(aim,src)==0 || strlen(aim)!=strlen(src))
    // 字串相等或长度不等，不是兄弟字串；
        return 0;
    else
    {
        for(i=0;src[i]!=0;i++)
        {
            map[aim[i]-'a']++;
            map[src[i]-'a']--; // 双向计数器
        }
        for(i=0;i<26;i++) // 字符集合仅包含小写字母
        {
            if(map[i]!=0) // 非全0，说明字符数量不同
                return 0;
        }
        return 1;  // 全0，字符数量完全一致，是兄弟字串
    }
}

int main()
{
    char str[1000][11];
    char sort[100][11];
    char aim[11];
    int n,k;
    int i,j;
    int cnt = 0;
    while(scanf("%d",&n)!=EOF)
    {
        for(i=0;i<n;i++)
        {
            scanf("%s",str[i]);
        }
        scanf("%s",aim);
        scanf("%d",&k);
        for(i=0;i<n;i++)
        {
            if(IsBrother(aim,str[i]))
            {
                strcpy(sort[cnt],str[i]);
                cnt++;
            }
        }
        
        printf("%d\n",cnt);
        int min;
        char tmp[11];
        if(k <= cnt) // sorting
        {
            for(i=0;i<k;i++) // k次简单排序
            {
                min = i;
                for(j=i+1;j<cnt;j++)
                {
                    if(strcmp(sort[min],sort[j])>0)
                    {
                        min = j;
                    }                    
                }
                if(min!=i)
                {
                    strcpy(tmp,sort[min]);
                    strcpy(sort[min],sort[i]);
                    strcpy(sort[i],tmp);
                }
            }
            printf("%s",sort[k-1]);
        }
    }
    
    return 0;
}
