  #include <stdio.h>    
  #include <stdlib.h>    
  #include <string.h>    
      
  int main ()    
  {    
    FILE *fd = fopen("./bite.txt","wb+");    
    if(fd==NULL){    
      perror("fopen error");    
          return -1;    
    }    
      char *str ="linux so easy";    
    int ret0=fwrite(str,strlen(str),1,fd);    //设置块大小为字符串长度，写入的块个数为一，写入成功则返回块个数为1.
    if(ret0==0){                                                                   
      perror("fwrite error");    
      return -1;    
    }    
      
    fseek(fd,0,SEEK_SET);    //fd回到起始偏移位置，准备读
    char buf[1024]={0};    
    int ret1 = fread(buf,1,1023,fd);    //设置块大小为1，返回读取到的数据个数
    if(ret1==0){    //判断文件是否读取完，fread返回值为0有两种结果分开判断
      if(feof(fd)){    
        printf("已读到文件末尾\n");    
      }    
      if(ferror(fd)){    
        perror("写入出错");    
      }    
    }    
    printf("%s\n",buf);    
    return 0;    
  } 
