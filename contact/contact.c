//函数的实现
#define _CRT_SECURE_NO_WARNINGS 1

#include"contact.h"
//静态版本初始化
//void InitContact(Contact* pc)
//{
//	pc->sz = 0;
//	//memset（）  内存设置
//	memset(pc->data, 0, sizeof(pc->data));//Contact con={0};
//}
 
void  LoadContact(Contact* pc)
{
	FILE* pf = fopen("contact.dat", "r");
	if (pf == NULL)
	{
		perror("LoadContact");
		return 0;
	}

	//读文件
	PeoInfo tmp = { 0 };
	while (fread(&tmp, sizeof(PeoInfo), 1, pf))
	{
		//考虑是否增容
		CheckCapacity(pc);
		pc->data[pc->sz] = tmp;
		pc->sz++;
	}
	

	//关闭文件
	fclose(pf);
	pf = NULL;
}



//动态版本
void InitContact(Contact* pc)
{
	pc->data =(PeoInfo*) malloc(DEFAULT_SZ*sizeof(PeoInfo));
	if (pc->data == NULL)
	{
		perror("InitContact");
		return;
	}
	pc->sz = 0;//默认初始化为0；
	pc->capacity = INC_SZ;//增量为2；
	//加载文件
	LoadContact(pc);
}

//静态版本的增加
//void ADDContact(Contact* pc)
//{
//	if (pc->sz == MAX)
//	{
//		printf("通讯录人员信息已满 无法增加\n");
//		return;//因为返回值是void 所以return 不需要返回东西  直接结束函数运行
//	}
//	//增加一个人的信息
//	printf("请输入姓名\n");
//	scanf("%s", pc->data[pc->sz].name);//pc-》data 里面数组下标为sz的地址里面的姓名
//	printf("请输入性别\n");
//	scanf("%s", pc->data[pc->sz].sex);
//	printf("请输入年龄\n");
//	scanf("%s", &(pc->data[pc->sz].age));
//	printf("请输入电话\n");
//	scanf("%s", pc->data[pc->sz].tele);
//	printf("请输入地址\n");
//	scanf("%s", pc->data[pc->sz].addr);
//
//	pc->sz++;
//	printf("增加成功\n");
//}

void CheckCapacity(Contact* pc)
{
	if (pc->sz == pc->capacity)
	{
		PeoInfo* ptr = (PeoInfo*)realloc(pc->data, (pc->capacity + INC_SZ) * sizeof(PeoInfo));
		if (ptr == NULL)
		{
			pc->data = ptr;
			pc->capacity += INC_SZ;
			printf("增容成功\n");
		}
		else
		{
			perror("ADDContact");
			printf("增加联系人失败\n");
			return;
		}
	}
}

//动态版本
void ADDContact(Contact* pc)
{
	//增容
	CheckCapacity(pc);
	//增加一个人的信息
	printf("请输入姓名\n");
	scanf("%s", pc->data[pc->sz].name);//pc-》data 里面数组下标为sz的地址里面的姓名
	printf("请输入性别\n");
	scanf("%s", pc->data[pc->sz].sex);
	printf("请输入年龄\n");
	scanf("%s", &(pc->data[pc->sz].age));
	printf("请输入电话\n");
	scanf("%s", pc->data[pc->sz].tele);
	printf("请输入地址\n");
	scanf("%s", pc->data[pc->sz].addr);

	pc->sz++;
	printf("增加成功\n");
}

void PrintContact(const Contact* pc)
{
	int i = 0;
	//打印标题
	printf("%-5s\t%-5s\t%-5s\t%-13s\t%-20s\t\n", "姓名", "年龄", "性别", "电话", "地址");
	for (i = 0;i < pc->sz;i++)
	{
		//打印数据
		printf("%-5s\t%-5d\t%-5s\t%-13s\t%-20s\t\n",
			pc->data[i].name,
			pc->data[i].age,
			pc->data[i].sex,
			pc->data[i].tele,
			pc->data[i].addr);
	}
}

static int FindByName(Contact* pc,char name[])//static 使用了这个函数之后 其他源文件就不能使用 findbyname函数 只能在本页面调用使用
{
	int i = 0;
	for (i = 0;i < pc->sz;i++)
	{
		if (strcmp(pc->data[i].name, name))
		{
			return i;
		}
	}
	return -1;//找不到
}



void DelContact(Contact* pc)
{
	char name[MAX_NAME];
	if (pc->sz == 0)
	{
		printf("通讯录为空 无需删除\n");
		return;
	}
	printf("请输入要删除人的名字\n");
	scanf("%s", name);

	//删除第一步 先找到那个人
	//先看看有没有那个人
	int  pos = FindByName(pc,name);//通过姓名查找
	if (pos == -1)
	{
		printf("要删除的人不存在\n");
		return;
	}
	//在删除
	int i = 0;
	for (i = pos;i < pc->sz - 1;i++)//最后一个元素进来不循环 虽然没有被覆盖掉  但是下面的pc—》--剪掉了那个地址访问不到了 也起到了删除的作用
	{
		pc->data[i] = pc->data[i + 1];
	}
	pc->sz--;
	printf("删除成功\n");

}

void SearchContact(Contact* pc)
{
	char name[MAX_NAME];
	printf("请输入要查找人的名字\n");
	scanf("%s", name);

	int  pos = FindByName(pc, name);//通过姓名查找
	if (pos == -1)
	{
		printf("要查找的人不存在\n");
		return;
	}
	else
	{
		printf("%-5s\t%-5s\t%-5s\t%-13s\t%-20s\t\n", "姓名", "年龄", "性别", "电话", "地址");
			//打印数据
			printf("%-5s\t%-5d\t%-5s\t%-13s\t%-20s\t\n",
				pc->data[pos].name,
				pc->data[pos].age,
				pc->data[pos].sex,
				pc->data[pos].tele,
				pc->data[pos].addr);
	}
}

void ModieyContact(Contact* pc)
{
	char name[MAX_NAME];
	printf("请输入要修改人的名字\n");
	scanf("%s", name);

	int  pos = FindByName(pc, name);//通过姓名查找
	if (pos == -1)
	{
		printf("要修改的人不存在\n");
		return;
	}
	else
	{
		printf("请输入姓名\n");
		scanf("%s", pc->data[pos].name);//pc-》data 里面数组下标为sz的地址里面的姓名
		printf("请输入性别\n");
		scanf("%s", pc->data[pos].sex);
		printf("请输入年龄\n");
		scanf("%s", &(pc->data[pos].age));
		printf("请输入电话\n");
		scanf("%s", pc->data[pos].tele);
		printf("请输入地址\n");
		scanf("%s", pc->data[pos].addr);
		
		printf("修改成功\n");
	}


}



void DestoryContact(Contact* pc)
{

	free(pc->data);
	pc->data = NULL;
	pc->capacity = 0;
	pc->sz = 0;
}


void SaveContact(Contact* pc)
{
	FILE* pf = fopen("contact.dat","w");
	if (pf == NULL)
	{
		perror("fopen");
		return 0;
	}

	//写文件
	int i = 0;
	for (i = 0;i < pc->sz;i++)
	{
		fwrite(&pc->data + i,sizeof(PeoInfo),1,pf);
	}
	//关闭文件
	fclose(pf);
	pf = NULL;

}













