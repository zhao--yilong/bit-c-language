#pragma once
// 类型的定义
//函数声明
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#define MAX_NAME 20  //ding定义姓名的长度 方便修改
#define MAX_SEX 10
#define MAX_TELE 12
#define MAX_ADDR 30
#define MAX 1000
#define DEFAULT_SZ 3  //默认的sz容量
#define INC_SZ 2    //增量
//类型的定义

typedef struct PeoInfo  //人员信息
{
	char name[MAX_NAME];//姓名
	char sex[MAX_SEX];//性别
	int age;//年龄
	char tele[MAX_TELE];//电话
	char addr[MAX_ADDR];//地址
}PeoInfo;
//
////通讯录--静态版本
//typedef struct Contact
//{
//	PeoInfo data[MAX];//存放添加进来的人的信息   //把数量定义成为一个自定义常量  方便以后更改信息
//	int  sz;//这里是记录当前通讯录中有效信息的个数
//}Contact;

//通讯录--动态版本
typedef struct Contact
{
	PeoInfo * data;//指向动态申请的空间，用来存放联系人的信息
	int  sz;//这里是记录当前通讯录中有效信息的个数
	int capacity;        //记录当前通讯录的最大容量
}Contact;

//初始化通讯录
void InitContact(Contact* pc);

//增加联系人
void ADDContact(Contact* pc);

//打印联系人
void PrintContact(const Contact*pc);

//删除联系人
void DelContact(Contact* pc);

//查找联系人
void SearchContact(Contact *pc);

//修改联系人
void ModieyContact(Contact *pc);

//销毁通讯录
void DestoryContact(Contact*pc);

//保存通讯录内容到文件
void SaveContact(Contact* pc);

//加载文件内容到通讯录
void  LoadContact(Contact*pc);

//增容
void CheckCapacity(Contact* pc);



