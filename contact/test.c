//测试通讯录的模块

//
//通讯录--静态版本
//1.通讯录能存放1000 个人的信息
//每个人的信息：
//名字+年龄+性别+电话+地址
//2.增加人的信息
//3。删除指定人的信息
//4.修改指定人的信息
//5.查找指定人的信息
//6.排序通讯里的信息
//
//版本2：
//动态增长的版本
//1.通讯录初始化后 能存放3个人的信息
//2.当我们的空间存放满的时候，我们增加2个人的信息
//
//版本三
//当通讯录退出的时候 把版本写到文件
//当通讯录打开的时候 加载文件的信息到通讯录中




#define _CRT_SECURE_NO_WARNINGS 1
#include "contact.h"

void menu()
{
	printf("*************************************\n");
	printf("*****      1.add     2 .del   *******\n");
	printf("*****      3.search  4.modify *******\n");
	printf("*****      5.sort    6.print  *******\n");
	printf("*****          0. eixt        *******\n");
	printf("*************************************\n");
}

enum Option
{
	EIXT,
	ADD,
	DEL,
	SEARCH,
	MODIFY,
	SORT,
	PRINT
};

int main()
{
	int input = 0;
	//创建通讯录
	Contact con;//定义了一个通讯录
	//初始化通讯录
	InitContact(&con);

	do
	{
		menu();
		printf("请选择—》\n");
		scanf("%d", &input);
		switch (input)
		{
		case EIXT:
			//保存通讯录
			SaveContact(&con);
			//销毁通讯录
			DestoryContact(&con);
			printf("退出通讯录\n");
			break;
		case ADD:
			//增加人的信息
			ADDContact(&con);
			break;
		case DEL:
			DelContact(&con);
			break;
		case SEARCH:
			SearchContact(&con);
			break;
		case MODIFY:
			ModieyContact(&con);
			break;
		case SORT:
			//排序通讯录信息
			break;
		case PRINT:
			PrintContact(&con);
			break;
		default:
			printf("输入错误 请重新选择\n");
		}
	} while (input);

	return 0;
}