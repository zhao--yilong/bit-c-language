#define _CRT_SECURE_NO_WARNINGS 1
#include "game.h"

void InitQiPan(char board[ROWS][COLS], int rows, int cols, char set)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < cols; j++)
		{
			board[i][j] = set;
		}
	}
}

void DisplayBoard(char board[ROWS][COLS], int row, int col)
{
	int i = 0, j = 0;
	printf("-------------扫雷游戏-------------\n");
	for (i = 0; i <= row; i++)
	{
		printf(" %d ", i);
		if (i < row)
		{
			printf("|");
		}
	}
	printf("\n");
	//控制分割线
		for (i = 0; i <= row; i++)
		{
			printf("---");
			if (i < row)
			{
				printf("|");
			}
		}
	printf("\n");
	//打印数据及控制分隔线
		for (i = 1; i <= row; i++)
		{
			printf(" %d |", i);
			for (j = 1; j <= col; j++)
			{
				printf(" %c ", board[i][j]);
				if (j < col)
				{
					printf("|");
				}
			}
			printf("\n");
			if (i < row)
			{
				for (int k = 0; k <= col; k++)
				{
					printf("---");
					if (k < col)
					{
						printf("|");
					}
				}
			}
			printf("\n");
		}
	printf("-------------扫雷游戏-------------\n");
}

void SetMine(char mine[ROWS][COLS], int row, int col)
{
	//布置10个雷
	int count = EASY_COUNT;
	while (count)
	{
		int x = rand() % row + 1;
		int y = rand() % col + 1;

		if (mine[x][y] == '0')
		{
			mine[x][y] = '1';
			count--;
		}
	}
}

int get_mine_count(char mine[ROWS][COLS], int x, int y)
{
	return (mine[x - 1][y] +
		mine[x - 1][y - 1] +
		mine[x][y - 1] +
		mine[x + 1][y - 1] +
		mine[x + 1][y] +
		mine[x + 1][y + 1] +
		mine[x][y + 1] +
		mine[x - 1][y + 1] - 8 * '0');
}

//递归拓展已选位置周围的区域
void boom_broad(char mine[ROWS][COLS], char show[ROWS][COLS], int x, int y, int* win)
{
	int count = get_mine_count(mine, x, y);
	if (count == 0)
	{
		show[x][y] = ' ';//没有雷的坐标赋值为空格
		(*win)++;
		//递归周围的八个格子
		if (show[x - 1][y - 1] == '*' && x - 1 > 0 && x - 1 < ROWS && y - 1 > 0 && y - 1 < COLS)
			boom_broad(mine, show, x - 1, y - 1, win);
		if (show[x - 1][y] == '*' && x - 1 > 0 && x - 1 < ROWS && y > 0 && y < COLS)
			boom_broad(mine, show, x - 1, y, win);
		if (show[x - 1][y + 1] == '*' && x - 1 > 0 && x - 1 < ROWS && y + 1 > 0 && y + 1 < COLS)
			boom_broad(mine, show, x - 1, y + 1, win);
		if (show[x][y - 1] == '*' && x > 0 && x < ROWS && y - 1 > 0 && y - 1 < COLS)
			boom_broad(mine, show, x, y - 1, win);
		if (show[x][y + 1] == '*' && x > 0 && x < ROWS && y + 1 > 0 && y + 1 < COLS)
			boom_broad(mine, show, x, y + 1, win);
		if (show[x + 1][y - 1] == '*' && x + 1 > 0 && x + 1 < ROWS && y - 1 > 0 && y - 1 < COLS)
			boom_broad(mine, show, x + 1, y - 1, win);
		if (show[x + 1][y] == '*' && x + 1 > 0 && x + 1 < ROWS && y > 0 && y < COLS)
			boom_broad(mine, show, x + 1, y, win);
		if (show[x + 1][y + 1] == '*' && x + 1 > 0 && x + 1 < ROWS && y + 1 > 0 && y + 1 < COLS)
			boom_broad(mine, show, x + 1, y + 1, win);

	}
	else
	{
		show[x][y] = count + '0';
	}
}


void FindMine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col)
{
	int x = 0;
	int y = 0;
	int win = 0;

	while (win < row * col - EASY_COUNT)
	{
		printf("请输入要排查雷的坐标:>");
		scanf("%d %d", &x, &y);
		if (x >= 1 && x <= row && y >= 1 && y <= col)
		{
			//坐标被排查过
			if (show[x][y] == '*')
			{
				if (mine[x][y] == '1')
				{
					printf("很遗憾，你被炸死了\n");
					DisplayBoard(mine, ROW, COL);
					break;
				}
				else
				{
					boom_broad(mine, show, x, y,&win);
					show[x][y] = get_mine_count(mine, x, y) + '0';
					/*int count = get_mine_count(mine, x, y);
					show[x][y] = count + '0';*/
					DisplayBoard(show, ROW, COL);
					win++;
				}
			}
			else
			{
				printf("该坐标已经被排查过了\n");
			}
		}
		else
		{
			printf("坐标非法，请重新输入\n");
		}
	}
	if (win == row * col - EASY_COUNT)
	{
		printf("恭喜你，排雷成功\n");
		DisplayBoard(mine, ROW, COL);
	}
}
