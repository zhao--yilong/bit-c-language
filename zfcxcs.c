#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

int main()
{
    char str[2000];
    char c;

    gets(str);
    scanf("%c", &c);

    int i = 0;
    int sum = 0;

    while (str[i] != '\0')
    {
        if (str[i] >= 'a'&&str[i]<='z')
        {
            if (str[i] == c)
                sum++;
            else if (str[i] - 32 == c)
                sum++;
        }

        if (str[i] >= 'A' && str[i] <= 'Z')
        {
            if (str[i] == c)
                sum++;
            else if (str[i] + 32 == c)
                sum++;
        }


        if (str[i] >= '0' && str[i] <= '9')
        {
            if (str[i] == c)
                sum++;
        }
        i++;
    }
    printf("%d", sum);
    return 0;
}