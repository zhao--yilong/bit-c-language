#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
#include<vector>
using namespace std;

//前置声明
template<class K, class T, class KeyofT, class HF>
class HashBucket;


template<class T>
struct HashBucketNode
{
	HashBucketNode(T data)
		:_data(data)
		, _pNext(nullptr)
	{}
	T _data;
	HashBucketNode* _pNext;
};

//KeyofT用来取出 T 中的key
template<class K, class T, class Ref, class Ptr, class KeyofT, class HF >
struct Iterator
{
	KeyofT kot;
	HF hf;
	typedef HashBucketNode<T> Node;
	typedef Iterator<K, T, Ref, Ptr, KeyofT, HF > Self;

	Node* _node;   //节点指针
	const HashBucket<K, T, KeyofT, HF>* _pht;  //哈希桶指针

	Iterator(Node* node, const HashBucket<K, T, KeyofT, HF>* pht)
		:_node(node)
		, _pht(pht)
	{}

	Ref operator*()
	{
		return _node->_data;
	}

	Ptr operator->()
	{
		return &_node->_data;
	}

	bool operator!=(const Self& s)
	{
		return _node != s._node;
	}

	bool operator==(const Self& s)
	{
		return _node == s._node;
	}

	Self& operator++()
	{
		KeyofT kot;
		HF hf;
		if (_node->_pNext != nullptr)   //++ 的下一个节点在当前桶中
			_node = _node->_pNext;
		else    //++ 的下一个节点不在当前桶中
		{
			Node* cur = _node;
			size_t hashi = hf(kot(cur->_data)) % _pht->_table.size();
			hashi++;
			while (hashi < _pht->_table.size())  //寻找下一个桶
			{
				if (_pht->_table[hashi] != nullptr)
				{
					_node = _pht->_table[hashi];
					return *this;
				}
				else
					hashi++;
			}
			_node = nullptr;
			return *this;
		}
		return *this;
	}
};
template<class K>
struct HashFunc
{
	size_t operator()(const K& key)
	{
		return key;
	}
};
template<>
struct HashFunc<std::string>
{
	size_t operator()(const std::string& s)
	{
		size_t res = 0;
		for (auto c : s)
		{
			res *= 31;
			res += c;
		}
		return res;
	}
};


	template<class K, class T, class KeyofT, class HF = HashFunc<K>>
	class HashBucket
	{
		typedef HashBucketNode<T> Node;
		typedef HashBucket<K, T, KeyofT, HF> Self;

	public:
		template<class K, class T, class Ref, class Ptr, class KeyofT, class HF >
		friend struct Iterator;   //声明迭代器友元
		typedef Iterator<K, T, const T&, const T*, KeyofT, HF> const_Iterator; 
		typedef Iterator<K, T, T&, T*, KeyofT, HF> Iterator;

		Iterator begin()
		{
			for (size_t i = 0; i < _table.size(); i++)
			{
				if (_table[i] != nullptr)
					return Iterator(_table[i], this);
			}
			return Iterator(nullptr, this);
		}

		Iterator end()
		{
			return Iterator(nullptr, this);
		}

		const_Iterator begin() const
		{
			for (size_t i = 0; i < _table.size(); i++)
			{
				if (_table[i] != nullptr)
					return const_Iterator(_table[i], this);
			}
			return const_Iterator(nullptr, this);
		}

		const_Iterator end() const
		{
			return const_Iterator(nullptr, this);
		}

		HashBucket()
		{
			_table.resize(10, nullptr);
			_size = 0;
		}

		//~HashBucket()
		//{
		//	.Clear();
		//}

		// 哈希桶中的元素不能重复
		pair<Iterator, bool> Insert(const T& data)
		{
			Iterator ret = Find(data);
			if (ret != end())
				return { Iterator(nullptr,this),false };
			HF hf;
			KeyofT kot;
			if (_size == _table.size())
			{
				size_t newsize = 2 * _table.size();
				vector<Node*> newtable(newsize, nullptr);
				for (size_t i = 0; i < _table.size(); i++)
				{
					Node* cur = _table[i];
					while (cur)
					{
						Node* next = cur->_pNext;
						size_t hashi = hf(kot(data)) % newsize;
						Node* newnode = new Node(cur->_data);
						if (newtable[hashi] == nullptr)
							newtable[hashi] = newnode;
						else
						{
							newnode->_pNext = newtable[hashi]->_pNext;
							newtable[hashi]->_pNext = newnode;
						}
						cur = next;
					}
					_table[i] = nullptr;
				}
				_table.swap(newtable);
			}
			Node* newnode = new Node(data);
			size_t hashi = hf(kot(data)) % _table.size();  //注意取模方式
			if (_table[hashi] == nullptr)
				_table[hashi] = newnode;
			else
			{
				newnode->_pNext = _table[hashi]->_pNext;
				_table[hashi]->_pNext = newnode;
			}
			_size++;
			return { Iterator(newnode,this),true };
		}

		//删除哈希桶中为data的元素(data不会重复)
		Iterator Erase(Iterator pos)
		{
			HF hf;
			KeyofT kot;
			Iterator it = begin();
			while (it != end())
			{
				if (it == pos)
				{
					Iterator tmp = it;
					delete it;
					return Iterator(++tmp, this);
				}
				++it;
			}
			return Iterator(nullptr, this);
		}

		Iterator Find(const T& data)
		{
			HF hf;
			KeyofT kot;
			size_t hashi = hf(kot(data)) % _table.size();
			Node* cur = _table[hashi];
			while (cur)
			{
				if (cur->_data == data)
					return Iterator(cur, this);
				else
					cur = cur->_pNext;
			}
			return end();
		}

	private:
		size_t HashFunc(const T& data)
		{
			return HF()(data) % _table.capacity();
		}
	private:
		vector<Node*> _table;
		size_t _size;      // 哈希表中有效元素的个数
	};
