#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<utility>
#include"HashT.h"
using namespace std;

namespace zyl
{
	template<class K>   //哈希函数
	struct DefHF
	{
		size_t operator()(const K& key)
		{
			return (size_t)key;
		}
	};
	template<class K, class V>
	class unordered_map
	{
		struct MapKeyofT   //从 pair 中取出 K
		{
			const K& operator()(const pair<const K, V>& kv)
			{
				return kv.first;
			}
		};

	public:
		//注意这里要使用 typename ，否则编译器无法分清 Iterator 是类型还是函数
		typedef typename HashBucket<K, pair<const K, V>, MapKeyofT>::Iterator iterator;   //普通迭代器
		typedef typename HashBucket<K, pair<const K, V>, MapKeyofT>::const_Iterator const_iterator;    //常量迭代器

		iterator begin()
		{
			return  _ht.begin();  //调用哈希桶的 begin
		}

		iterator end()
		{
			return _ht.end();    //调用哈希桶的 end
		}

		const_iterator begin() const
		{
			return  _ht.begin();
		}

		const_iterator end() const
		{
			return _ht.end();
		}

		//插入
		pair<iterator, bool> insert(const pair<K, V>& kv)
		{
			return _ht.Insert(kv);
		}
		//删除
		iterator erase(iterator pos)
		{
			return _ht.Erase(pos);
		}

		// Acess
		V& operator[](const K& key)
		{
			pair<iterator, bool> ret = _ht.Insert({ key,V() });
			iterator it = ret.first;
			return it->second;
		}
		const V& operator[](const K& key)const
		{
			pair<const_iterator, bool> ret = _ht.Insert({ key,V() });
			const_iterator it = ret.first;
			return it->second;
		}

		// capacity
		size_t size()const
		{
			return _ht.size();
		}
		bool empty()const
		{
			return _ht.empty();
		}

		// lookup
		iterator find(const K& key)
		{
			return _ht.Find(key);
		}
		size_t count(const K& key)
		{
			return _ht.Count(key);
		}

		// bucket
		size_t bucket_count()
		{
			return _ht.BucketCount();
		}
		size_t bucket_size(const K& key)
		{
			return _ht.BucketSize(key);
		}

	private:
		//传入 MapKeyofT
		HashBucket<K, pair<const K, V>, MapKeyofT> _ht;  //底层是哈希桶
	};

}