
char shift(char c,int x)
{
      char z = c+(x-'0');
      if(z<='z')
      {
          return z; 
      }
      return 0;
}


char * replaceDigits(char * s)
{
      int i =1;
      int sz = strlen(s);
      for(i=1;i<sz;i+=2)
      {
         s[i] = shift(s[i-1],s[i]);
      }
     return s;
}