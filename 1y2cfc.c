#include <stdio.h>

#include <math.h>
//顺序结构求解一元二次方程的根
int main() {
	double a, b, c, disc, x1, x2, p, q;
	scanf("%lf%lf%lf", &a, &b, &c);
	disc = b * b - 4 * a * c; //求解判别式
	if (disc < 0) {
		printf("This equation hasnot real roots\n");
	} else {
		p = -b / (2.0 * a);
		q = sqrt(disc) / (2.0 * a);
		x1 = p + q ;
		x2 = p - q ;
		printf("real roots is :\nx1 = %7.2f\nx2 = %7.2f\n", x1, x2);
	}
	return 0;
}
