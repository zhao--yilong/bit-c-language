#define _CRT_SECURE_NO_WARNINGS 1

//编写函数void del（char*s，char c） 其功能是在s所指的字符串中删除与变量c相同的 字符， 并设计主函数实现


#include<stdio.h>

void del(char* s, char c)
{
	int k = 1;
	char* p = s;
	while (*p != c && *p != '\0')//找到与c 相同字符的坐标
		p++;
	while (*p != '\0') {
		char* t = p + 1; //把p下一位置的字符 送到t 里
		if (*t == c)   //如果下一位置也等于c k++；
			k++;
		else           //不等于后 进行替换
			*(p - k + 1) = *t;
		p++;
	}
}

int main()
{
	char s1[100] ;
	char s2;
	scanf("%s %c", s1, &s2);

	del(s1,s2);
	puts(s1);
	return 0;
}