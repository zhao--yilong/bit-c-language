#include "stdio.h"

void show(char ch, int *screen, int *top, int *bottom, int *index, int n);

int main(){
    int n;
    while(scanf("%d", &n) != EOF){
      	//***根据歌曲数量初始化屏幕显示内容***//
        int m = n >= 4 ? 4 : n;
        int screen[m];
        for(int i = 0; i < m; i++)
            screen[i] = i + 1;
      	//***初始化第一行显示的歌曲、最后显示的歌曲和光标所在位置的歌曲***//
        int top = screen[0], bottom = screen[m-1], index = screen[0];
      	//***获取命令***//
        char cmd[100];
        scanf("%s", cmd);
      	//***根据命令更新屏幕显示的内容、第一行显示的歌曲、最后显示的歌曲和光标所在位置的歌曲***//
        int len = strlen(cmd);
        for(int i = 0; i < len; i++)
            show(cmd[i], screen, &top, &bottom, &index, n);
      	//***输出屏幕最终显示的内容***//
        for(int i = 0; i < m; i++)
            printf("%d ", screen[i]);
      	//***输出光标所在位置的歌曲***//
        printf("\n%d\n", index);
    }
    return 0;
}

void show(char ch, int *screen, int *top, int *bottom, int *index, int n){
  	//***情况1：命令为向上，且歌曲数量>4***//
    if(ch == 'U' && n > 4){
      	//***若光标在第一行，且第一行为第一首歌，则翻页：最后一行为最后一首歌，第一行为倒数第四首歌，光标在最后一行，同时更新屏幕显示的内容***//
        if(*index == 1){
            *bottom = n;
            *top = n - 3;
            *index = n;
            int temp = *bottom;
            for(int i = 3; i >= 0; i--)
                screen[i] = temp--;
        }
      	//***若光标在第一行，且第一行不为第一首歌，则整个歌曲列表向下移***//
        else if(*index == *top){
            (*top)--;
            (*index)--;
            (*bottom)--;
            int temp = *top;
            for(int i = 0; i <= 3; i++)
                screen[i] = temp++;
        }
      	//***若光标不在第一行，则只需将光标往上移***//
        else
            (*index)--;
    }
  	//***情况2：命令为向下，且歌曲数量>4***//
    else if(ch == 'D' && n > 4){
      	//***若光标在最后一行，且最后一行为最后一首歌，则翻页：第一行为第一首歌，最后一行为第四首歌，光标在第一行，同时更新屏幕显示的内容***//
        if(*index == n){
            *top = 1;
            *bottom = 4;
            *index = 1;
            int temp = *top;
            for(int i = 0; i <= 3; i++)
                screen[i] = temp++;
        }
      	//***若光标在最后一行，且最后一行不为最后一首歌，则整个歌曲列表向上移***//
        else if(*index == *bottom){
            (*top)++;
            (*index)++;
            (*bottom)++;
            int temp = *top;
            for(int i = 0; i <= 3; i++)
                screen[i] = temp++;
        }
      	//***若光标不在最后一行，则只需将光标往下移***//
        else
            (*index)++;
    }
  	//***情况3：命令为向上，且歌曲数量<=4***//
    else if(ch == 'U' && n <= 4){
      	//***若光标在第一行，则光标移至最后一行***//
        if(*index == 1)
            *index = n;
      	//***若光标不在第一行，则光标往上移***//
        else
            (*index)--;
    }
  	//***情况4：命令为向下，且歌曲数量<=4***//
    else if(ch == 'D' && n <= 4){
      	//***若光标在最后一行，则光标移至第一行***//
        if(*index == n)
            *index = 1;
      	//***若光标不在最后一行，则光标往下移***//
        else
            (*index)++;
    }
}
