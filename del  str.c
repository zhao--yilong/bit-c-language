#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main()
{
    char str[22];
    while(scanf("%s", str) != EOF)
    {
        int flag[26] = {0};
        int min=20;
        int len=strlen(str);
        //统计每个字符的次数
        for(int i=0; i<len; ++i)
        {
            flag[str[i]-'a']++;
        }
        //找出字符出现的最小次数
        for(int i=0; i<26; ++i)
        {
            if(flag[i] && flag[i] < min)
                min = flag[i];
        }
        //输出出现次数大于最小次数的字符
        for(int i=0; i<len; ++i)
        {
            if(flag[str[i]-'a'] > min)
                printf("%c", str[i]);
        }
        printf("\n");
    }
    return 0;
}
