#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<assert.h>

namespace zyl
{
    template<class T>
    class vector
    {
    public:
        // Vector的迭代器是一个原生指针
        typedef T* iterator;
        typedef const T* const_iterator;

        iterator begin()
        {
            return _start;
        }
        iterator end()
        {
            return _finish;
        }

        const_iterator cbegin() const
        {
            return _start;
        }
        const_iterator cend() const
        {
            return _finish;
        }

        // construct and destroy

        vector()
            :_endOfStorage(nullptr)
            ,_start(nullptr)
            ,_finish(nullptr)
        {

        }

        vector(int n, const T& value = T())
            :_endOfStorage(nullptr)
            , _start(nullptr)
            , _finish(nullptr)
        {
            reserve(n);//提前开n个空间
            for (int i = 0;i < n;i++)
            {
                push_back(value);//缺省值默认为val；
            }
        }

        //迭代器区间构造
        template<class InputIterator>
        vector(InputIterator first, InputIterator last)
            :_endOfStorage(nullptr)
            ,_start(nullptr)
            ,_finish(nullptr)
        {
            while (first != last)
            {
                push_back(*first);
                ++first;
            }

        }
        vector(const vector<T>& v)
        {
            vector<T> tmp(v.cbegin(), v.cend());
            swap(tmp);
        }

        vector<T>& operator= (vector<T> v)
        {
            if (this != &v)
            {
                delete[] _start;
                _start = new T[v.capacity()];
                for (size_t i = 0;i < v.size();i++)
                {
                    _start[i] = v[i];
                }
                _finish = _start + v.size();
                _endOfStorage = _start + v.capacity();
            }
            return *this;
        }

        ~vector()
        {
            delete[]_start;
            _start = nullptr;
            _finish = nullptr;
            _endOfStorage = nullptr;
        }
            // capacity
        size_t size() const
        {
            return _finish - _start;
        }

        size_t capacity() const
        {
            return  _endOfStorage - _start;
        }

        void reserve(size_t n)
        {
            if (n >capacity())
            {
                size_t sz = size();
                T* tmp = new T[n];

                if (_start)//如果为空  则不用将旧数据转移
                {
                    for (size_t i = 0;i <size();i++)
                    {
                        tmp[i] = _start[i];
                    }
                    delete[] _start;
                }
                _start = tmp;
                _finish = _start + sz;
                _endOfStorage = _start + n;
            }
        }
        void resize(size_t n, const T& value = T())
        {
            //查看是否需要扩容
            if (n > capacity())
            {
                reserve(n);
            }

            if (n > size())
            {
                while (_finish > _start + n)
                {
                    *_finish = value;
                    ++_finish;
                }
            }
            else
            {
                _finish = _start + n;
            }
        }
            ///////////////access///////////////////////////////
        T& operator[](size_t pos)
        {
            assert(pos < size());
            return _start[pos];
        }
        const T& operator[](size_t pos)const
        {
            assert(pos < size());
            return _start[pos];
        }

        void push_back(const T& x)
        {
            insert(_finish, x);
        }

        void pop_back()
        {
            erase(_finish);
        }

        void swap(vector<T>& v)
        {
            std::swap(_start, v._start);
            std::swap(_finish, v._finish);
            std::swap(_endOfStorage, v._endOfStorage);
        }
        iterator insert(iterator pos, const T& x)
        {
            //pos范围必须在_start和_finish之间
            assert(pos>=_start);
            assert(pos <= _finish);
            //内存满了 进行扩容
            if (_finish == _endOfStorage)
            {
                size_t len = pos - _start;
                reseve(capacity() > 0 ? 4 : capacity * 2);
                pos = _start + len;
            }
            iterator end = _finish;
            //移动数据 进行插入
            while (end >= pos)
            {
                *end = *(end - 1);
                end--;
            }
            *pos = x;
            ++_finish;
            return pos;
        }
        //判空
        bool empty() const
        {
            return _finish == _start;
        }

        //删除
        iterator erase(iterator pos)
        {
            //判断pos是否合法
            assert(pos > _finish);
            assert(pos < _start);
            assert(!empty());

            iterator begin = pos + 1;
            while (begin < _finish)
            {
                *(begin - 1) = *begin;
                ++begin;
            }
            --_finish;
            return pos;
        }
    private:
        iterator _start; // 指向数据块的开始
        iterator _finish; // 指向有效数据的尾
        iterator _endOfStorage; // 指向存储容量的尾
    };

}