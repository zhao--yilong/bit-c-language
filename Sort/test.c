#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<stdlib.h>
#include"Stack.h"

// 直接插入排序
void InsertSort(int a[], int n)
{
	//i 每一个数都算是插入进去的
	for (int i = 0;i < n - 1;i++)
	{
		//end 数组内数据个数
		int end = i; 
		//tmp 为插入数据
		int tmp = a[end + 1];

		//插入数据与数组内数据比较，如果插入数据比数组最后一个数据大 好 直接原地插入 
		//跳出循环 ，否则，跟数组内数据比较 找到数组内合适的位置插入
		while (end>=0) // 数组内数据遍历到a【0】
		{
			if (tmp < a[end]) //插入数据 与 数组最后位置比较
			{
				a[end + 1] = a[end];  // 小于 往前走 
				--end;
			}
			else
			{  
				break;    //大于 结束
			}
			a[end + 1] = tmp; //
		}
		
	}
}

// 希尔排序
void ShellSort(int* a, int n)
{
	//希尔排序
	 
	//1. 先预排序
	// 预排序 就是  把要排序数组分成 gap组 然后开始比较 交换
	// 
	//2.在插入排序

	// gap > 1 预排序
	// gap == 1 直接插入排序
	int gap = n;
	while (gap > 1) //
	{
		// gap = gap / 2; 
		gap = gap / 3 + 1;// 如果不加1  就有可能到不了 1 

		//当 gap  =1 时就是直插  ，gap 越小 与排序越接近排序好的状态
		for (int i = 0; i < n - gap; ++i)
		{
			int end = i;
			int tmp = a[end + gap]; 

			while (end >= 0)
			{
				if (tmp < a[end])
				{
					a[end + gap] = a[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}

			a[end + gap] = tmp;
		}


	}
}

void  printf1(int* a, int  n) 
{	
	for (int i = 0;i < n;i++)
	{
		printf("%d ", a[i]);
	}
}

void Swap(int* p1, int* p2)
{
	int tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}

// 选择排序（遍历一边  选最小 和 最大的 数  跟 头尾交换）
void SelectSort(int* a, int n)
{
	int begin = 0, end = n - 1;

	while (begin < end)
	{
		int mini = begin, maxi = begin;

		for (int i = begin + 1; i <= end; ++i)
		{
			if (a[i] < a[mini])//遍历数组 找最小数据
			{
				mini = i;
			}

			if (a[i] > a[maxi])//找最大数据
			{
				maxi = i;
			}
		}

		Swap(&a[begin], &a[mini]); //交换最开始和 最小的数据
		if (maxi == begin)    //如果最大的是最开始的数据，上面交换最大的数据被换到了最小的数据位置上
			maxi = mini;      //把最小数据的位置交给最大数据

		Swap(&a[end], &a[maxi]);//交换最后和 最大的数据
		++begin;
		--end;
	}
}


// 堆排序
void AdjustDown(int* a, int n, int parent)
{
	int child = parent * 2 + 1;
	while (child < n)
	{
		// 确认child指向大的那个孩子
		if (child + 1 < n && a[child + 1] > a[child])
		{
			++child;
		}

		// 1、孩子大于父亲，交换，继续向下调整
		// 2、孩子小于父亲，则调整结束
		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}
void HeapSort(int* a, int n)
{
	// 向下调整建堆 -- O(N)
	// 升序：建大堆
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(a, n, i);
	}

	// O（N*logN）
	int end = n - 1;
	while (end > 0)
	{
		Swap(&a[0], &a[end]);
		AdjustDown(a, end, 0);
		--end;
	}
}

// 冒泡排序
void BubbleSort(int* a, int n)
{
	int i = 0;
	for (i = 0;i < n - 1;i++)
	{
		int j = 0;
		for (j = 0;j < n - i - 1;j++)
		{
			if (a[j] > a[j + 1])
			{
				int tmp = a[j];
				a[j] = a[j + 1];
				a[j + 1] = tmp;
			}
		}
	}
}
//递归实现快排
//void QuickSort(int* a, int begin, int end)
//{
//	if (begin >= end)
//	{
//		return;
//	}
//	int left = begin, right = end;
//	int keyi = left;
//	while (left < right)
//	{
//		// 右边先走，找小
//		while (left < right && a[right] >= a[keyi])
//		{
//			--right;
//		}
//
//		// 左边再走，找大
//		while (left < right && a[left] <= a[keyi])
//		{
//			++left;
//		}
//
//		Swap(&a[left], &a[right]);
//	}
//
//	Swap(&a[left], &a[keyi]);
//	keyi = left;
//
//	// [begin, keyi-1]  keyi [keyi+1, end]
//	QuickSort(a, begin, keyi - 1);
//	QuickSort(a, keyi + 1, end);
//}


//递归实现快排
//三数取中
int Fetchmid(int* a, int begin, int end)
{
	int mid = (begin + end) / 2;
	if (a[begin] < a[mid])
	{
		if (a[mid] < a[end])
		{
			return mid;
		}
		else if (a[begin] > a[end])
		{
			return begin;
		}
		else
		{
			return end;
		}
	}
	else // a[begin] > a[mid]
	{
		if (a[mid] > a[end])
		{
			return mid;
		}
		else if (a[begin] < a[end])
		{
			return begin;
		}
		else
		{
			return end;
		}
	}
}
void QuickSort(int* a, int begin, int end)
{
	if (begin >= end)
	{
		return;
	}
	//小区间优化
	if (end - begin < 12)
	{
		//当小于12 个数的时候用直接插入法插入
		InsertSort(a+begin, end - begin);
	}
	else
	{
		int left = begin, right = end;
		int keyi = Fetchmid(a,begin, end);
		while (left < right)
		{
			// 右边先走，找小
			while (left < right && a[right] >= a[keyi])
			{
				--right;
			}

			// 左边再走，找大
			while (left < right && a[left] <= a[keyi])
			{
				++left;
			}

			Swap(&a[left], &a[right]);
		}

		Swap(&a[left], &a[keyi]);
		keyi = left;

		// [begin, keyi-1]  keyi [keyi+1, end]
		QuickSort(a, begin, keyi - 1);
		QuickSort(a, keyi + 1, end);
	}
}

//int PartSort2(int* a, int begin, int end)
//{
//	
//	return hole;
//}

//挖坑法
void QuickSort2(int* a, int begin, int end)
{
		if (begin >= end)
		{
			return;
		}

		if ((end - begin + 1) < 15)
		{
			// 小区间用直接插入替代，减少递归调用次数
			InsertSort(a + begin, end - begin + 1);
		}
		else
		{
			int mid = Fetchmid(a, begin, end);
			Swap(&a[begin], &a[mid]);

			int left = begin, right = end;
			int key = a[left];
			int hole = left;
			while (left < right)
			{
				// 右边找小，填到左边坑里面
				while (left < right && a[right] >= key)
				{
					--right;
				}

				a[hole] = a[right];
				hole = right;

				// 左边找大，填到右边坑里面
				while (left < right && a[left] <= key)
				{
					++left;
				}

				a[hole] = a[left];
				hole = left;
			}

			a[hole] = key;
			key = hole;

			// [begin, keyi-1]  keyi [keyi+1, end]
			QuickSort(a, begin, key - 1);
			QuickSort(a, key + 1, end);
		}
}
//双指针实现快排
void QuickSort3(int* a, int begin, int end)
{

	if (begin >= end)
	{
		return;
	}

	if ((end - begin + 1) < 15)
	{
		// 小区间用直接插入替代，减少递归调用次数
		InsertSort(a + begin, end - begin + 1);
	}
	else
	{
		int mid = Fetchmid(a, begin, end);
		Swap(&a[begin], &a[mid]);

		int keyi = begin;
		int prev = begin, cur = begin + 1;
		while (cur <= end)
		{
			// 找到比key小的值时，跟++prev位置交换，小的往前翻，大的往后翻
			if (a[cur] < a[keyi] && ++prev != cur)
				Swap(&a[prev], &a[cur]);

			++cur;
		}

		Swap(&a[prev], &a[keyi]);
		keyi = prev;

		// [begin, keyi-1]  keyi [keyi+1, end]
		QuickSort(a, begin, keyi - 1);
		QuickSort(a, keyi + 1, end);
	}

}
void QuickSortNonR(int* a, int begin, int end)
{
	Stack st;
	StackInit(&st);
	StackPush(&st, begin);
	StackPush(&st, end);

	while (!StackEmpty(&st))
	{
		int right = StackTop(&st);
		StackPop(&st);
		int left = StackTop(&st);
		StackPop(&st);

		int keyi = PartSort3(a, left, right);
		// [left, keyi-1] keyi [keyi+1, right]
		if (keyi + 1 < right)
		{
			StackPush(&st, keyi + 1);
			StackPush(&st, right);
		}

		if (left < keyi - 1)
		{
			StackPush(&st, left);
			StackPush(&st, keyi - 1);
		}
	}

	StackDestroy(&st);
}


int  main()
{
	int a[10] = { 1,3,5,7,9,2,4,6,8,0 };

	QuickSort3(a, 0,sizeof(a) / sizeof(int)-1);
	printf1(a, sizeof(a) / sizeof(int));

	return 0;
}