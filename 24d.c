#include<stdio.h>
int v[4]={0};  //对数据进行标记，未使用则置0
int dfs(int *a,int s){
    if(s==24)
        return 1;
    for(int i=0;i<4;i++){
        if(v[i]==0){
            v[i]=1;
            if(dfs(a,s+a[i])||dfs(a,s*a[i])||dfs(a,s-a[i])||dfs(a,s/a[i]))
                return 1;
            v[i]=0; //未找到则回退
        }
    }
    return 0;
}
int main(){
    int a[4];
    for(int i=0;i<4;i++)
        scanf("%d",&a[i]);
    int s=a[0]; //用于记录数据总和
    v[0]=1;
    if(dfs(a,a[0])==1) //进行搜索判断
        printf("true");
    else
        printf("false");
    return 0;
}
