#define _CRT_SECURE_NO_WARNINGS 1

//#include<stdio.h>
//int main()
//{
//	int aa[2][5] = { 10,9,8,7,6,5,4,3,2,1 };
//	int* ptr1 = (int*)(&aa + 1);//&aa+1 跳过一个aa  位置到1后面
//	int* ptr2 = (int*)(*(aa + 1));//aa +1 跳到5的位置  
//	printf("%d,%d", *(ptr1 - 1), *(ptr2 - 1));
//	return 0;
//}
//练习使用库函数，qsort排序各种类型的数据
//#include<stdio.h>
//#include<stdlib.h>
//#include<string.h>
//int bjdx_zx(const void *e1,const void*e2)
//{
//	return *(int*)e1 - *(int*)e2;
//}
//
//void print_arr(int* arr, int sz)
//{
//	int i = 0;
//	for (i = 0;i < sz;i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//
//void test()
//{
//	int arr[] = { 1,3,5,7,9,2,4,6,8,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, 4, bjdx_zx);
//	print_arr(arr, sz);
//}
//
//struct STU
//{
//	char name[20];
//	int age;
//};
//int bj_zf(const void* e1, const void* e2)
//{
//	return strcmp(((struct STU*)e1)->name, ((struct STU*)e2)->name);
//}
//int bj_sz(const void* e1, const void* e2)
//{
//	return ((struct STU*)e1)->age-((struct STU*)e2)->age;
//}
//void test1()
//{
//	struct STU s[] = { {"zsh",16},{"zyl",22},{"shh",2}};
//	int sz = sizeof(s) / sizeof(s[0]);
//	//qsort(s, sz, sizeof(s[0]), bj_zf);
//	qsort(s, sz, sizeof(s[0]), bj_sz);
//
//}
//
//int main()
//{
//	test();//整型
//	test1();// 结构体 字符 
//	return 0;
//}

//模仿qsort的功能实现一个通用的冒泡排序s


#include<stdio.h>

int bjdx_zx(const void* e1, const void* e2)
{
	return *(int*)e1 - *(int*)e2;
}


void jh(char* s1, char* s2,int cd)
{
	while(cd)
	{
		char tmp = *s1;
		*s1 = *s2;
		*s2 = tmp;
		cd--;
		s1++;
		s2++;
	}
}

void maopao_sort(void* Base, int sz, int cd, int(*com)(const void* e1, const void* e2))
{
	int i = 0;
	for (i = 0;i < sz - 1;i++)
	{
		int j = 0;
		for (j = 0;j < sz - 1 - i;j++)
		{
			if (com((char*)Base + j*cd, (char*)Base + (j+1) * cd)>0)
			{
				//交换
				jh((char*)Base + j * cd, (char*)Base + (j + 1) * cd, cd);
			}
		}
	}

}

void print_arr(int* arr, int sz)
{
	int i = 0;
	for (i = 0;i < sz;i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}

int main()
{
	int arr[] = { 1,3,5,7,9,2,4,6,8,0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	print_arr(arr, sz);
	maopao_sort(arr, sz, 4, bjdx_zx);
	print_arr(arr, sz);
	return 0;
}


