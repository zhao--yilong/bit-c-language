int MOD=1e9+7;

int cmp(int *a,int *b){
    return *a-*b;
}
int maxProfit(int* inventory, int inventorySize, int orders){
    long long res=0;
    int length=inventorySize;
    int index=length-1;

    qsort(inventory,length,sizeof(int),cmp);
    while(orders>0){
        res+=inventory[index--]--;
        if(index<0 || inventory[index]<=inventory[length-1]){
            index=length-1;
        }
        orders--;
    }

    return res%=MOD;
}
