#define _CRT_SECURE_NO_WARNINGS 1

//编写一个函数实现n的k次方，使用递归实现。
//
//#include<stdio.h>
//
//int xx(int n, int k)
//{
//	if (k <= 1)
//		return n;
//	else
//		return n*xx(n,k - 1);
//}
//
//int main()
//{
//	int k = 0;
//	int n = 0;
//	scanf("%d %d", &n,&k);
//	int ret = xx(n,k);
//	printf("%d的%d次方为%d", n, k, ret);
//	return 0;
//}

//写一个递归函数DigitSum(n)，输入一个非负整数，返回组成它的数字之和
//例如，调用DigitSum(1729)，则应该返回1 + 7 + 2 + 9，它的和是19
//输入：1729，输出：19

#include<stdio.h>

int DigitSum(unsigned int x)
{
	if (x < 10)
	     return x;
	else
		return  x % 10 + DigitSum(x / 10);	
}

int main()
{
	unsigned int n = 0;
	scanf("%ud", &n);
	printf("%d",DigitSum(n));

	return 0;
}