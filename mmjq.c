#include <stdio.h>
#include <string.h>

//颠倒后比较最长字段 XXX
//中心遍历
//增加遍历指针low high是遍历更加简洁
int main()
{
    char str[3000];
    scanf("%s", str);
    int len = strlen(str);
    int max[len][2];
    for (int i = 0; i < len; i++)
    {
        int low, high;
        //以i为中心奇数串
        low = i - 1, high = i + 1;
        max[i][0] = 1;
        while (low >= 0 && high < len && str[low] == str[high])
        {
            low--;
            high++;
            max[i][0] += 2;
        }
        //以i为中心偶数串
        low = i, high = i + 1;
        max[i][1] = 0;
        while (low >= 0 && high < len && str[low] == str[high])
        {
            low--;
            high++;
            max[i][1] += 2;
        }
    }

    int maxlen = 0;
    for (int i = 0; i < len; i++)
    {
        for (int j = 0; j < 2; j++)
        {
            if (max[i][j] > maxlen)
                maxlen = max[i][j];
        }
    }

    printf("%d", maxlen);

    return 0;
}
