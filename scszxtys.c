//删除一维数组所有相同的数字 只剩下一个

//#include<stdio.h>
//
//int fun(int a[], int n)
//{
//	int k = 0;
//
//	int b[100] = { 0 };
//	int i;
//	for (i = 0;i < n;i++)
//	{
//		b[a[i]] = a[i];
//	}
//	int j = 0;
//	for (i = 0;i < 100;i++)
//	{
//		if (b[i] != 0)
//		{
//			a[j] = b[i];
//			j++;
//			k++;
//		}
//
//	}
//	return k;
//
//
//}
//
//
//
//
//int main()
//{
//	int a[10] = { 2,3,5,4,2,3,5,1,5,1 }, i, k;
//	k = fun(a, 10);
//
//	for (i = 0;i < k;i++)
//	{
//		printf("%d ", a[i]);
//	}
//
//	return 0;
//}


#include<stdio.h>

int fun(int a[], int n)
{
	int k = 0;
	int i, j;
	//先进行排序
	for (i = 0;i < n - 1;i++)
	{
		for (j = 0;j< n - i - 1;j++)
		{
			if (a[j] > a[j + 1])
			{
				int tmp = a[j];
				a[j] = a[j + 1];
				a[j + 1] = tmp;
			}
		}
	}
	//然后进行删除
	for (i = 0;i < n;i++)
	{
		if (a[i] != a[i + 1])
		{
			a[k] = a[i];
			k++;
		}
	}
	return k;
}
int main()
{
	int a[10] = { 2,3,5,4,2,3,5,1,5,1 }, i, k;
	k = fun(a, 10);

	for (i = 0;i < k;i++)
	{
		printf("%d ", a[i]);
	}

	return 0;
}