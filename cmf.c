#include <stdio.h>
#include <string.h>
//遍历添加砝码,一种类似桶排序的思想
int main()
{
  //暴力遍历
  int n;
  while (scanf("%d", &n) != EOF)
  {
    int weight_base[15] = {0};
    int limit[15] = {0};

    for (int i = 0; i < n; i++)
      scanf("%d", &weight_base[i]);
    for (int i = 0; i < n; i++)
      scanf("%d", &limit[i]);

    int total = 0;
    for (int i = 0; i < n; i++)
    {
      total += weight_base[i] * limit[i];
    }
    int weight_exist[total + 1] ;
      for(int i=0;i<total+1;i++)
          weight_exist[i]=0;
      
      
      
    weight_exist[0] = 1;
    //遍历添加每一个砝码,从后向前,从当前获得过得最大值开始
    for (int i = 0; i < n; i++)
    {
      for (int j = 0; j < limit[i]; j++)
      {
        for (int k = total; k >= 0; k--)//从前向后会前面影响后面的操作如 1 2 3 4  1+2=3  会使3+2 = 5这样连加两个砝码
        {
          if (weight_exist[k] == 1)
            weight_exist[k + weight_base[i]] = 1;
        }
      }
    }
int kind = 0;
    for(int i=0;i<total+1;i++){
kind += weight_exist[i];
    }

    printf("%d\n",kind);
  }


  return 0;
}

