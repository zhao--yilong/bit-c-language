#define _CRT_SECURE_NO_WARNINGS 1

#include"list.h"

void test()
{
	SListNode* plist = NULL;
	//头插
	SListPushFront(&plist, 100);
	SListPushFront(&plist, 200);
	SListPushFront(&plist, 400);
	
	//尾插
	SListPushBack(&plist, 100);
	//尾删
	SListPopBack(&plist);
	//头删
	SListPopFront(&plist);

	SListNode* cur = SListFind(plist, 100);
	//查找
	if (cur)
	{
		//在pos 之后位置插入一个数
		SListInsertAfter(cur, 3);

		//删除3
		SListEraseAfter(cur);
		printf("找到了\n");
	}
	else
	{
		printf("没找到\n");
	}

	SListPrint(plist);
	SListDestroy(&plist);
	printf("\n");
	SListPrint(plist);

}
int main()
{
	test();


	return 0;
}