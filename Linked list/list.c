#define _CRT_SECURE_NO_WARNINGS 1

#include"list.h"

// 动态申请一个节点
SListNode* BuySListNode(SLTDateType x)
{
	SListNode* newnode = (SListNode*)malloc(sizeof(SListNode));

	if (newnode == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}

	newnode->data = x;
	newnode->next = NULL;

	return newnode;

}

// 单链表打印
void SListPrint(SListNode* plist)
{
	SListNode* cur = plist;
	while (cur != NULL)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
}
// 单链表尾插
void SListPushBack(SListNode** pplist, SLTDateType x)
{
	SListNode* newnode = BuySListNode(x);

	if (*pplist == NULL)
	{
		*pplist = newnode;
	}
	else
	{
		SListNode* tail = *pplist;
		// 找尾
		while (tail->next)
		{
			tail = tail->next;
		}

		tail->next = newnode;
	}
}
// 单链表的头插
void SListPushFront(SListNode** pplist, SLTDateType x)
{
	SListNode* newnode = BuySListNode(x);
	newnode->next = *pplist;  //new 链接 pp
	*pplist = newnode;  
}
// 单链表的尾删
void SListPopBack(SListNode** pplist)
{
	//断言  链表为空
	assert(*pplist);

	//链表就一个元素
	if ((*pplist)->next == NULL)
	{
		free(*pplist);
		*pplist = NULL;
	}
	else  //链表为多元素
	{
		SListNode* tail = *pplist;
		while (tail->next->next)//找到倒数第二个元素
		{
			tail =  tail->next;//最后tail  指向的是倒数第二个元素
		}
		free(tail->next);//释放掉最后一个
		tail->next = NULL;//倒数第二个元素变为最后一个  置为空
	}
}
// 单链表头删
void SListPopFront(SListNode** pplist)
{
	SListNode* next = (*pplist)->next;//next  指向 头部的下一个地址
	free(*pplist);//删除头部
	*pplist = next;//头部指向下一个地址处·
}

// 单链表查找
SListNode* SListFind(SListNode* plist, SLTDateType x)
{
	SListNode* sur = plist;

	while (sur)
	{
		if (sur->data == x)
		{
			return sur;
		}
		sur = sur->next;
	}
	return NULL;  
}
// 单链表在pos位置之后插入x
// 分析思考为什么不在pos位置之前插入？
void SListInsertAfter(SListNode* pos, SLTDateType x)
{
	assert(pos);

	SListNode* cur = BuySListNode(x); //先动态申请一个节点
	
	 cur->next = pos->next;//先把后面的节点给新节点
	 pos->next = cur;//再把pos的下一个节点指向cur
}
// 单链表删除pos位置之后的值
// 分析思考为什么不删除pos位置？
void SListEraseAfter(SListNode* pos)
{
	assert(pos);

	SListNode* cur = pos->next;
	pos->next = pos->next->next;
	free(cur);
	cur = NULL;
}

// 单链表的销毁
void SListDestroy(SListNode** plist)
{
	SListNode* cur = *plist;
	while (cur)
	{
		SListNode* newnode = cur->next;//新节点等于初始下一个

		free(cur);   //删除头
	
		cur = newnode;// 头等于新节点

	}
	*plist = NULL;
}