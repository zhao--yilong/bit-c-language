#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<stdlib.h>
int comp(const void* a, const void* b)
{
    return *(char*)a - *(char*)b;
}
int main()
{
    char str[1001];
    scanf("%s", str);
    int n = strlen(str);
    qsort(str, n, sizeof(char), comp);
    printf("%s\n", str);
    return 0;
}